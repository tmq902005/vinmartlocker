package wee.vinmart.locker

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.lifecycle.LifecycleOwner
import com.intel.realsenseid.api.Faceprints
import kotlinx.android.synthetic.main.activity_register_face.*
import wee.vinmart.locker.app.app
import wee.vinmart.locker.camera.FaceControl
import wee.vinmart.locker.camera.FaceResult
import wee.vinmart.locker.utils.handlerMain
import wee.vinmart.locker.utils.post
import wee.vinmart.locker.utils.show
import wee.vinmart.locker.vm.CabinetVM
import wee.vinmart.locker.vm.TIME_OUT
import wee.vinmart.locker.vm.pLockerData

class RegisterFaceActivity : WeeActivity(), LifecycleOwner {

    companion object {
        const val TAG = "VerifyFaceActivity"
    }

    private var isProcessing = false

    private val mHandler: Handler by lazy { Handler(this.mainLooper) }

    private var mRunTimeOut = Runnable {
        actRegisterFace_verifyFaceControl.isOn = false
        gotoWelcomeActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_face)
        initVerifyFaceControl()
        initUi()
    }

    private fun initUi() {
        actRegisterFace_registerFailedControl.onCancelClick { gotoWelcomeActivity() }
    }

    private fun initVerifyFaceControl() {
        actRegisterFace_verifyFaceControl.createPreviewCamera()
        showCamera()
        actRegisterFace_verifyFaceControl.setVerifyFaceListener(object :
            FaceControl.OnVerifyFaceListener {
            override fun onMessage(message: String) {

            }

            override fun onResult(faceResult: FaceResult) {
                isProcessing = true
                actRegisterFace_verifyFaceControl.actionVerify()
                post { actRegisterFace_status.text = "Hệ thống đang xử lý..." }
                resetTimeOut()
            }

            override fun onActionDone(faceResult: FaceResult, faceprints: Faceprints) {
                Log.e("onActionDone", "$faceprints")
                CabinetVM.ins.registerLocker(
                    faceResult.faceData!!,
                    faceprints,
                    pLockerData!!,
                    object : CabinetVM.RegLockerListener {
                        override fun onResult(resp: CabinetVM.RespRegLocker?) {
                            Log.e("registerLocker", "$resp")
                            if (resp != null) {
                                when (resp.ErrorCode) {
                                    0 -> {
                                        post {
                                            app.usbConnectionControlV2?.write(pLockerData?.ID ?: "")
                                            actRegisterFace_verifyFaceControl.successFace()
                                            actRegisterFace_status.text = "Đang thực hiện mở tủ số"
                                            actRegisterFaceNumber.show()
                                            actRegisterFaceNumber.text = "- ${pLockerData?.ID} -"
                                            handlerMain.postDelayed({ gotoWelcomeActivity() }, 2000)
                                        }
                                    }
                                    10 -> {
                                        post {
                                            actRegisterFace_registerFailedControl.show()
                                            actRegisterFace_registerFailedControl.setIconAnimFail()
                                            actRegisterFace_registerFailedControl.setTitle("Bạn đã có tủ rồi!")
                                            actRegisterFace_registerFailedControl.setMessage("Bạn đã đăng ký tủ trước đó, Vui lòng lấy đồ trước khi đăng ký tủ")
                                            actRegisterFace_registerFailedControl.hideTryAgain = true
                                            actRegisterFace_registerFailedControl.setActionCancelBtn { gotoWelcomeActivity() }
                                            mHandler.postDelayed({ gotoWelcomeActivity() }, 10000)
                                        }
                                    }
                                    else -> {
                                        post {
                                            actRegisterFace_registerFailedControl.show()
                                            actRegisterFace_registerFailedControl.setIconAnimFail()
                                            actRegisterFace_registerFailedControl.setTitle("Chưa thể xử lý tủ cho bạn!")
                                            actRegisterFace_registerFailedControl.setMessage("Vui lòng thử lại sau, xin cảm ơn")
                                            actRegisterFace_registerFailedControl.hideTryAgain = true
                                            actRegisterFace_registerFailedControl.setActionCancelBtn { gotoWelcomeActivity() }
                                            mHandler.postDelayed({ gotoWelcomeActivity() }, 10000)
                                        }
                                    }
                                }
                            } else {
                                post {
                                    actRegisterFace_registerFailedControl.show()
                                    actRegisterFace_registerFailedControl.setIconAnimFail()
                                    actRegisterFace_registerFailedControl.setTitle("Chưa thể xử lý tủ cho bạn!")
                                    actRegisterFace_registerFailedControl.setMessage("Vui lòng thử lại sau, xin cảm ơn")
                                    actRegisterFace_registerFailedControl.hideTryAgain = true
                                    actRegisterFace_registerFailedControl.setActionCancelBtn { gotoWelcomeActivity() }
                                    mHandler.postDelayed({ gotoWelcomeActivity() }, 10000)
                                }
                            }
                        }

                    })
            }

        })
    }

    private fun showCamera() {
        actRegisterFace_verifyFaceControl.isOn = true
        actRegisterFace_verifyFaceControl.visibility = View.VISIBLE
        actRegisterFace_verifyFaceControl.resetData()
    }

    private fun resetTimeOut() {
        mHandler.removeCallbacks(mRunTimeOut)
        mHandler.postDelayed(mRunTimeOut, TIME_OUT)
    }

    override fun onResume() {
        super.onResume()
        actRegisterFace_verifyFaceControl.startCamera()
        resetTimeOut()
    }

    override fun onPause() {
        super.onPause()
        actRegisterFace_verifyFaceControl.stopCamera()
        mHandler.removeCallbacks(mRunTimeOut)
    }

    override fun onDestroy() {
        super.onDestroy()
        mHandler.removeCallbacksAndMessages(null);
        actRegisterFace_verifyFaceControl.destroyCamera()
    }
}
