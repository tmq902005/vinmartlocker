package wee.vinmart.locker

import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.LifecycleOwner
import com.intel.realsenseid.api.Faceprints
import kotlinx.android.synthetic.main.activity_register_face_admin.*
import wee.vinmart.locker.app.toast
import wee.vinmart.locker.camera.FaceControl
import wee.vinmart.locker.camera.FaceResult
import wee.vinmart.locker.control.RegisterInfoAdminControl
import wee.vinmart.locker.utils.CommonAnimation
import wee.vinmart.locker.utils.hide
import wee.vinmart.locker.utils.post
import wee.vinmart.locker.utils.show
import wee.vinmart.locker.vm.CabinetVM
import wee.vinmart.locker.vm.TIME_OUT
import wee.vinmart.locker.vm.pAdminData

class RegisterFaceAdminActivity : WeeActivity(), LifecycleOwner {

    private var isProcessing = false

    private val mHandler: Handler by lazy { Handler(mainLooper) }

    private var mFaceUser: FaceResult? = null

    private var mFacePrint: Faceprints? = null

    private var mRunTimeOut = Runnable {
        actRegisterFaceAdmin_verifyFaceControl.isOn = false
        gotoWelcomeActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_face_admin)
        initVerifyFaceControl()
        initHeader()
        initRegisterAdminResult()
    }

    private fun initHeader() {
        adminFaceBack.setOnClickListener {
            if (isProcessing) return@setOnClickListener
            gotoWelcomeActivity()
        }
        actRegisterFaceAdmin_registerInfo.actionRegisterBtn {
            resetTimeOut()
            val name = actRegisterFaceAdmin_registerInfo.name
            val phone = actRegisterFaceAdmin_registerInfo.phone
            if (name.isNullOrEmpty()) {
                toast("vui lòng nhập họ và tên của bạn")
                return@actionRegisterBtn
            }
            if (phone.isNullOrEmpty() || phone.length < 10) {
                toast("vui lòng nhập đúng số điện thoại")
                return@actionRegisterBtn
            }
            val userAdminInfo = RegisterInfoAdminControl.RegisterAdminInfo(
                mFaceUser!!.faceData,
                mFacePrint!!,
                name,
                phone
            )
            CabinetVM.ins.registerUserAdmin(userAdminInfo, object : CabinetVM.RegAdminListener {
                override fun onResult(resp: CabinetVM.RespRegAdmin?) {
                    post {
                        actRegisterFaceAdmin_registerInfoContainer.hide()
                        actRegisterFaceAdmin_registerResultContainer.show()
                        actRegisterFaceAdmin_registerResult.showButton1 = false
                        actRegisterFaceAdmin_registerResult.showButton3 = false
                        actRegisterFaceAdmin_registerResult.textButton2 = "Quay lại"
                    }
                    when (resp?.ErrorCode) {
                        0 -> {
                            pAdminData = resp.AdminID
                            actRegisterFaceAdmin_registerResult.showSuccess()
                            actRegisterFaceAdmin_registerResult.setIconAnimSuccess()
                            actRegisterFaceAdmin_registerResult.setActionCancelBtn {
                                gotoAdminActivity()
                            }
                            mHandler.postDelayed({ gotoAdminActivity() }, 3000)
                        }
                        10 -> {
                            actRegisterFaceAdmin_registerResult.setIconAnimFail()
                            actRegisterFaceAdmin_registerResult.showFailed("Khuôn mặt đã tồn tại, bạn đã đăng ký trước đó")
                            actRegisterFaceAdmin_registerResult.setActionCancelBtn {
                                gotoWelcomeActivity()
                            }
                            mHandler.postDelayed({ gotoWelcomeActivity() }, 3000)
                        }
                        else -> {
                            actRegisterFaceAdmin_registerResult.setIconAnimFail()
                            actRegisterFaceAdmin_registerResult.showFailed("Hệ thốn đang bị lỗi, Bạn vui lòng thử lại")
                            actRegisterFaceAdmin_registerResult.setActionCancelBtn {
                                gotoWelcomeActivity()
                            }
                            mHandler.postDelayed({ gotoWelcomeActivity() }, 3000)
                        }
                    }
                }
            })
        }
    }

    private fun initVerifyFaceControl() {
        actRegisterFaceAdmin_verifyFaceControl.createPreviewCamera()
        startCamera()
        actRegisterFaceAdmin_verifyFaceControl.startCamera()
        actRegisterFaceAdmin_verifyFaceControl.setVerifyFaceListener(object :
            FaceControl.OnVerifyFaceListener {
            override fun onMessage(message: String) {

            }

            override fun onResult(faceResult: FaceResult) {
                isProcessing = true
                actRegisterFaceAdmin_verifyFaceControl.actionVerify()
                resetTimeOut()
            }

            override fun onActionDone(faceResult: FaceResult, faceprints: Faceprints) {
                isProcessing = false
                mFaceUser = faceResult
                mFacePrint = faceprints
                post {
                    actRegisterFaceAdmin_cameraContainer.hide()
                    actRegisterFaceAdmin_registerInfoContainer.show()
                    actRegisterFaceAdmin_registerInfo.setData(faceResult.faceData!!)
                }
                resetTimeOut()
            }
        })
    }

    private fun startCamera() {
        actRegisterFaceAdmin_verifyFaceControl.isOn = true
        actRegisterFaceAdmin_verifyFaceControl.resetData()
    }

    private fun initRegisterAdminResult() {
        actRegisterFaceAdmin_registerResult.setActionCancelBtn {

        }
        actRegisterFaceAdmin_registerResult.setActionExitBtn {

        }
        with(actRegisterFaceAdmin_registerResult) {
            actRegisterFaceAdmin_registerResult.setActionTryAgainBtn {
                CommonAnimation().setAnimFadeOut(
                    actRegisterFaceAdmin_registerResultContainer,
                    300
                ) {
                    isProcessing = false
                    startCamera()
                }
            }
        }

    }

    private fun resetTimeOut() {
        mHandler.removeCallbacks(mRunTimeOut)
        mHandler.postDelayed(mRunTimeOut, TIME_OUT)
    }

    override fun onResume() {
        super.onResume()
        resetTimeOut()
    }

    override fun onPause() {
        super.onPause()
        mHandler.removeCallbacks(mRunTimeOut)
    }

    override fun onDestroy() {
        super.onDestroy()
        mHandler.removeCallbacksAndMessages(null)
        actRegisterFaceAdmin_verifyFaceControl.destroyCamera()
    }
}
