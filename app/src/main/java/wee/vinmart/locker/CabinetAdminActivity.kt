package wee.vinmart.locker

import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.activity_cabinet_admin.*
import wee.vinmart.locker.adapter.CabinetAdapter
import wee.vinmart.locker.control.LockerControl
import wee.vinmart.locker.vm.TIME_OUT
import wee.vinmart.locker.vm.pListLockerData
import wee.vinmart.locker.vm.pLockerDataAdmin

class CabinetAdminActivity : WeeActivity() {

    private val mHandler: Handler by lazy {
        Handler(mainLooper)
    }

    private val runnableTimeOut = Runnable {
        gotoWelcomeActivity()
    }

    private val adapterAdmin = CabinetAdapter(true)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cabinet_admin)
        initAdapter()
        actCabinetAdminBack.setOnClickListener {
            gotoAdminActivity()
        }
        actCabinetAdminCancel.setOnClickListener {
            gotoWelcomeActivity()
        }
    }

    private fun initAdapter() {
        adapterAdmin.set(pListLockerData)
        adapterAdmin.bind(actCabinetAdminRecyclerView, 2)
        adapterAdmin.onItemClick { lockerData, i ->
            if (lockerData.Type == LockerControl.SCREEN) return@onItemClick
            pLockerDataAdmin = lockerData
            gotoCheckCabinetActivity()
        }
    }

    private fun resetTimeOut() {
        mHandler.removeCallbacks(runnableTimeOut)
        mHandler.postDelayed(runnableTimeOut, TIME_OUT)
    }

    override fun onResume() {
        super.onResume()
        resetTimeOut()
    }

    override fun onPause() {
        super.onPause()
        mHandler.removeCallbacks(runnableTimeOut)
    }

}