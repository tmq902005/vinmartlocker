package wee.vinmart.locker

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import kotlinx.android.synthetic.main.activity_cabinet.*
import wee.vinmart.locker.adapter.CabinetAdapter
import wee.vinmart.locker.control.LockerControl
import wee.vinmart.locker.utils.CommonAnimation
import wee.vinmart.locker.vm.TIME_OUT
import wee.vinmart.locker.vm.pIsShowTime
import wee.vinmart.locker.vm.pListLockerData
import wee.vinmart.locker.vm.pLockerData

class CabinetActivity : WeeActivity() {

    private var mHandler = Handler(Looper.getMainLooper())

    private var mRunTimeOut = Runnable {
        gotoWelcomeActivity()
    }

    private val adapter = CabinetAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cabinet)
        initUI()
    }

    private fun initUI() {
        pIsShowTime = false

        actCabinetBack.setOnClickListener { gotoWelcomeActivity() }

        adapter.set(pListLockerData)
        adapter.bind(actCabinetRecyclerView, 2)
        adapter.onItemClick { cabinetData, i -> changeUiAdapter(cabinetData, i) }
    }

    private fun changeUiAdapter(data: LockerControl.LockerData, position: Int) {
        if (data.Type == LockerControl.SCREEN) return
        if (data.Type == LockerControl.USED) return
        pLockerData = data
        gotoRegisterFaceActivity()
    }

    private fun resetTimeOut(){
        mHandler.removeCallbacks(mRunTimeOut)
        mHandler.postDelayed(mRunTimeOut, TIME_OUT)
    }

    override fun onResume() {
        super.onResume()
        resetTimeOut()
    }

    override fun onPause() {
        super.onPause()
        mHandler.removeCallbacks(mRunTimeOut)
    }

}
