package wee.vinmart.locker


import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.activity_check_cabinet.*
import wee.vinmart.locker.control.DialogConfirm
import wee.vinmart.locker.utils.gone
import wee.vinmart.locker.vm.*

class CheckCabinetActivity : WeeActivity() {

    private var mDialogConfirm: Dialog? = null

    private var userId: String = ""

    private val mHandler: Handler by lazy {
        Handler(mainLooper)
    }

    private val runnableTimeOut = Runnable {
        gotoWelcomeActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_cabinet)
        initUI()
    }

    private fun initUI() {
        pIsShowTime = true
        lockerInfoBack.setOnClickListener { gotoAdminActivity() }
        lockerInfoCancel.setOnClickListener { gotoWelcomeActivity() }

        lockerInfoControl_openBtn.setTextColor(R.color.red_gym)
        lockerInfoControl_openBtn.setBackground(R.drawable.bg_cabinet)
        lockerInfoControl_openBtn.setText("MỞ TỦ")
        lockerInfoControl_openBtn.setOnClickListener {
            var mess = "Bạn đang thực hiện mở hộc tủ. Bạn có chắc muốn tiếp tục?"
            mDialogConfirm = DialogConfirm(this, mess, {
                val intent = Intent(
                    this@CheckCabinetActivity,
                    LockerAccessActivity::class.java
                )
                intent.putExtra("status", "open")
                startActivity(intent)
                finish()
            }, {
                gotoCabinetAdminActivity()
            })
            mDialogConfirm?.show()
        }

        lockerInfoControl_resetBtn.setTextColor(R.color.red_gym)
        lockerInfoControl_resetBtn.setBackground(R.drawable.bg_cabinet)
        lockerInfoControl_resetBtn.setText("RESET TỦ")
        lockerInfoControl_resetBtn.setOnClickListener {
            mDialogConfirm = DialogConfirm(this,
                "Bạn đang thực hiện reset hộc tủ của khách. Bạn có chắc muốn tiếp tục?",
                {
                    CabinetVM.ins.resetLocker(
                        pLockerDataAdmin!!.ID,
                        userId,
                        object : CabinetVM.ResetLockerListener {
                            override fun onResult(isSuccess: Boolean) {
                                val intent = Intent(
                                    this@CheckCabinetActivity,
                                    LockerAccessActivity::class.java
                                )
                                intent.putExtra("status", "reset")
                                intent.putExtra("isReset", isSuccess)
                                startActivity(intent)
                                finish()
                            }
                        })
                },
                {
                    gotoCabinetAdminActivity()
                })
            mDialogConfirm?.show()
        }

        lockerInfoControl_lockerNo.text = pLockerDataAdmin?.ID
        CabinetVM.ins.getInfoCabinet(
            pLockerDataAdmin?.ID ?: "",
            object : CabinetVM.InfoCabinetListener {
                override fun onResult(resp: CabinetVM.RespCabinetInfo?) {
                    if (resp!!.user == null) {
                        lockerInfoControl_timeRegTitle.text = "Tủ đang trống"
                        lockerInfoControl_timeReg.gone()
                        lockerInfoControl_timeHasTitle.gone()
                        lockerInfoControl_timeHas.gone()
                        lockerInfoControl_resetBtn.gone()
                    } else {
                        userId = resp.user?.userId ?: ""
                        lockerInfoControl_avatar.setImageBitmap(resp.user?.userPhoto.toBitmap())
                        lockerInfoControl_timeReg.text = pLockerDataAdmin?.Times.formatTimer()
                        val time = System.currentTimeMillis() - (pLockerDataAdmin?.Times ?: 0L)
                        lockerInfoControl_timeHas.text = time.formatTime()
                    }
                }
            })
    }

    private fun resetTimeout() {
        mHandler.removeCallbacks(runnableTimeOut)
        mHandler.postDelayed(runnableTimeOut, TIME_OUT)
    }

    override fun onResume() {
        super.onResume()
        resetTimeout()
    }

    override fun onPause() {
        super.onPause()
        mHandler.removeCallbacks(runnableTimeOut)
    }

}
