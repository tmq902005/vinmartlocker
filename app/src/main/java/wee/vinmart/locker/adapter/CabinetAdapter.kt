package wee.vinmart.locker.adapter

import android.annotation.SuppressLint
import android.graphics.PorterDuff
import android.os.Build
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.item_cabinet.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.control.LockerControl
import wee.vinmart.locker.utils.color
import wee.vinmart.locker.utils.gone
import wee.vinmart.locker.utils.show
import wee.vinmart.locker.utils.tint
import java.util.concurrent.TimeUnit

class CabinetAdapter(isShowTime: Boolean = false) : BaseRecyclerAdapter<LockerControl.LockerData>() {

    private val isShow = isShowTime

    override fun layoutResource(model: LockerControl.LockerData, position: Int): Int {
        return R.layout.item_cabinet
    }

    @SuppressLint("ResourceType")
    override fun View.onBindModel(model: LockerControl.LockerData, position: Int, layout: Int) {
        itemCabinetLabel.text = model.ID
        itemCabinetTime.setTime(model.Times)
        if (model.Type == LockerControl.USED) {
            itemCabinetIcon.setImageResource(R.drawable.ic_lock)
            itemCabinetParent.setBackgroundResource(R.mipmap.cabinet_used)
            itemCabinetIcon.tint(ContextCompat.getColor(context, R.color.red_gym))
            itemCabinetLabel.color(ContextCompat.getColor(context, R.color.red_gym))
            return
        }
        if (model.Type == LockerControl.SCREEN) {
            itemCabinetParent.setBackgroundResource(R.mipmap.cabinet_screen)
            itemCabinetIcon.gone()
            itemCabinetLabel.gone()
            itemCabinetScreen.show()
            return
        }
        itemCabinetParent.setBackgroundResource(R.mipmap.cabinet)
        itemCabinetIcon.tint(ContextCompat.getColor(context, R.color.gray))
        itemCabinetLabel.color(ContextCompat.getColor(context, R.color.gray))
    }

    @SuppressLint("DefaultLocale")
    fun TextView.setTime(time: Long) {
        if (time <= 0 || !isShow) return
        if (time > 828000000) {
            itemCabinetTime.backgroundTint(ContextCompat.getColor(context, R.color.yellow))
        }
        this.show()
        val timeCabinet = System.currentTimeMillis() - time
        val hours = TimeUnit.MILLISECONDS.toHours(timeCabinet)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(timeCabinet) - TimeUnit.HOURS.toMinutes(
            TimeUnit.MILLISECONDS.toHours(timeCabinet)
        )
        val milli = TimeUnit.MILLISECONDS.toSeconds(timeCabinet) - TimeUnit.MINUTES.toSeconds(
            TimeUnit.MILLISECONDS.toMinutes(timeCabinet)
        )
        val hms = java.lang.String.format("%02d:%02d:%02d", hours, minutes, milli)
        this.text = hms
    }

    fun View.backgroundTint(@ColorInt color: Int) {
        post {
            @Suppress("DEPRECATION")
            background?.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                background?.colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
            } else {

            }*/
        }
    }

}

data class CabinetData(var number: String?, var isLock: Boolean?)

val listCabinet = listOf(
    CabinetData("01", false),
    null,
    CabinetData("02", true),
    CabinetData("03", false),
    CabinetData("04", false),
    CabinetData("05", false)
)