package wee.vinmart.locker.adapter

import android.annotation.SuppressLint
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_key_icon.view.*
import kotlinx.android.synthetic.main.item_key_text.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.app.app

class KeyboardAdapter : BaseRecyclerAdapter<Any?>() {

    var itemTouchClick: (Any?, Int) -> Unit = { _, _ -> }

    fun onItemTouchClick(block: (Any?, Int) -> Unit) {
        itemTouchClick = block
    }

    override fun layoutResource(model: Any?, position: Int): Int {
        return when (model) {
            is Int -> R.layout.item_key_icon
            is String -> R.layout.item_key_text
            else -> 0
        }
    }

    override fun View.onBindModel(model: Any?, position: Int, layout: Int) {}

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val type = getItemViewType(position)
        if (type == 0) return
        val model = get(position) ?: return
        when (model) {
            is Int -> viewHolder.itemView.itemKeyIcon.setImageResource(model)
            is String -> viewHolder.itemView.itemKeyTextLabel.text = model.toString()
        }
        viewHolder.itemView.setOnTouchListener { v, event ->
            var touch = false
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    itemTouchClick(model, position)
                    if (model is Int) {
                        viewHolder.itemView.itemKeyIconCard.setCardBackgroundColor(app.getColor(R.color.white30))
                    } else {
                        viewHolder.itemView.itemKeyTextCard.setCardBackgroundColor(app.getColor(R.color.white30))
                    }
                    touch = true
                }
                MotionEvent.ACTION_UP -> {
                    if (model is Int) {
                        viewHolder.itemView.itemKeyIconCard.setCardBackgroundColor(app.getColor(R.color.white))
                    } else {
                        viewHolder.itemView.itemKeyTextCard.setCardBackgroundColor(app.getColor(R.color.white))
                    }
                    touch = true
                }
                MotionEvent.ACTION_CANCEL -> {
                    if (model is Int) {
                        viewHolder.itemView.itemKeyIconCard.setCardBackgroundColor(app.getColor(R.color.white))
                    } else {
                        viewHolder.itemView.itemKeyTextCard.setCardBackgroundColor(app.getColor(R.color.white))
                    }
                    touch = true
                }
            }
            touch
        }
    }

}