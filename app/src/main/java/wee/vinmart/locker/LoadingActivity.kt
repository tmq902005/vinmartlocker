package wee.vinmart.locker

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.activity_loading.*
import wee.vinmart.locker.vm.pLockerData

class LoadingActivity : WeeActivity() {

    private var mHandler = Handler()

    private val runnable = Runnable {
        actLoadingVideo?.pause()
        pLockerData = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)
    }

    override fun onResume() {
        super.onResume()
        loadVideo()
    }

    private fun loadVideo() {
        val uri = "android.resource://" + packageName + "/" + R.raw.citygym
        actLoadingVideo?.post {
            actLoadingVideo.setVideoURI(Uri.parse(uri))
            actLoadingVideo.requestFocus()
            actLoadingVideo.start()
            actLoadingVideo?.setOnCompletionListener { gotoWelcomeActivity() }
        }
    }

    override fun onPause() {
        super.onPause()
        mHandler.removeCallbacks(runnable)
    }

    override fun onDestroy() {
        mHandler.removeCallbacksAndMessages(null)
        super.onDestroy()
    }

}
