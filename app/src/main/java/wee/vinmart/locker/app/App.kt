package wee.vinmart.locker.app

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import android.widget.Toast
import rsidlib.RSM
import rsidlib.Rsidlib
import wee.vinmart.locker.camera.RealSenseIDImp
import wee.vinmart.locker.control.UsbConnectionControlV2

lateinit var app: App private set

class App : Application() {

    /**
     * [Application] override
     */
    lateinit var mHandlerMain: Handler

    lateinit var lib: RSM

    lateinit var realSenseIDImp: RealSenseIDImp

    lateinit var usbConnectionControlV2: UsbConnectionControlV2

    private val usbBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            when(intent?.action){
                "android.hardware.usb.action.USB_DEVICE_ATTACHED" -> {
                    toast("USB_DEVICE_ATTACHED")
                    usbConnectionControlV2?.openConnect {  }
                }

                "android.hardware.usb.action.USB_DEVICE_DETACHED" -> {
                    toast("USB_DEVICE_DETACHED")
                    usbConnectionControlV2?.closeConned()
                }
            }
        }

    }


    override fun onCreate() {
        super.onCreate()
        app = this
        lib = Rsidlib.newLib().rsm
        RealSenseIDImp.initLib()
        realSenseIDImp = RealSenseIDImp(app.baseContext, null)
        mHandlerMain = Handler(app.applicationContext.mainLooper)
        realSenseIDImp.create()
        usbConnectionControlV2 = UsbConnectionControlV2(this)
        usbConnectionControlV2?.openConnect()
        initReceiver()
    }

    private fun initReceiver(){
        val filter = IntentFilter().apply {
            this.addAction("android.hardware.usb.action.USB_DEVICE_ATTACHED")
            this.addAction("android.hardware.usb.action.USB_DEVICE_DETACHED")
        }
        this.registerReceiver(usbBroadcastReceiver,filter)
    }

    override fun onTerminate() {
        realSenseIDImp.destroy()
        usbConnectionControlV2?.closeConned()
        this.unregisterReceiver(usbBroadcastReceiver)
        super.onTerminate()
    }

}

private var curToast: Toast? = null
fun toast(message: String?) {
    message ?: return
    app.mHandlerMain.post {
        curToast?.cancel()
        curToast = Toast.makeText(app, message, Toast.LENGTH_SHORT)
        curToast?.show()
    }
}
