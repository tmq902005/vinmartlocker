package wee.vinmart.locker

import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.activity_locker_access.*
import wee.vinmart.locker.app.app
import wee.vinmart.locker.vm.TIME_OUT
import wee.vinmart.locker.vm.pLockerData
import wee.vinmart.locker.vm.pLockerDataAdmin

class LockerAccessActivity : WeeActivity() {

    private val mHandler: Handler by lazy {
        Handler(mainLooper)
    }

    private var mRunGoWelcome = Runnable {
        pLockerData = null
        gotoWelcomeActivity()
    }

    private var runnableTimeOut = Runnable {
        pLockerData = null
        gotoWelcomeActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_locker_access)
        initUI()
    }

    private fun actionUnlock(cabinetId: String) {
        app.usbConnectionControlV2?.write(cabinetId)
    }

    private fun initUI() {
        val status = intent.extras?.getString("status")
        val cabinet = intent.extras?.getString("cabinet")
        when (status) {
            "register" -> {
                actionUnlock(pLockerData?.ID ?: "")
                actLockerAccess_lockerAccessControl.unLocking()
                actLockerAccess_lockerAccessControl.numberCabinet = pLockerData?.ID
                mHandler.postDelayed({
                    actLockerAccess_lockerAccessControl.animSuccessLocker()
                    actLockerAccess_lockerAccessControl.unLocked()
                    mHandler.postDelayed(mRunGoWelcome, 4000)
                }, 400)
            }
            "verify" -> {
                actLockerAccess_lockerAccessControl.numberCabinet = cabinet
                actLockerAccess_lockerAccessControl.unLocking()
                actionUnlock(cabinet ?: "")
                mHandler.postDelayed({
                    actLockerAccess_lockerAccessControl.animSuccessLocker()
                    actLockerAccess_lockerAccessControl.openCabinet()
                    mHandler.postDelayed(mRunGoWelcome, 2000)
                }, 400)
            }
            "open" -> {
                actionUnlock(pLockerDataAdmin?.ID ?: "")
                actLockerAccess_lockerAccessControl.numberCabinet = pLockerDataAdmin?.ID
                actLockerAccess_lockerAccessControl.opened()
                mHandler.postDelayed(
                    { actLockerAccess_lockerAccessControl.animSuccessLocker() },
                    100
                )
                mHandler.postDelayed({ gotoAdminActivity() }, 1500)
            }
            "reset" -> {
                val sttCabinet = intent.extras?.getBoolean("isReset")
                val mColor: Boolean = if (sttCabinet == true) {
                    actLockerAccess_lockerAccessControl.reset()
                    actionUnlock(pLockerDataAdmin?.ID ?: "")
                    false
                } else {
                    actLockerAccess_lockerAccessControl.resetFail()
                    true
                }
                actLockerAccess_lockerAccessControl.numberCabinet = pLockerDataAdmin?.ID
                mHandler.postDelayed(
                    { actLockerAccess_lockerAccessControl.animSuccessLocker(mColor) },
                    100
                )
                mHandler.postDelayed({ gotoAdminActivity() }, 1500)
            }
        }
    }

    private fun resetTimeOut() {
        mHandler.removeCallbacks(runnableTimeOut)
        mHandler.postDelayed(runnableTimeOut, TIME_OUT)
    }

    override fun onResume() {
        super.onResume()
        resetTimeOut()
    }

    override fun onPause() {
        super.onPause()
        mHandler.removeCallbacks(runnableTimeOut)
    }

    override fun onDestroy() {
        mHandler.removeCallbacksAndMessages(null)
        super.onDestroy()
    }

}
