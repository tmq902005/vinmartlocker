package wee.vinmart.locker

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.lifecycle.LifecycleOwner
import com.intel.realsenseid.api.Faceprints
import kotlinx.android.synthetic.main.activity_verify_face_admin.*
import wee.vinmart.locker.app.toast
import wee.vinmart.locker.camera.FaceControl
import wee.vinmart.locker.camera.FaceResult
import wee.vinmart.locker.utils.post
import wee.vinmart.locker.vm.CabinetVM
import wee.vinmart.locker.vm.TIME_OUT
import wee.vinmart.locker.vm.pAdminData

class VerifyFaceAdminActivity : WeeActivity(), LifecycleOwner {

    private var isProcessing = false

    private val mHandler: Handler by lazy {
        Handler(mainLooper)
    }

    var mRunGoBack = Runnable { gotoWelcomeActivity() }

    var mRunGoNext = Runnable { gotoAdminActivity() }

    private var mRunTimeOut = Runnable {
        actVerifyFaceAdmin_verifyFaceControl.isOn = false
        gotoWelcomeActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_face_admin)
        initUI()
        if (intent.hasExtra("VERIFY_LOCKER")) {
            val isUnlock = intent.getBooleanExtra("UNLOCK", false)
            val isReset = intent.getBooleanExtra("RESET", false)
            mRunGoBack = Runnable {
                gotoCheckCabinetActivity()
            }
            mRunGoNext = Runnable {
                gotoLockerAccessAdminActivity(isUnlock, isReset)
            }
        }
    }

    private fun initUI() {
        actVerifyAdminBack.setOnClickListener {
            if(isProcessing) return@setOnClickListener
            gotoWelcomeActivity()
        }
        actVerifyFaceAdmin_verifyFaceControl.createPreviewCamera()
        actVerifyFaceAdmin_verifyFaceControl.setVerifyFaceListener(object :
            FaceControl.OnVerifyFaceListener {

            override fun onMessage(message: String) {

            }

            override fun onResult(faceResult: FaceResult) {
                isProcessing = true
                actVerifyFaceAdmin_verifyFaceControl.actionVerify()
                post { actVerifyFaceAdmin_status.text = "Hệ thống đang xử lý..." }
                resetTimeOut()
            }

            override fun onActionDone(faceResult: FaceResult, faceprints: Faceprints) {
                CabinetVM.ins.verifyAdminUser(faceprints, object : CabinetVM.VerifyAdminListener {
                    override fun onResult(resp: CabinetVM.RespVerifyAdmin?) {
                        when (resp?.ErrorCode) {
                            0 -> {
                                post {
                                    pAdminData = resp.AdminID
                                    actVerifyFaceAdmin_verifyFaceControl.successFace()
                                    mHandler.postDelayed({ gotoAdminActivity() }, 700)
                                }
                            }
                            1 -> {
                                post {
                                    toast("không thể nhận dạng khuôn mặt")
                                    gotoWelcomeActivity()
                                }
                            }

                            2 -> {
                                post { gotoRegisterFaceAdminActivity() }
                            }

                        }
                    }

                })
            }

        })
    }

    private fun showVerifyFailed(title: String, message: String, isButton: Boolean) {
        actVerifyFaceAdmin_verifyFailedControl.setTitle(title)
        actVerifyFaceAdmin_verifyFailedControl.setMessage(message)
        actVerifyFaceAdmin_verifyFailedControl.visibility = View.VISIBLE
        Handler().postDelayed(mRunGoBack, 2000)
        if (isButton) {
            actVerifyFaceAdmin_verifyFailedControl.changeBackgroundTextColor(
                R.color.white,
                R.drawable.bg_white_stroke2_corner10
            )
            actVerifyFaceAdmin_verifyFailedControl.setActionCancelBtn(View.OnClickListener {
                mRunGoBack.run()
            })
            actVerifyFaceAdmin_verifyFailedControl.setActionTryAgainBtn(View.OnClickListener {
                isProcessing = false
                actVerifyFaceAdmin_verifyFailedControl.visibility = View.GONE
            })
        } else {
            actVerifyFaceAdmin_verifyFailedControl.hideButton()
        }
    }

    private fun showCamera() {
        actVerifyFaceAdmin_verifyFaceControl.isOn = true
        actVerifyFaceAdmin_verifyFaceControl.resetData()
    }

    private fun resetTimeOut() {
        mHandler.removeCallbacks(mRunTimeOut)
        mHandler.postDelayed(mRunTimeOut, TIME_OUT)
    }

    override fun onResume() {
        super.onResume()
        actVerifyFaceAdmin_verifyFaceControl.startCamera()
        resetTimeOut()
    }

    override fun onPause() {
        super.onPause()
        mHandler.removeCallbacks(mRunTimeOut)
    }

    override fun onDestroy() {
        super.onDestroy()
        mHandler.removeCallbacksAndMessages(null)
        actVerifyFaceAdmin_verifyFaceControl.stopCamera()
    }
}
