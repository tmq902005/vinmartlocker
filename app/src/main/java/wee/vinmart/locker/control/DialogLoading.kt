package wee.vinmart.locker.control

import android.app.Dialog
import android.content.Context
import wee.vinmart.locker.R

class DialogLoading(context: Context) : Dialog(context) {
    init {
        this.setContentView(R.layout.dialog_loading_layout)
        val mWindow = this.window
        mWindow?.setBackgroundDrawableResource(android.R.color.transparent)
        this.setCanceledOnTouchOutside(false)
    }
}