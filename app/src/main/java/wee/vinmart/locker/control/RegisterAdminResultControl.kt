package wee.vinmart.locker.control

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.control_registeradmin_result_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.utils.gone
import wee.vinmart.locker.utils.show


class RegisterAdminResultControl : ConstraintLayout {

    var showButton1: Boolean
        get() = false
        set(value) {
            if (value) {
                registerAdminResultControl_tryAgainBtn.show()
            } else {
                registerAdminResultControl_tryAgainBtn.gone()
            }
        }

    var showButton2: Boolean
        get() = false
        set(value) {
            if (value) {
                registerAdminResultControl_cancelBtn.show()
            } else {
                registerAdminResultControl_cancelBtn.gone()
            }
        }

    var showButton3: Boolean
        get() = false
        set(value) {
            if (value) {
                registerAdminResultControl_exitBtn.show()
            } else {
                registerAdminResultControl_exitBtn.gone()
            }
        }

    var textButton1: String
        get() = ""
        set(value) {
            registerAdminResultControl_tryAgainBtn.setText(value)
        }

    var textButton2: String
        get() = ""
        set(value) {
            registerAdminResultControl_cancelBtn.setText(value)
        }

    var textButton3: String
        get() = ""
        set(value) {
            registerAdminResultControl_exitBtn.setText(value)
        }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView(context, attrs)
    }

    private fun initView(context: Context, attrs: AttributeSet) {
        LayoutInflater.from(context).inflate(R.layout.control_registeradmin_result_layout, this)

        registerAdminResultControl_cancelBtn.setText("VỀ TRANG CHÍNH")
        registerAdminResultControl_tryAgainBtn.setText("THỬ LẠI")
        registerAdminResultControl_exitBtn.setText("THOÁT RA")
        registerAdminResultControl_cancelBtn.setTextColor(R.color.pinkish_red_two)
        registerAdminResultControl_tryAgainBtn.setTextColor(R.color.pinkish_red_two)
        registerAdminResultControl_exitBtn.setTextColor(R.color.white)
        registerAdminResultControl_exitBtn.setBackground(R.drawable.bg_pinkish_red_corner5)
        registerAdminResultControl_tryAgainBtn.setBackground(R.drawable.bg_white_corner5)
        registerAdminResultControl_cancelBtn.setBackground(R.drawable.bg_white_corner5)
        /*hideBtn()*/
    }

    private fun hideBtn(){
        registerAdminResultControl_tryAgainBtn.visibility = View.GONE
        registerAdminResultControl_cancelBtn.visibility = View.GONE
    }

    fun setActionTryAgainBtn(onClickListener: OnClickListener){
        registerAdminResultControl_tryAgainBtn.setActionClick(onClickListener)
    }

    fun setActionCancelBtn(onClickListener: OnClickListener){
        registerAdminResultControl_cancelBtn.setActionClick(onClickListener)
    }

    fun setActionExitBtn(onClickListener: OnClickListener){
        registerAdminResultControl_exitBtn.setActionClick(onClickListener)
    }

    fun setIconAnimSuccess(){
        registerAdminResultControl_statusIcon.setAnimation("ic_success.json")
        registerAdminResultControl_statusIcon.playAnimation()
    }

    fun setIconAnimFail(){
        registerAdminResultControl_statusIcon.setAnimation("ic_fail.json")
        registerAdminResultControl_statusIcon.playAnimation()
    }

    fun showSuccess() {
        registerAdminResultControl_title.text = "ĐĂNG KÝ THÀNH CÔNG"
        registerAdminResultControl_message.gone()
    }

    fun showFailed(mess: String) {
        registerAdminResultControl_title.text = "ĐĂNG KÝ\nKHÔNGTHÀNH CÔNG"
        registerAdminResultControl_message.text = mess

    }
}