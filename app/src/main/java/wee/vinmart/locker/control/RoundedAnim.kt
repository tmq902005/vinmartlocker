package wee.vinmart.locker.control

import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.animation.LinearInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.rounded_anim.view.*
import wee.vinmart.locker.R


class RoundedAnim : ConstraintLayout {

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(context, attrs)
    }

    private fun initView(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.rounded_anim, this)
    }

    fun startAnim(color: Int = R.color.green, start: Float = 0f, end: Float = 360f) {
        canvasRounded?.color = color
        canvasRounded?.start = start
        val valueAnim = ValueAnimator.ofFloat(start, end).apply {
            interpolator = LinearInterpolator()
            duration = 500
            addUpdateListener {
                val value = it.animatedValue as Float
                canvasRounded?.end = value
            }
        }
        valueAnim.start()
    }

}