package wee.vinmart.locker.control

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.control_verifyfailed_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.utils.CommonAnimation
import wee.vinmart.locker.utils.gone
import wee.vinmart.locker.utils.show

class VerifyFailedControl : ConstraintLayout {

    private var isProcessing = false

    var hideTryAgain: Boolean
        get() = verifyFailedControl_tryAgainBtn.isShown
        set(value) {
            if (value) {
                verifyFailedControl_tryAgainBtn.gone()
            } else {
                verifyFailedControl_tryAgainBtn.show()
            }
        }

    var hideCancel: Boolean
        get() = verifyFailedControl_cancelBtn.isShown
        set(value) {
            if (value) {
                verifyFailedControl_cancelBtn.gone()
            } else {
                verifyFailedControl_cancelBtn.show()
            }
        }

    var textButton1: String
        get() = ""
        set(value) {
            verifyFailedControl_tryAgainBtn.setText(value)
        }

    var textButton2: String
        get() = ""
        set(value) {
            verifyFailedControl_cancelBtn.setText(value)
        }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(context, attrs)
    }

    private fun initView(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.control_verifyfailed_layout, this)
    }

    fun onTryAgainClick(block: () -> Unit) {
        verifyFailedControl_tryAgainBtn.setOnClickListener { block() }
    }

    fun onCancelClick(block: () -> Unit) {
        verifyFailedControl_cancelBtn.setOnClickListener { block() }
    }

    fun setActionTryAgainBtn(onClickListener: OnClickListener) {
        verifyFailedControl_tryAgainBtn.visibility = View.VISIBLE
        verifyFailedControl_tryAgainBtn.setText("THỬ LẠI")
        verifyFailedControl_tryAgainBtn.setActionClick(onClickListener)
    }

    fun setActionCancelBtn(onClickListener: OnClickListener) {
        verifyFailedControl_cancelBtn.visibility = View.VISIBLE
        verifyFailedControl_cancelBtn.setText("VỀ TRANG CHỦ")
        verifyFailedControl_cancelBtn.setActionClick(onClickListener)
    }

    fun changeBackgroundTextColor(textColor: Int, bgColor: Int){
        verifyFailedControl_tryAgainBtn.setTextColor(textColor)
        verifyFailedControl_tryAgainBtn.setBackground(bgColor)
        verifyFailedControl_cancelBtn.setTextColor(textColor)
        verifyFailedControl_cancelBtn.setBackground(bgColor)
    }

    fun hideButton(){
        verifyFailedControl_tryAgainBtn.visibility = View.GONE
        verifyFailedControl_cancelBtn.visibility = View.GONE
    }

    fun setIconAnimFail(){
        verifyFailedControl_failImg.playAnimation()
    }

    fun setTitle(title: String){
        verifyFailedControl_title.text = title
    }

    fun setMessage(mess: String){
        verifyFailedControl_message.text = mess
    }

}