package wee.vinmart.locker.control

import android.animation.Animator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import kotlinx.android.synthetic.main.control_lockeraccess_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.utils.post
import wee.vinmart.locker.utils.show


class LockerAccessControl : ConstraintLayout {

    private var isProcessing = false

    var numberCabinet: String?
        get() = lockAccessLabelNumber.text.toString()
        set(value) {
            lockAccessLabelNumber.text = value
        }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(context, attrs)
    }

    private fun initView(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.control_lockeraccess_layout, this)
    }

    fun animSuccessLocker(isGreen : Boolean = true) {
//        lockerAccessRounded.startAnim(color)
        if(isGreen){
            lockerAccessLottie.setAnimation("cabinet_success.json")
        }else{
            lockerAccessLottie.setAnimation("cabinet_reset.json")
        }
        lockerAccessLottie.playAnimation()
        post(500) { lockerAccessLottie.pauseAnimation() }
    }

    fun labelStatusCabinet(numberCabinet: String) {
        val text = context.getString(R.string.lock_access_view_selected, numberCabinet)
        lockAccessStatus.text = HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_LEGACY)
    }

    fun unLocking() {
        lockAccessLabelNumber.setTextColor(ContextCompat.getColor(context, R.color.red_gym))
        lockAccessStatus.text = "Đang mở khoá"
    }

    fun opened() {
        lockAccessLabelNumber.setTextColor(ContextCompat.getColor(context, R.color.black20))
        lockAccessStatus.text = "Đã mở khoá"
    }

    fun reset() {
        lockAccessLabelNumber.setTextColor(ContextCompat.getColor(context, R.color.black20))
        lockAccessStatus.text = "Đã reset"
    }

    fun resetFail() {
        lockAccessLabelNumber.setTextColor(ContextCompat.getColor(context, R.color.black20))
        lockAccessStatus.text = "reset tủ không thành công"
    }

    fun unLocked() {
        lockAccessLabelNumber.setTextColor(ContextCompat.getColor(context, R.color.green))
        lockAccessStatus.text =
            "Mời quý khách cất đồ vào tủ. Tủ sẽ tự động khoá lại sau khi đóng tủ."
    }

    fun showIcon() {

    }

    fun openCabinet() {
        lockAccessLabelNumber.setTextColor(ContextCompat.getColor(context, R.color.green))
        lockAccessStatus.text = "Tủ đã được mở khoá. Xin đừng quên bất kì vật dụng nào nhé !"
        lockerAccessRootIcon.show()
    }

}