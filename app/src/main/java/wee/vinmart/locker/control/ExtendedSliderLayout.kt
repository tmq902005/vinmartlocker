package wee.vinmart.locker.control

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import com.daimajia.slider.library.SliderLayout


class ExtendedSliderLayout : SliderLayout {

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return true
    }
}