package wee.vinmart.locker.control

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.control_lockeraccessadmin_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.vm.pLockerDataAdmin


class LockerAccessAdminControl : ConstraintLayout {

    private var isProcessing = false
    private var mRootView: View?=null
    //---
    constructor(context: Context):super(context)
    constructor(context: Context, attrs: AttributeSet):super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int):super(context, attrs, defStyleAttr)
    //---
    init {
        if(mRootView==null) {
            mRootView = inflate(context, R.layout.control_lockeraccessadmin_layout, this)
            setUnLocking()
        }
    }

    fun startAnimationUnlockSuccess(){
        Handler().postDelayed({
            lockerAccessAdminControl_jsonAnim.playAnimation()
            setUnlockSuccess()
        },1000)
    }

    fun startAnimationUnlockUnSuccess(){
        Handler().postDelayed({
            lockerAccessAdminControl_jsonAnim.playAnimation()
            setUnlockUnSuccess()
        },1000)
    }

    fun startAnimationResetSuccess(){
        Handler().postDelayed({
            lockerAccessAdminControl_jsonAnim.visibility = View.GONE
            setResetSuccess()
        },1000)
    }

    fun startAnimationResetUnSuccess(){
        Handler().postDelayed({
            lockerAccessAdminControl_jsonAnim.visibility = View.GONE
            resetUnSuccess()
        },1000)
    }

    private fun setUnLocking(){
        val numLocker = pLockerDataAdmin!!.ID
        val statusLocker = "Đang mở khóa"
        val textMessage = ""
        lockerAccessAdminControl_numLocker.setTextColor(resources.getColor(R.color.white))
        lockerAccessAdminControl_message.text = textMessage
        lockerAccessAdminControl_numLocker.text = numLocker
        lockerAccessAdminControl_title.text = statusLocker
    }

    private fun setUnlockSuccess(){
        lockerAccessAdminControl_numLocker.setTextColor(resources.getColor(R.color.white))
        val status = "Đã mở khóa"
        lockerAccessAdminControl_title.text = status
    }

    private fun setResetSuccess(){
        lockerAccessAdminControl_numLocker.setTextColor(resources.getColor(R.color.white))
        val status = "Đã Reset"
        lockerAccessAdminControl_title.text = status
    }

    private fun resetUnSuccess(){
        lockerAccessAdminControl_numLocker.setTextColor(resources.getColor(R.color.white))
        val status = "Reset Không Thành Công"
        lockerAccessAdminControl_title.text = status
    }

    private fun setUnlockUnSuccess(){
        lockerAccessAdminControl_numLocker.setTextColor(resources.getColor(R.color.white))
        val status = "Mở Khóa Không Thành Công"
        lockerAccessAdminControl_title.text = status
    }

}