package wee.vinmart.locker.control

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.control_locker_register_layout.view.*
import wee.vinmart.locker.R


class LockerRegisterControl : ConstraintLayout {

    private var isProcessing = false
    private var mRootView: View?=null
    private var mRunAfter = Runnable {  }
    private val TIME_OUT = 500L
    //---
    constructor(context: Context):super(context)
    constructor(context: Context, attrs: AttributeSet):super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int):super(context, attrs, defStyleAttr)
    //---
    init {
        if(mRootView==null) {
            mRootView = inflate(context, R.layout.control_locker_register_layout, this)
        }
    }

    fun setRunAfter(runnable: Runnable){
        mRunAfter = runnable
    }

    fun setLock(lockerData: LockerControl.LockerData){
        val numLocker = lockerData.ID
        val statusLocker = "TỦ ĐANG KHÓA"
        val textMessage = "Quý khách cần đăng ký nhận diện khuôn mặt để mở khoá tủ này"
        lockerRegisterControl_numLocker.setTextColor(resources.getColor(R.color.white))
        lockerRegisterControl_message.text = textMessage
        lockerRegisterControl_numLocker.text = numLocker
        lockerRegisterControl_title.text = statusLocker
        Handler().postDelayed(mRunAfter,TIME_OUT)
    }
}