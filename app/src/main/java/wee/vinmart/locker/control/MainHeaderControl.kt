package wee.vinmart.locker.control

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.control_mainheader_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.utils.CommonAnimation

class MainHeaderControl : ConstraintLayout {
    //---
    private var mRootView: View?=null
    private var mListener: IMainHeaderControl?=null
    private var isProcessing = false
    //---
    constructor(context: Context):super(context)
    constructor(context: Context, attrs: AttributeSet):super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int):super(context, attrs, defStyleAttr)
    //---
    init {
        if(mRootView==null) {
            mRootView = inflate(context, R.layout.control_mainheader_layout, this)
            //---
            initBt()
        }
    }
    //----
    fun addListener(listener: IMainHeaderControl){
        mListener=listener
    }

    fun enableButton() {
        headerControl_btContainner.visibility=View.VISIBLE
    }

    fun disableButton(){
        headerControl_btContainner.visibility=View.GONE
    }
    fun changeLogoImage(id:Int){
        headerControl_logoImg.setImageDrawable(resources.getDrawable(id))
    }
    //-----
    private fun initBt(){
        headerControl_backBt.setOnClickListener {
            if(isProcessing) return@setOnClickListener
            isProcessing = true
            CommonAnimation().setTouchAnimationForView(it,400){
                mListener?.onBackBtClick()
                isProcessing = false
            }
        }

        headerControl_cancelBt.setOnClickListener {
            if(isProcessing) return@setOnClickListener
            isProcessing = true
            CommonAnimation().setTouchAnimationForView(it,400){
                mListener?.onCancelBtClick()
                isProcessing = false
            }

        }

        headerControl_logoImg.setOnClickListener {
            if(isProcessing) return@setOnClickListener
            isProcessing = true
            CommonAnimation().setTouchAnimationForView(it,400){
                isProcessing = false
                mListener?.onLogoClick()
            }

        }
    }

    interface IMainHeaderControl {
        fun onCancelBtClick()
        fun onBackBtClick()
        fun onLogoClick()
    }

}