package wee.vinmart.locker.control.slide

import wee.vinmart.locker.control.slide.VideoControllerDelegate

class AdvItem(
    val videoRes: Int? = null,
    val imageRes: Int? = null
) {
    var videoController: VideoControllerDelegate? = null

    val isImage get() = imageRes != null

    val isVideo get() = videoRes != null
}