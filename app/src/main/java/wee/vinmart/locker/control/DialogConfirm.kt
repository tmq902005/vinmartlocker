package wee.vinmart.locker.control

import android.app.Dialog
import android.content.Context
import kotlinx.android.synthetic.main.dialog_confirm_layout.*
import wee.vinmart.locker.R
import wee.vinmart.locker.utils.CommonAnimation

class DialogConfirm(context: Context, message: String, runOke:Runnable, runCancel:Runnable) : Dialog(context) {
    var isProcessing = false
    init {
        this.setContentView(R.layout.dialog_confirm_layout)
        val mWindow = this.window
        mWindow?.setBackgroundDrawableResource(android.R.color.transparent)
        this.setCanceledOnTouchOutside(false)
        dialogConfirm_message.text = message
        dialogConfirm_exitBtn.setOnClickListener {
            if(isProcessing) return@setOnClickListener
            isProcessing = true
            CommonAnimation().setTouchAnimationForView(it,300){
                this.dismiss()
                isProcessing = false
            }
        }
        dialogConfirm_yesBtn.setOnClickListener {
            if(isProcessing) return@setOnClickListener
            isProcessing = true
            CommonAnimation().setTouchAnimationForView(it,300){
                runOke.run().apply {
                    this@DialogConfirm.dismiss()
                    isProcessing = false
                }
            }
        }

        dialogConfirm_noBtn.setOnClickListener {
            if(isProcessing) return@setOnClickListener
            isProcessing = true
            CommonAnimation().setTouchAnimationForView(it, 300) {
                runCancel.run().apply {
                    this@DialogConfirm.dismiss()
                    isProcessing = false
                }
            }
        }
    }
}