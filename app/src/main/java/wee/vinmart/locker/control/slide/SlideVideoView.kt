package wee.vinmart.locker.control.slide

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View.OnTouchListener
import androidx.constraintlayout.widget.ConstraintLayout
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.slide_video_view.view.*
import wee.vinmart.locker.R
import java.util.concurrent.TimeUnit

class SlideVideoView : ConstraintLayout {

    private val advAdapter = AdvAdapter()

    private var mDisposable: Disposable? = null

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(context, attrs)
    }

    private fun initView(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.slide_video_view, this)

        advViewPager.isUserInputEnabled = false
        countDownNextSlide()
    }

    fun setListAdv(list: List<AdvItem>) {
        advAdapter.set(list)
        advAdapter.bindToViewPager(advViewPager)
        advAdapter.onPageChanged = {
            val i = advViewPager.currentItem
            if (advAdapter.get(i)?.isImage == true) {
                countDownNextSlide()
            }
        }
    }

    private fun countDownNextSlide() {
        mDisposable?.dispose()
        mDisposable = Observable.timer(5, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                mDisposable?.dispose()
                val i = advViewPager.currentItem + 1
                advViewPager.setCurrentItem(i, true)
            }, {})
    }

    fun pauseSlide() {
        mDisposable?.dispose()
        advAdapter.listItem.forEach {
            it.videoController?.also { videoController ->
                videoController.stopVideo()
            }
        }
    }

}