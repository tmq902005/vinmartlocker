package wee.vinmart.locker.control

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.intel.realsenseid.api.Faceprints
import kotlinx.android.synthetic.main.control_registerinfoadmin_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.utils.CommonAnimation

class RegisterInfoAdminControl : ConstraintLayout {

    private var avatar: ByteArray? = null

    private var isProcessing = false

    val name: String
        get() = registerInfoAdminControl_inputName.text.toString()

    val phone: String
        get() = registerInfoAdminControl_inputPhoneNumber.text.toString()

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        iniView(context, attrs)
    }

    private fun iniView(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.control_registerinfoadmin_layout, this)

        registerInfoAdminControl_registerBtn.setTextColor(R.color.pinkish_red_two)
        registerInfoAdminControl_registerBtn.setBackground(R.drawable.bg_white_corner5)
    }

    fun setData(frame: ByteArray){
        Glide.with(this)
            .load(frame)
            .skipMemoryCache(true)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .circleCrop()
            .into(registerInfoAdminControl_avatarImg)
        avatar = frame
    }

    fun setAvatarOnClick(onClickListener: OnClickListener){
        if(isProcessing) return
        isProcessing = true
        registerInfoAdminControl_avatarImg.setOnClickListener {
            CommonAnimation().setTouchAnimationForView(it,300){
                onClickListener.onClick(it).apply {
                    isProcessing = false
                }
            }
        }
    }

    fun actionRegisterBtn(onClickListener: OnClickListener){
        registerInfoAdminControl_registerBtn.setActionClick(onClickListener)
    }

    /*fun getData(): RegisterAdminInfo{
        val name = registerInfoAdminControl_inputName.text.toString()
        val phone = registerInfoAdminControl_inputPhoneNumber.text.toString()
        return RegisterAdminInfo(avatar,name,phone)
    }*/

    fun showLoading(){
        registerInfoAdminControl_registerBtn.showProcessing()
    }

    fun hideLoading(){
        registerInfoAdminControl_registerBtn.hideProcessing()
    }

    private fun clearAll(){

    }
    data class RegisterAdminInfo(val avatar: ByteArray?, val faceprints: Faceprints, val name: String, val phone: String)
}