package wee.vinmart.locker.control

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PorterDuff
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.ColorInt
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.control_button_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.utils.CommonAnimation


class ButtonControl : ConstraintLayout {

    private var isProcessing = false

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(context, attrs)
    }

    @SuppressLint("ResourceType")
    private fun initView(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.control_button_layout, this)

        val types =
            context.theme.obtainStyledAttributes(attrs, R.styleable.CustomView, 0, 0)

        val labelText = types.getString(R.styleable.CustomView_android_text)
        buttonControl_text.text = labelText

    }

    fun setText(text: String) {
        buttonControl_text.text = text
    }

    fun setTextColor(idColor: Int){
        buttonControl_text.setTextColor(resources.getColor(idColor))
    }

    fun setActionClick(onClickListener: OnClickListener) {
        if (isProcessing) return
        isProcessing = true
        buttonControl_container.setOnClickListener {
            CommonAnimation().setTouchAnimationForView(it,250){
                onClickListener.onClick(it)
                isProcessing = false
            }
        }
    }


    @SuppressLint("ResourceType")
    fun setBackground(idRes: Int, color: Int = R.color.white) {
        buttonControl_container.setBackgroundResource(idRes)
        buttonControl_container.backgroundTint(ContextCompat.getColor(context, color))
    }

    fun showProcessing() {
        buttonControl_processing.visibility = View.VISIBLE
        buttonControl_text.visibility = View.GONE
    }

    fun hideProcessing() {
        buttonControl_processing.visibility = View.GONE
        buttonControl_text.visibility = View.VISIBLE
    }

    fun View.backgroundTint(@ColorInt color: Int) {
        post {
            @Suppress("DEPRECATION")
            background?.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                background?.colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
            } else {

            }*/
        }
    }

}