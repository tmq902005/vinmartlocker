package wee.vinmart.locker.control

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.ViewPager
import com.daimajia.androidanimations.library.sliders.SlideOutLeftAnimator
import com.daimajia.slider.library.Animations.BaseAnimationInterface
import com.daimajia.slider.library.Indicators.PagerIndicator
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Transformers.BaseTransformer
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.nineoldandroids.animation.ObjectAnimator
import com.nineoldandroids.view.ViewHelper
import kotlinx.android.synthetic.main.control_slideimage_layout.view.*
import wee.vinmart.locker.R
import javax.xml.transform.Transformer
import kotlin.math.abs


class SlideImageControl : ConstraintLayout {

    private var mRootView: View?=null
    private var mActivity: Activity?=null
    //---
    constructor(context: Context):super(context)
    constructor(context: Context, attrs: AttributeSet):super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int):super(context, attrs, defStyleAttr)

    fun setActivity(activity: Activity,arrList: ArrayList<Int>){
        mActivity = activity
        initSlide(arrList)
    }

    //---
    init {
        if(mRootView==null) {
            mRootView = inflate(context, R.layout.control_slideimage_layout, this)
        }
    }

    private fun initSlide(arrList: ArrayList<Int>){

        /*val urlMaps = HashMap<String, String>()
        urlMaps["Hannibal"] = "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg"
        urlMaps["Big Bang Theory"] = "http://tvfiles.alphacoders.com/100/hdclearart-10.png"
        urlMaps["House of Cards"] = "http://cdn3.nflximg.net/images/3093/2043093.jpg"
        urlMaps["Game of Thrones"] = "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg"*/
        slideImageControl_slide.removeAllSliders()
        val fileMaps = HashMap<String, Int>()
        var i = 0
        for(item in arrList){
            i += 1
            fileMaps["Guide $i"] = item
        }


        for (name in fileMaps.keys) {
            val textSliderView = TextSliderView(mActivity!!)
            // initialize a SliderLayout
            textSliderView
                    .image(fileMaps[name]!!)
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener {
                    }

            //add your extra information
            textSliderView.bundle(Bundle())
            textSliderView.bundle
                    .putString("extra", name)

            slideImageControl_slide.addSlider(textSliderView)

            //slideAdsControl_slide.setPresetTransformer(FadeOutTransformation())
            slideImageControl_slide.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
            slideImageControl_slide.setCustomAnimation(DescriptionAnimation())
            slideImageControl_slide.setDuration(5000)
            //slideImageControl_slide.setPagerTransformer(true,FadeOutTransformation())
        }
    }

    fun onDestroyView(){
        slideImageControl_slide.removeAllSliders()
        slideImageControl_customIndicator.destroySelf()
        slideImageControl_slide.stopAutoCycle()
    }

    class DescriptionAnimation : BaseAnimationInterface {

        override fun onPrepareCurrentItemLeaveScreen(current: View) {
            val descriptionLayout = current.findViewById<View>(com.daimajia.slider.library.R.id.description_layout)
            if (descriptionLayout != null) {
                current.findViewById<View>(com.daimajia.slider.library.R.id.description_layout).visibility = View.INVISIBLE
            }
        }

        /**
         * When next item is coming to show, let's hide the description layout.
         * @param next
         */
        override fun onPrepareNextItemShowInScreen(next: View) {
            val descriptionLayout = next.findViewById<View>(com.daimajia.slider.library.R.id.description_layout)
            if (descriptionLayout != null) {
                next.findViewById<View>(com.daimajia.slider.library.R.id.description_layout).visibility = View.INVISIBLE
            }
        }


        override fun onCurrentItemDisappear(view: View) {

        }

        /**
         * When next item show in ViewPagerEx, let's make an animation to show the
         * description layout.
         * @param view
         */
        override fun onNextItemAppear(view: View) {

            val descriptionLayout = view.findViewById<View>(com.daimajia.slider.library.R.id.description_layout)
            if (descriptionLayout != null) {
                val layoutY = ViewHelper.getY(descriptionLayout)
                //view.findViewById<View>(com.daimajia.slider.library.R.id.description_layout).visibility = View.VISIBLE
                val animator = ObjectAnimator.ofFloat(
                        descriptionLayout, "y", layoutY + descriptionLayout.height,
                        layoutY).setDuration(500)
                //animator.start()
            }

        }
    }

    inner class FadeOutTransformation : BaseTransformer() {
        override fun onTransform(page: View?, position: Float) {
            page!!.translationX = -position * page.width

            page.alpha = 1 - abs(position)

        }

        public override fun isPagingEnabled(): Boolean {
            return true
        }
    }

}