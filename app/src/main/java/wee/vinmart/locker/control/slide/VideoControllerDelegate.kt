package wee.vinmart.locker.control.slide

interface VideoControllerDelegate {

    fun playVideo()

    fun stopVideo()

}