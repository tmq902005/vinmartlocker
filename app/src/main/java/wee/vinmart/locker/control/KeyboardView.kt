package wee.vinmart.locker.control

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.keyboard_view.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.adapter.KeyboardAdapter

class KeyboardView : ConstraintLayout {

    var listener: KeyboardListener? = null

    private val adapter = KeyboardAdapter()

    private val listKey = listOf<Any?>(
        "1", "2", "3",
        "4", "5", "6",
        "7", "8", "9",
        null, "0", R.drawable.drw_key_delete
    )

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        initView(context, attributeSet)
    }

    private fun initView(context: Context, attributeSet: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.keyboard_view, this)
        bindAdapter()
    }

    private fun bindAdapter() {
        adapter.set(listKey)
        adapter.bind(keyboardViewRecycler, 3)
        adapter.onItemTouchClick { any, i ->
            any ?: return@onItemTouchClick
            listener?.onResult(any, i)
        }
    }

    interface KeyboardListener {
        fun onResult(model: Any, position: Int)
    }

}