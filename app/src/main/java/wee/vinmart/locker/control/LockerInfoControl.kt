package wee.vinmart.locker.control

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.control_locker_info_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.utils.getDate
import wee.vinmart.locker.vm.CabinetVM
import java.util.*


class LockerInfoControl : ConstraintLayout {

    private var mData = LockerControl.LockerData()

    private var updateTimer = Timer()

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView(context, attrs)
    }

    private fun initView(context: Context, attrs: AttributeSet) {
        LayoutInflater.from(context).inflate(R.layout.control_locker_info_layout, this)
    }

    fun setData(bioId: String, lockerData: LockerControl.LockerData) {
        resetData()
        mData = lockerData
        lockerInfoControl_lockerNo.text = mData.ID
        if(bioId.isNotEmpty()) {
            lockerInfoControl_openBtn.visibility = View.VISIBLE
            lockerInfoControl_resetBtn.visibility = View.VISIBLE
            lockerInfoControl_infoContainer.visibility = View.VISIBLE
            lockerInfoControl_emptyContainer.visibility = View.GONE
            CabinetVM.ins.getUserAvatar(bioId, object : CabinetVM.GetAvatarUserListener {
                override fun onResult(avatar: ByteArray?) {
                    Handler(Looper.getMainLooper()).post {
                        Glide.with(this@LockerInfoControl)
                            .load(avatar)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .circleCrop()
                            .into(lockerInfoControl_avatar)
                    }
                }

            })

            lockerInfoControl_timeReg.text = getDate(mData.Times*1000, "HH:MM")
            setFormatInTime()
        }
    }

    fun setResetBtn(onClickListener: OnClickListener){
        lockerInfoControl_resetBtn.visibility = View.VISIBLE
        lockerInfoControl_resetBtn.setActionClick(onClickListener)
        lockerInfoControl_resetBtn.setText("RESET TỦ")
        lockerInfoControl_resetBtn.setTextColor(R.color.white)
        lockerInfoControl_resetBtn.setBackground(R.drawable.bg_pinkish_red_corner5)
    }

    fun setOpenBtn(onClickListener: OnClickListener){
        lockerInfoControl_openBtn.visibility = View.VISIBLE
        lockerInfoControl_openBtn.setActionClick(onClickListener)
        lockerInfoControl_openBtn.setText("MỞ TỦ")
        lockerInfoControl_openBtn.setTextColor(R.color.white)
        lockerInfoControl_openBtn.setBackground(R.drawable.bg_pinkish_red_corner5)
    }

    private fun setFormatInTime(){
        updateTimer.schedule(object : TimerTask() {
            override fun run() {
                try {
                    val mills = System.currentTimeMillis() - (mData.Times*1000)
                    val h = mills / (1000 * 60 * 60)
                    val m = (mills / (1000 * 60) % 60).toInt()
                    //var diff = "${if(h<10)"0$h" else "$h"}:${if(m<10)"0$m" else "$m"}"
                    val diff = if(h>0){
                       "$h giờ $m phút"
                    }else{
                        "$m phút"
                    }

                    Handler(Looper.getMainLooper()).post {
                        if(h>=3) lockerInfoControl_timeHas.setTextColor(resources.getColor(R.color.pinkish_red_two))
                        lockerInfoControl_timeHas.text = diff
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }, 0, 30000)
    }

    fun resetData(){
        mData = LockerControl.LockerData()
        lockerInfoControl_avatar.setImageResource(R.drawable.ic_empty_avatar)
        lockerInfoControl_emptyContainer.visibility = View.VISIBLE
        lockerInfoControl_infoContainer.visibility = View.GONE
        updateTimer.cancel()
        updateTimer = Timer()
        lockerInfoControl_resetBtn.visibility = View.GONE
        lockerInfoControl_openBtn.visibility = View.VISIBLE
    }

}