package wee.vinmart.locker.control

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.lucasr.twowayview.TwoWayLayoutManager
import org.lucasr.twowayview.widget.StaggeredGridLayoutManager
import wee.vinmart.locker.utils.CommonAnimation
import wee.vinmart.locker.vm.pIsShowTime

class ListLockersControl : RecyclerView {

    private var mListLockerData = arrayListOf<LockerControl.LockerData>()
    var onItemClickListener:OnItemClickListener?=null
    private var mItemSelected: LockerControl.LockerData?=null
    var isProcessing = false
    private var mHandler = Handler()
    constructor(context: Context):super(context)
    constructor(context: Context, attrs: AttributeSet):super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int):super(context, attrs, defStyleAttr)

    init {
        setHasFixedSize(true)
        val managerLayout= StaggeredGridLayoutManager(TwoWayLayoutManager.Orientation.HORIZONTAL,6,10)
        layoutManager=managerLayout
    }

    fun setData(listLockerData: ArrayList<LockerControl.LockerData>){
        mListLockerData.clear()
        destroy()
        mListLockerData.add(LockerControl.LockerData("", LockerControl.SCREEN,"","",0L,1))
        /*for(item in listLockerData){
            if(item.Type!=LockerControl.SELECTED){
                item.Type = LockerControl.READY
            }
        }*/
        mListLockerData.addAll(listLockerData)
        setItemViewCacheSize(mListLockerData.size)
        adapter =ListLockersAdapter(object : OnItemClickListener{
            override fun onClick(view: View, lockerData: LockerControl.LockerData) {
                if(isProcessing) return
                if(lockerData.Type==LockerControl.SCREEN) return
                if(lockerData.Type==LockerControl.USED && !pIsShowTime) return
                isProcessing = true
                CommonAnimation().setTouchAnimationForView(view,150){
                    for(item in mListLockerData){
                        if(item.Type==LockerControl.SELECTED){
                            item.Type = LockerControl.READY
                        }
                        /*if(mItemSelected!=null){
                            if(item.ID == mItemSelected!!.ID){
                                item.Type = LockerControl.READY
                            }
                        }*/
                        if(!pIsShowTime) {
                            if (item.ID == lockerData.ID) {
                                item.Type = LockerControl.SELECTED
                            }
                        }
                    }
                    mItemSelected = lockerData
                    Log.e("Item Click",mItemSelected.toString())
                    adapter?.notifyDataSetChanged().apply {
                        Handler().postDelayed({
                            onItemClickListener?.onClick(view,lockerData)
                        },150)
                    }

                }

            }

        })
    }

    fun updateData(listLockerData: ArrayList<LockerControl.LockerData>){
        mListLockerData.clear()
        mListLockerData.add(LockerControl.LockerData("", LockerControl.SCREEN,"","",0L,1))
        mListLockerData.addAll(listLockerData)
        adapter?.notifyDataSetChanged()
        setItemViewCacheSize(mListLockerData.size)
    }

    fun destroy(){
        if(pIsShowTime){
            mHandler.removeCallbacksAndMessages(null)
        }
    }

    private inner class ListLockersAdapter(val onItemClickListener:OnItemClickListener) :Adapter<ListLockerViewHolder>(){
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ListLockerViewHolder {
            val layoutView = LockerControl(context)
            return ListLockerViewHolder(layoutView,onItemClickListener)
        }

        override fun getItemCount(): Int {
            return mListLockerData.size
        }

        override fun onBindViewHolder(parent: ListLockerViewHolder, position: Int) {
            parent.setData(mListLockerData[position])
            val itemView = parent.itemView
            val itemData = mListLockerData[position]
            val lp: StaggeredGridLayoutManager.LayoutParams
            if(itemData.Type == LockerControl.SCREEN){
                lp=StaggeredGridLayoutManager.LayoutParams(220, 440).apply {
                    setMargins(140,4,4,4)
                }
                lp.span = 4
                itemView.layoutParams = lp
            }else{

                lp = StaggeredGridLayoutManager.LayoutParams(220, 220).apply {
                    when {
                        itemData.ID.toInt()<4 -> setMargins(140, 4, 4, 4)
                        itemData.ID.toInt() in 24..28 -> setMargins(4, 4, 140, 4)
                        else -> setMargins(4, 4, 4, 4)
                    }
                }
                lp.span = if(itemData.Size==0) 1 else 2
                itemView.layoutParams = lp
            }
        }


    }
    inner class ListLockerViewHolder(itemView: LockerControl, onItemClickListener: OnItemClickListener) : ViewHolder(itemView){
        private var mLockerData: LockerControl.LockerData?=null
        private var mLockerControl = itemView
       init {
           mLockerControl.addOnClickListener(OnClickListener {
               onItemClickListener.onClick(itemView,mLockerData!!)
           })
       }
        fun setData(lockerData:LockerControl.LockerData){
            mLockerControl.setData(lockerData, pIsShowTime, mHandler)
            mLockerData = lockerData
        }
    }

    interface OnItemClickListener{
        fun onClick(view:View,lockerData:LockerControl.LockerData)
    }
}