package wee.vinmart.locker.control

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.button_view.view.*
import kotlinx.android.synthetic.main.control_button_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.utils.CommonAnimation

class ButtonView : ConstraintLayout {

    private var isProcessing = false

    constructor(context: Context, attributeSet: AttributeSet?): super(context, attributeSet){
        initView(context, attributeSet)
    }

    private fun initView(context: Context, attributeSet: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.button_view, this)

        val types =
            context.theme.obtainStyledAttributes(attributeSet, R.styleable.CustomView, 0, 0)

        val labelText = types.getString(R.styleable.CustomView_android_text)
        buttonViewLabel.text = labelText

    }

    fun setActionClick(onClickListener: OnClickListener){
        if(isProcessing) return
        isProcessing = true
        buttonViewParent.setOnClickListener {
            CommonAnimation().setTouchAnimationForView(it,250){
                onClickListener.onClick(it)
                isProcessing = false
            }
        }
    }

}