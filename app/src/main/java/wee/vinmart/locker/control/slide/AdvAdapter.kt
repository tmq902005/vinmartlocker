package wee.vinmart.locker.control.slide

import android.annotation.SuppressLint
import android.view.View
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import kotlinx.android.synthetic.main.item_adv_image.view.*
import kotlinx.android.synthetic.main.item_adv_video.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.adapter.BaseRecyclerAdapter

class AdvAdapter : BaseRecyclerAdapter<AdvItem>() {

    /**
     * Current of viewpager position adapt this adapter
     */
    var currentPosition: Int = -1

    var onPageChanged: () -> Unit = {}

    override fun getItemCount(): Int {
        return listItem.size * 10000000
    }

    override fun get(position: Int): AdvItem? {
        if (listItem.isEmpty()) return null
        val realPosition = position % listItem.size
        if (realPosition !in 0..lastIndex) return null
        return listItem[realPosition]
    }

    override fun layoutResource(model: AdvItem, position: Int): Int {
        return when {
            model.imageRes != null -> R.layout.item_adv_image
            model.videoRes != null -> R.layout.item_adv_video
            else -> 0
        }
    }

    override fun View.onBindModel(model: AdvItem, position: Int, layout: Int) {
        currentPosition = position
        when {
            model.imageRes != null -> {
                onBindImage(model)
            }
            model.videoRes != null -> {
                onBindVideo(model)
            }
        }
    }

    private fun View.onBindImage(model: AdvItem) {
        advImageView.setImageResource(model.imageRes!!)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun View.onBindVideo(model: AdvItem) {
        model.videoController?.also {
            it.stopVideo()
        }
        model.videoController = VideoController(playerView, model.videoRes!!).also {
            it.onVideoStopped = {
                viewPager?.also { viewPager ->
                    viewPager.currentItem = viewPager.currentItem + 1
                }
            }
            it.playVideo()
        }
    }

    /**
     * ViewHolder util
     */
    private var viewPager: ViewPager2? = null

    fun bindToViewPager(viewPager: ViewPager2) {
        this.viewPager = viewPager
        viewPager.adapter = this
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(state: Int) {
                when (state) {
                    ViewPager.SCROLL_STATE_DRAGGING -> {
                    }
                    ViewPager.SCROLL_STATE_IDLE -> {
                        onPageChanged()
                        if (currentPosition != -1) {
                            get(currentPosition)?.videoController?.stopVideo()
                        }
                        currentPosition = viewPager.currentItem
                        get(currentPosition)?.videoController?.playVideo()
                    }
                }
            }
        })
    }

}