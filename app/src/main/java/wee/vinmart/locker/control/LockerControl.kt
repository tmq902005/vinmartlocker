package wee.vinmart.locker.control

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.control_locker_layout.view.*
import wee.vinmart.locker.R


@Suppress("DEPRECATION")
class LockerControl : ConstraintLayout {
    companion object{
        const val READY = 1
        const val USED = 2
        const val SELECTED = 3
        const val SCREEN = 4

        const val BG_READY = R.drawable.bg_red_stroke2_corner5
        const val BG_USED = R.drawable.bg_pinkish_red_corner5
        const val BG_SELECTED = R.drawable.bg_goldenrod_corner5
    }
    private var isProcessing = false
    private var mRootView: View?=null
    private var mData = LockerData()
    private var mHandler: Handler?=null
    private var mRunUpdateTime = Runnable {
        updateTime()
    }
    //---
    constructor(context: Context):super(context)
    constructor(context: Context, attrs: AttributeSet):super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int):super(context, attrs, defStyleAttr)
    //---
    init {
        if(mRootView==null) {
            mRootView = inflate(context, R.layout.control_locker_layout, this)
            setActionClick()
        }
    }

    /*override fun onDetachedFromWindow() {
        updateTimer.cancel()
        timerTask.cancel()
        super.onDetachedFromWindow()
    }*/

    fun setData(lockerData: LockerData, isShowTime: Boolean, handler: Handler){
        mHandler = handler
        mData = lockerData
        setText(lockerData.ID)
        when(lockerData.Type){
            READY -> {
                setBackground(BG_READY)
                lockerControl_title.setTextColor(resources.getColor(R.color.pinkish_red_two))
            }
            USED -> {
                setBackground(BG_USED)
                lockerControl_title.setTextColor(resources.getColor(R.color.white))
                if(isShowTime) {
                    lockerControl_time.visibility = View.VISIBLE
                    setFormatInTime()
                }
            }
            SELECTED -> {
                setBackground(BG_SELECTED)
                lockerControl_title.setTextColor(resources.getColor(R.color.pinkish_red_two))
            }
            SCREEN -> {
                setBackground(BG_USED)
                lockerControl_screenContainer.visibility = View.VISIBLE
                lockerControl_title.visibility = View.GONE
            }
        }
    }

    private fun setFormatInTime(){
        lockerControl_time.setTextColor(resources.getColor(R.color.white))
        lockerControl_time.setBackgroundResource(R.drawable.bg_white_stroke2_corner5)
        //updateTimer.schedule(timerTask, 3000, 30000)
        updateTime()
    }

    fun updateTime(){
        val mills = System.currentTimeMillis() - (mData.Times*1000)
        val h = mills / (1000 * 60 * 60)
        if(h>6) setFormatOutTime()
        val m = (mills / (1000 * 60) % 60).toInt()
        val diff = "${if(h<10)"0$h" else "$h"}:${if(m<10)"0$m" else "$m"}"
        lockerControl_time.text = diff
        mHandler?.removeCallbacks(mRunUpdateTime)
        mHandler?.postDelayed(mRunUpdateTime,60000)
    }

    private fun setFormatOutTime(){
        lockerControl_time.setTextColor(resources.getColor(R.color.white))
        lockerControl_time.setBackgroundResource(R.drawable.bg_marigold_corner5)
    }

    private fun setText(text:String){
        lockerControl_title.text = text
    }

    private fun setActionClick(){
        if(isProcessing) return
        isProcessing = true
        /*lockerControl_container.setOnClickListener {
            pOnLockerClickListener?.onResult(mData)
            if(mData.Type!= USED) {
                CommonAnimation().setTouchAnimationForView(it, 150) {
                    pOnLockerClickSelectListener?.onResult(mData)
                    isProcessing = false
                }
            }else{
                isProcessing = false
            }
        }*/
    }

    fun addOnClickListener(onClickListener: OnClickListener){
        lockerControl_container.setOnClickListener(onClickListener)
    }

    private fun setBackground(idRes: Int) {
        lockerControl_container.setBackgroundResource(idRes)
    }

    fun stopCountTime() {
        mHandler?.removeCallbacks(mRunUpdateTime)
    }


    data class LockerData(
        var ID: String = "",
        var Type: Int = 4,
        var Code: String = "",
        var UsedID: String = "",
        var Times: Long = 0L,
        var Size: Int = 1
    ) {
//        constructor():this(
//            ID = "01",
//            Type = READY,
//            Code = "1",
//            UsedID = "",
//            Times = 0,
//            Size = 1
//        )
    }

    interface OnLockerClickListener {
        fun onResult(data: LockerData)
    }
}