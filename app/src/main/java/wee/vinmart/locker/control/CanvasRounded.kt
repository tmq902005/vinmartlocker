package wee.vinmart.locker.control

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import wee.vinmart.locker.R

class CanvasRounded @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    var color = R.color.green
        set(value) {
            field = value
            invalidate()
        }

    var start = 0f
        set(value) {
            field = value
            invalidate()
        }

    var end = 0f
        set(value) {
            field = value
            invalidate()
        }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val painLineCheck = createPaint(context, color)
        val ovalLineCheck = createOval(painLineCheck, width / 2f, height / 2f, width / 2.5f)
        canvas.drawArc(ovalLineCheck, start, end, false, painLineCheck)
    }

    private fun createPaint(context: Context, @ColorRes color: Int = R.color.white): Paint {
        val paint = Paint()
        paint.color = ContextCompat.getColor(context, color)
        paint.strokeWidth = 9f
        paint.style = Paint.Style.STROKE
        paint.strokeCap = Paint.Cap.ROUND
        return paint
    }

    private fun createOval(
        paint: Paint,
        centerX: Float,
        centerY: Float,
        radiusTopLeft: Float
    ): RectF {
        val oval = RectF()
        paint.style = Paint.Style.STROKE
        oval.set(
            centerX - radiusTopLeft,
            centerY - radiusTopLeft,
            centerX + radiusTopLeft,
            centerY + radiusTopLeft
        )
        return oval
    }

}