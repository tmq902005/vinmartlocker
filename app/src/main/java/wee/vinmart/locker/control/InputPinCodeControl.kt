package wee.vinmart.locker.control

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.TextView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_welcome.*
import kotlinx.android.synthetic.main.control_inputpincode_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.utils.CommonAnimation
import java.lang.Exception


class InputPinCodeControl : ConstraintLayout, KeyboardView.KeyboardListener {

    private var mPinCode = ""

    private var mListPinCode = arrayListOf<TextView>()

    var onPinCodeListener: OnPinCodeListener? = null

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(context, attrs)
    }

    private fun initView(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.control_inputpincode_layout, this)
        mListPinCode = arrayListOf(
            inputPinCode_text1, inputPinCode_text2, inputPinCode_text3,
            inputPinCode_text4, inputPinCode_text5, inputPinCode_text6
        )
        actWelcomeKey?.listener = this
        clearAll()
        /*inputPinCodeControl_input.inputType = InputType.TYPE_CLASS_NUMBER
        inputPinCodeControl_input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (text.isNullOrEmpty()) {
                    clearAll()
                    return
                }
                mPinCode = text.toString()

            }

        })*/
    }

    fun pushKey(key: String) {
        mPinCode += key
        updateView()
    }

    fun removeKey() {
        try {
            if (mPinCode.isEmpty()) return
            mPinCode = mPinCode.substring(0, mPinCode.length - 1)
            updateView()
        } catch (e: Exception) {
            updateView()
        }
    }

    private fun updateView() {
        if (mPinCode.isEmpty()) {
            clearAll()
            return
        }
        if (mPinCode.length < 6) {
            clearShowPinCode()
            for (index in 0 until mPinCode.length) {
                mListPinCode[index].text = mPinCode[index].toString()
            }
        } else if (mPinCode.length == 6) {
            for (index in 0 until mPinCode.length) {
                mListPinCode[index].text = mPinCode[index].toString()
            }
            Handler(Looper.getMainLooper()).postDelayed({
                onPinCodeListener?.onResult(checkPinCode())
            }, 400)
        }
    }

    private fun checkPinCode(): Boolean {
        return if (mPinCode == "696969") {
            true
        } else {
            CommonAnimation().setAnimShake(inputPinCodeControl_pinCodeContainer) {
                clearAll()
            }
            false
        }
    }

    private fun clearShowPinCode() {
        for (item in mListPinCode) {
            item.text = ""
        }
    }

    private fun clearAll() {
        clearShowPinCode()
        mPinCode = ""
    }

    fun onBackClick(block: () -> Unit) {
        inputCodeBack.setOnClickListener { block() }
    }

    /**
     * listener keyboard click
     */
    override fun onResult(model: Any, position: Int) {
        when (model) {
            is String -> pushKey(model)
            is Int -> removeKey()
            else -> ""
        }

    }

    interface OnPinCodeListener {
        fun onResult(boolean: Boolean)
    }

}