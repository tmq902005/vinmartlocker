package wee.vinmart.locker.control

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbDeviceConnection
import android.hardware.usb.UsbManager
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.hoho.android.usbserial.driver.UsbSerialDriver
import com.hoho.android.usbserial.driver.UsbSerialPort
import com.hoho.android.usbserial.driver.UsbSerialProber
import com.hoho.android.usbserial.util.SerialInputOutputManager
import io.reactivex.Single
import io.reactivex.SingleObserver
import wee.vinmart.locker.app.toast
import wee.vinmart.locker.data.Cabinet
import wee.vinmart.locker.vm.CabinetVM


class UsbConnectionControlV2(private val context: Context) : SerialInputOutputManager.Listener {
    companion object {
        const val PERMISSION = ".SERIAL_PERMISSION"
    }
    private val manager: UsbManager = context.getSystemService(Context.USB_SERVICE) as UsbManager
    private var port: UsbSerialPort? = null
    private var device: UsbDevice? = null
    private var usbIoManager: SerialInputOutputManager? = null
    private val mHandler: Handler by lazy {
        Handler(Looper.getMainLooper())
    }
    private var usbReceiver: BroadcastReceiver? = null

    private val intentFilter: IntentFilter by lazy {
        IntentFilter(PERMISSION).also {
            it.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED)
            it.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED)
        }
    }

    private fun usbReceiver(permissionGranted: () -> Unit): BroadcastReceiver {
        return object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val usb = intent.getParcelableExtra<UsbDevice>(UsbManager.EXTRA_DEVICE)
                if (usb?.vendorId != device?.vendorId) return
                if (intent.action === UsbManager.ACTION_USB_DEVICE_DETACHED) return
                if (intent.action === UsbManager.ACTION_USB_DEVICE_ATTACHED) {
                    if (manager.hasPermission(usb)) {
                        permissionGranted()
                    } else {
                        requestPermission(usb)
                    }
                }
                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    permissionGranted()
                }
            }
        }
    }

    fun requestPermission(usb: UsbDevice?, permissionGranted: () -> Unit = {}) {
        if (manager.hasPermission(usb)) {
            permissionGranted()
            return
        }

        if (usbReceiver == null) {
            usbReceiver = usbReceiver(permissionGranted).also {
                context.registerReceiver(it, intentFilter)
            }
        }

        usb ?: return
        val permissionIntent = PendingIntent.getBroadcast(context, 1111, Intent(PERMISSION), 0)
        manager.requestPermission(usb, permissionIntent)
    }

    fun openConnect(block: () -> Unit = {}) {
        if (port != null) {
            block()
            return
        }
        val availableDrivers: List<UsbSerialDriver> =
            UsbSerialProber.getDefaultProber().findAllDrivers(manager)
        if (availableDrivers.isEmpty()) {
            Log.e("UsbConnectionControlV2", "No Available Driver")
            return
        }

        // Open a connection to the first available driver.
        val driver: UsbSerialDriver = availableDrivers[0]
        val connection: UsbDeviceConnection? =  try{
            manager.openDevice(driver.device)
        }catch (e :Exception){
            e.printStackTrace()
            null
        }
        device = driver.device
        if (connection == null) {
            // add UsbManager.requestPermission(driver.getDevice(), ..) handling here
            Log.e(
                "UsbConnectionControlV2",
                "${driver.device.deviceName}:${driver.device.deviceId} - No permission"
            )
            requestPermission(device){
                openConnect(block)
            }
            return
        }

        port = driver.ports[0] // Most devices have just one port (port 0)
        port?.open(connection)
        port?.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE)
        usbIoManager = SerialInputOutputManager(port, this)
        usbIoManager?.start()
        block()
        Log.e("UsbConnectionControlV2", "Open")
    }

    private var listCabinet = arrayListOf<Cabinet>()
    private var isReset = false

    private val runOpen =  object :Runnable {
        override fun run() {
            if(listCabinet.isNotEmpty()){
                val cabinet = listCabinet.first()
                listCabinet.remove(cabinet)
                write(cabinet.cabinetId)
                if(isReset) CabinetVM.ins.resetLocker(cabinet)
                mHandler.postDelayed(this,400)
            }else{
                isReset = false
            }
        }
    }

    fun startOpenAll(isReset: Boolean){
        listCabinet = CabinetVM.ins.getAllCabinet()
        this.isReset = isReset
        mHandler.post(runOpen)
    }

    fun write(str: String) {
        try {
            openConnect {
                port?.write(str.toByteArray(), 1000)
            }

        } catch (e: Exception) {
            e.printStackTrace()
            toast(e.message)
        }
        Log.e("UsbConnectionControlV2", "Req: $str")
    }

    fun closeConned() {
        usbIoManager?.stop()
        port?.close()
        port = null
        Log.e("UsbConnectionControlV2", "Close")
    }

    override fun onNewData(data: ByteArray?) {
        Log.e("UsbConnectionControlV2", "Resp: ${if (data != null) String(data) else "null"}")
    }

    override fun onRunError(e: Exception?) {
        Log.e("UsbConnectionControlV2", "Error: ${e?.printStackTrace()}")
        toast("${e?.message}")
    }
}
