package wee.vinmart.locker.control

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PorterDuff
import android.os.Build
import android.text.InputFilter
import android.text.InputType
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.ColorInt
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.app_input_layout.view.*
import wee.vinmart.locker.R

class AppInputView : ConstraintLayout {

    var text: String?
        get() = inputEdittext.text.toString()
        set(value) {
            inputEdittext.setText(value)
        }

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        initView(context, attributeSet)
    }

    private fun initView(context: Context, attributeSet: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.app_input_layout, this)

        val types =
            context.theme.obtainStyledAttributes(attributeSet, R.styleable.CustomView, 0, 0)

        val labelText = types.getString(R.styleable.CustomView_android_text)
        text = labelText

        val hintLabel = types.getString(R.styleable.CustomView_android_hint)
        inputEdittext.hint = hintLabel

        val icon = types.getResourceId(R.styleable.CustomView_android_src, R.drawable.ic_back)
        inputIcon.setImageResource(icon)

        val myInputType =
            types.getInt(R.styleable.CustomView_android_inputType, InputType.TYPE_CLASS_TEXT)
        inputEdittext.inputType = myInputType

        val length = types.getInt(R.styleable.CustomView_android_maxLength, 40)
        inputEdittext.filters = arrayOf(InputFilter.LengthFilter(length))

        inputEdittext.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                viewFocus()
            } else {
                viewUnFocus()
            }
        }

        viewUnFocus()
    }

    @SuppressLint("ResourceType")
    private fun viewFocus() {
        inputIcon.setColorFilter(
            ContextCompat.getColor(context, R.color.white),
            PorterDuff.Mode.MULTIPLY
        )
        inputParent.setBackgroundResource(R.drawable.bg_input_focus)
    }

    @SuppressLint("ResourceType")
    private fun viewUnFocus() {
        inputIcon.setColorFilter(
            ContextCompat.getColor(context, R.color.white30),
            PorterDuff.Mode.MULTIPLY
        )
        inputParent.setBackgroundResource(R.drawable.bg_input_unfocus)
    }

    private fun View.backgroundTint(@ColorInt color: Int) {
        post {
            @Suppress("DEPRECATION")
            background?.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                background?.colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
            } else {

            }*/
        }
    }

}