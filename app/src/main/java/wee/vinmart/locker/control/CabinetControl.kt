package wee.vinmart.locker.control

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.control_cabinet_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.vm.*
import java.util.*
import kotlin.collections.ArrayList


class CabinetControl : ConstraintLayout {
    private var isProcessing = false
    private var mRootView: View?=null
    private var mTimer = Timer()
    private var mHandler = Handler()
    private var mListLockerControl = arrayListOf<LockerControl>()
    var onCabinetControlListener: OnCabinetControlListener?=null
    private var selected: LockerControl.LockerData? =null
    //---
    constructor(context: Context):super(context)
    constructor(context: Context, attrs: AttributeSet):super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int):super(context, attrs, defStyleAttr)
    //---
    init {
        if(mRootView==null) {
            mRootView = inflate(context, R.layout.control_cabinet_layout, this)
            mListLockerControl = arrayListOf(
                cabinetControl_locker01, cabinetControl_locker02,cabinetControl_locker03,
                cabinetControl_locker04, cabinetControl_locker05, cabinetControl_locker06,
                cabinetControl_locker07, cabinetControl_locker08,cabinetControl_locker09,
                cabinetControl_locker10, cabinetControl_locker11, cabinetControl_locker12,
                cabinetControl_locker13, cabinetControl_locker14,cabinetControl_locker15,
                cabinetControl_locker16, cabinetControl_locker17, cabinetControl_locker18)
        }
        pOnLockerClickSelectListener = null
        pOnLockerClickListener = null
    }

    fun updateData(){
        if(pListLockerData.isEmpty()) {
            CabinetVM.ins.updateInfoCabinet(object : CabinetVM.UpdateCabinetInfoListener{
                override fun onResult(isSuccess: Boolean, listLockerData: ArrayList<LockerControl.LockerData>, mess:String) {
                    Handler(Looper.getMainLooper()).post {
                        updateUI()
                    }
                }
            })
        }else{
            for(i  in 0 until mListLockerControl.size){
                mListLockerControl[i].setData(pListLockerData[i], pIsShowTime,mHandler)
            }
            updateUI()
        }
    }

    private fun selectLocker(lockerData: LockerControl.LockerData){
        val index = pListLockerData.indexOf(lockerData)
        clearSelected()
        pListLockerData[index].Type = LockerControl.SELECTED
        updateUI()
    }

    private fun clearSelected(){
        for(item in pListLockerData){
            if(item.Type==LockerControl.SELECTED){
                item.Type = LockerControl.READY
            }
        }
    }

    private fun updateUI(){
        for(i in 0 until pListLockerData.size){
            mListLockerControl[i].setData(pListLockerData[i], pIsShowTime,mHandler)
        }
    }

    fun listenerLockerSelect(){
        pOnLockerClickListener = null
        pOnLockerClickSelectListener = object : LockerControl.OnLockerClickListener{
            override fun onResult(data: LockerControl.LockerData) {
                if(isProcessing) return
                isProcessing = true
                selected = data
                onCabinetControlListener?.onSelected(data)
                selectLocker(data).apply {
                    isProcessing = false
                }
            }

        }
    }

    fun listenerLockerClick(){
        pOnLockerClickSelectListener = null
        pOnLockerClickListener = object : LockerControl.OnLockerClickListener{
            override fun onResult(data: LockerControl.LockerData) {
                if(isProcessing) return
                isProcessing = true
                selected = data
                onCabinetControlListener?.onSelected(data).apply {
                    isProcessing = false
                }
            }

        }
    }

    fun getValue():LockerControl.LockerData?{
        return selected
    }

    interface OnCabinetControlListener{
        fun onSelected(data: LockerControl.LockerData)
    }

    fun destroy(){
        for(i  in 0 until mListLockerControl.size){
            mListLockerControl[i].stopCountTime()
        }
    }

}