package wee.vinmart.locker

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_loading.*
import kotlinx.android.synthetic.main.activity_welcome.*
import wee.vinmart.locker.control.InputPinCodeControl
import wee.vinmart.locker.control.KeyboardView
import wee.vinmart.locker.control.LockerControl
import wee.vinmart.locker.control.slide.AdvItem
import wee.vinmart.locker.utils.gone
import wee.vinmart.locker.utils.hide
import wee.vinmart.locker.utils.show
import wee.vinmart.locker.vm.*

class WelcomeActivity : WeeActivity(), InputPinCodeControl.OnPinCodeListener {

    private var isProcessing = false

    private val mHandler: Handler by lazy {
        Handler(mainLooper)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        initUI()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initUI() {
        pAdminData = ""

        if (CabinetVM.ins.getListUsedCabinet().isNullOrEmpty()) {
            actWelcomeRootButton.hide()
//            actWelcome_sendMoreBtn.setBackground(R.drawable.bg_cabinet, R.color.white_gray)
//            actWelcome_getBtn.setBackground(R.drawable.bg_cabinet, R.color.white_gray)
            actWelcome_sendMoreBtn.setOnClickListener {  }
            actWelcome_getBtn.setOnClickListener {  }
        } else {
            actWelcomeRootButton.show()
            actWelcome_sendMoreBtn.setBackground(R.drawable.bg_cabinet, R.color.red_gym)
            actWelcome_getBtn.setBackground(R.drawable.bg_cabinet, R.color.red_gym)
        }
        actWelcome_versionName.text = BuildConfig.VERSION_NAME
        actWelcome_pinCodeControl.also {
            it.onPinCodeListener = this
            it.onBackClick { actWelcome_mainContainer.transitionToStart() }
        }
        mHandler.postDelayed({ hideKeyboard(this) }, 200)
        actWelcome_sendBtn.setActionClick(View.OnClickListener {
            if (isProcessing) return@OnClickListener
            isProcessing = true
            pIsRemove = false
            CabinetVM.ins.updateInfoCabinet(object : CabinetVM.UpdateCabinetInfoListener {
                override fun onResult(
                    isSuccess: Boolean,
                    listLockerData: ArrayList<LockerControl.LockerData>,
                    mess: String
                ) {
                    runOnUiThread {
                        if (listLockerData.isNotEmpty()) {
                            gotoCabinetActivity()
                        } else {
                            isProcessing = false
                            Toast.makeText(
                                this@WelcomeActivity,
                                "Không có dữ liệu tủ",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                }

            })

        })
        actWelcome_sendMoreBtn.setActionClick(View.OnClickListener {
            if (isProcessing) return@OnClickListener
            isProcessing = true
            pIsRemove = false
            CabinetVM.ins.updateInfoCabinet(object : CabinetVM.UpdateCabinetInfoListener {
                override fun onResult(
                    isSuccess: Boolean,
                    listLockerData: ArrayList<LockerControl.LockerData>,
                    mess: String
                ) {
                    runOnUiThread {
                        if (listLockerData.isNotEmpty()) {
                            var isUsed = false
                            for (locker in listLockerData) {
                                if (locker.Type == LockerControl.USED) {
                                    isUsed = true
                                    break
                                }
                            }
                            if (isUsed) {
                                gotoVerifyFaceActivity()
                            } else {
                                Toast.makeText(this@WelcomeActivity, "Tủ trống", Toast.LENGTH_SHORT)
                                    .show()
                                isProcessing = false
                            }
                        } else {
                            Toast.makeText(
                                this@WelcomeActivity,
                                "Không có dữ liệu tủ",
                                Toast.LENGTH_SHORT
                            ).show()
                            isProcessing = false
                        }
                    }
                }
            })
        })
        actWelcome_getBtn.setActionClick(View.OnClickListener {
            if (isProcessing) return@OnClickListener
            isProcessing = true
            pIsRemove = true
            CabinetVM.ins.updateInfoCabinet(object : CabinetVM.UpdateCabinetInfoListener {
                override fun onResult(
                    isSuccess: Boolean,
                    listLockerData: ArrayList<LockerControl.LockerData>,
                    mess: String
                ) {
                    runOnUiThread {
                        if (listLockerData.isNotEmpty()) {
                            var isUsed = false
                            for (locker in listLockerData) {
                                if (locker.Type == LockerControl.USED) {
                                    isUsed = true
                                    break
                                }
                            }
                            if (isUsed) {
                                gotoVerifyFaceActivity()
                            } else {
                                Toast.makeText(this@WelcomeActivity, "Tủ trống", Toast.LENGTH_SHORT)
                                    .show()
                                isProcessing = false
                            }
                        } else {
                            Toast.makeText(
                                this@WelcomeActivity,
                                "Không có dữ liệu tủ",
                                Toast.LENGTH_SHORT
                            ).show()
                            isProcessing = false
                        }
                    }
                }
            })
        })
    }

    override fun onResult(boolean: Boolean) {
        if (!boolean) return
        CabinetVM.ins.updateInfoCabinet(object : CabinetVM.UpdateCabinetInfoListener {
            override fun onResult(
                isSuccess: Boolean,
                listLockerData: ArrayList<LockerControl.LockerData>,
                mess: String
            ) {
                runOnUiThread {
                    if (listLockerData.isNotEmpty()) {
                        gotoVerifyFaceAdminActivity()
                    } else {
                        isProcessing = false
                        Toast.makeText(
                            this@WelcomeActivity,
                            "Không có dữ liệu tủ",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        })
        hideKeyboard(this@WelcomeActivity)
    }

    private fun setupVideo() {
        val uri = "android.resource://" + packageName + "/" + R.raw.adv_video
        actWelcomeVideo?.post {
            actWelcomeVideo?.setVideoURI(Uri.parse(uri))
            actWelcomeVideo?.requestFocus()
            actWelcomeVideo?.start()
            actWelcomeVideo?.setOnCompletionListener {
                actWelcomeVideo?.start()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        setupVideo()
        /*actWelcome_slideGuide?.setListAdv(listImageSlide)*/
    }

    override fun onPause() {
        super.onPause()
        /*actWelcome_slideGuide?.pauseSlide()*/
    }

    override fun onDestroy() {
        super.onDestroy()
        /*actWelcome_slideGuide?.pauseSlide()*/
    }

}

