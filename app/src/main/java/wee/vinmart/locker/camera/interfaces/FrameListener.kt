package wee.vinmart.locker.camera.interfaces

import android.graphics.Bitmap

interface FrameListener {
    fun onFrame(bitmap: Bitmap)
}