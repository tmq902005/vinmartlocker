package wee.vinmart.locker.camera.interfaces

import com.intel.realsenseid.api.AuthenticateStatus
import com.intel.realsenseid.api.Faceprints
import com.intel.realsenseid.api.Status

interface AuthListener {
    fun onHint(hint: AuthenticateStatus?)
    fun onResult(status: AuthenticateStatus?, userId: String, facePrints: Faceprints?)
    fun onDone(status: Status?)
}