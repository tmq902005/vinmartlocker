package wee.vinmart.locker.camera

import android.graphics.Bitmap
import android.util.Log
import android.util.Size
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.Face
import wee.vinmart.locker.camera.FaceDetector.MIN_SIZE
import wee.vinmart.locker.camera.FaceDetector.RESIZE
import wee.vinmart.locker.camera.ImageConverter.resize
import java.util.concurrent.Executors


class FaceDetectorProcess {

    companion object {
        val instance by lazy {
            FaceDetectorProcess()
        }

        const val MAX_COUNT = 8
    }

    fun debug(s: Any?) {
        Log.d("FaceDetector", s.toString())
    }

    private var curFace: Face? = null

    private var faceExecutors = Executors.newSingleThreadExecutor()

    private var isFaceProcessing = false

    private var isPause = false

    private var isRecognize = false

    private var count = MAX_COUNT

    var listener: FaceListener? = null

    private var imageSize: Size? = null

    fun processImageBitmap(color: Bitmap?) {

        if (isPause) return
        if (isFaceProcessing) return
        isFaceProcessing = true
        if (color != null) {
            if (imageSize == null) imageSize = Size(color.width, color.height)
            highAccuracyProcessing(color)
        } else {
            isFaceProcessing = false
        }

    }

    fun resetData() {
        count = MAX_COUNT
        isRecognize = false
    }

    private fun checkFaceChange(face: Face): Boolean {
        return if (curFace?.trackingId != face.trackingId) {
            curFace = face
            true
        } else {
            false
        }
    }

    private fun highAccuracyProcessing(color: Bitmap) {
        faceExecutors.submit {
            try {
                val colorResize = color.copy(color.config, true).resize(RESIZE)
                synchronized(isFaceProcessing) {
                    val img = InputImage.fromBitmap(colorResize, 0)
                    FaceDetector.setRectTracking(colorResize.width, colorResize.height)
                    val completeListener = OnCompleteListener { it: Task<List<Face>> ->
                        it.addOnSuccessListener { faces: List<Face> ->
                            isFaceProcessing = false
                            synchronized(isFaceProcessing) {
                                Log.i("FaceDetected", "$faces")
                                val face: Face? = faces.maxByOrNull { it.boundingBox.width() }
                                val sizeFace = face?.boundingBox?.width() ?: 0
                                if(face!=null) listener?.hadFace(face) else listener?.onNoFace()
                                if (sizeFace >= MIN_SIZE && face != null) {
                                    if (checkFaceChange(face)) {
                                        resetData()
                                    }
                                    val isFaceOke = if (checkFaceOke(face)) {
                                        true
                                    } else {
                                        resetData()
                                        false
                                    }
                                    if (isFaceOke && !isRecognize) {
                                        count--
                                        if (count == 0) {
                                            isRecognize = true
                                            listener?.onFaceRecognized(face, color)
                                        }
                                    }

                                }
                            }
                        }.addOnFailureListener { t: Throwable ->
                            debug(t.message)
                            isFaceProcessing = false
                            synchronized(isFaceProcessing) {
                                listener?.onNoFace()
                            }
                        }

                    }
                    FaceDetector.detector
                        .process(img)
                        .addOnCompleteListener(completeListener)
                }

            } catch (e: Exception) {
                e.printStackTrace()
                isFaceProcessing = false
            }
        }
    }


    private fun checkFaceOke(face: Face?): Boolean {
        face ?: return false
        return FaceDetector.checkFaceOke(face)
    }

    interface FaceListener {
        fun onFaceRecognized(face: Face, colorMat: Bitmap)
        fun hadFace(face: Face)
        fun onNoFace()
    }

}