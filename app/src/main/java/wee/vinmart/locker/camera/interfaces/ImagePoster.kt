package wee.vinmart.locker.camera.interfaces

import android.graphics.*
import android.util.Log
import android.view.TextureView

internal class ImagePoster(
    private val mImage: Bitmap?,
    private val mTexture: TextureView?,
    private val mFlipPreview: Boolean
) : Runnable {
    companion object {
        private const val TAG = "ImagePoster"
        private var LANDMARKS_PAINT: Paint? = null
        private var DOT_PAINT: Paint? = null
        var SCALE_X = 0f
        var SCALE_Y = 0f

        init {
            SCALE_X = -1f
            SCALE_Y = 1f
            LANDMARKS_PAINT = Paint()
            LANDMARKS_PAINT?.color = Color.RED
            DOT_PAINT = Paint()
            DOT_PAINT?.color = Color.YELLOW
        }
    }

    private var mCanvas: Canvas? = null
    private var mScaleMatrix: Matrix? = null
    override fun run() {
        if (mTexture != null && mImage != null) {
            mCanvas = mTexture.lockCanvas()
            try {
                val bitmapRect = RectF(
                    0.0f, 0.0f, mImage.width.toFloat(), mImage.height
                        .toFloat()
                )
                if (mCanvas != null) {
                    val canvasRect = RectF(
                        (-mCanvas!!.width).toFloat(), 0.0f, 0.0f, mCanvas!!.height.toFloat()
                    )
                    mScaleMatrix = Matrix()
                    mScaleMatrix!!.setRectToRect(bitmapRect, canvasRect, Matrix.ScaleToFit.CENTER)
                    if (mFlipPreview) mScaleMatrix!!.postRotate(
                        180f, (-mCanvas!!.width).toFloat() / 2, mCanvas!!.height
                            .toFloat() / 2
                    )
                    mCanvas!!.scale(SCALE_X, SCALE_Y)
                    mCanvas!!.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
                    mCanvas!!.drawBitmap(mImage, mScaleMatrix!!, null)
                }
            } catch (e: Exception) {
                Log.e(TAG, "Failed to draw bitmap!")
                e.printStackTrace()
                Log.e(TAG, "run: message$e")
            } finally {
                mTexture.unlockCanvasAndPost(mCanvas!!)
            }
            mImage.recycle()
        }
    }
}