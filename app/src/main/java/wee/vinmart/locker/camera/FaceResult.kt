package wee.vinmart.locker.camera

import com.google.mlkit.vision.face.Face

class FaceResult(var face: Face?, var faceData:ByteArray?,var blurScore: Double = 0.0)