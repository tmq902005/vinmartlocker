package wee.vinmart.locker.camera

import android.content.Context
import android.util.Log
import android.view.TextureView
import com.intel.realsenseid.api.*
import com.intel.realsenseid.impl.UsbCdcConnection
import com.intel.realsenseid.impl.UsbCdcConnection.PermissionCallback
import wee.vinmart.locker.FaceAuthenticatorCreator
import wee.vinmart.locker.camera.interfaces.*
import java.util.*
import java.util.concurrent.Executors
import kotlin.concurrent.thread

class RealSenseIDImp(private val context: Context, private var m_previewTxv: TextureView?) {
    companion object {
        const val TAG = "RealSenseIDImp"
        const val ID_MAX_LENGTH = 30
        fun initLib() {
            System.loadLibrary("RealSenseIDSwigJNI")
        }
    }

    private val mEnrollmentRequiredPoses = arrayOf(FacePose.Center, FacePose.Left, FacePose.Right)
    private var mUsbCdcConnection: UsbCdcConnection = UsbCdcConnection()
    private var mFaceAuthenticator: FaceAuthenticator? = null
    private var mPreview: Preview? = null
    private var mPreviewCallback: AndroidPreviewImageReadyCallback? = null
    private var mFlipOrientation = false
    private var mAllowMasks = false
    private val mUserIds: ArrayList<String>? = null
    var mLoopRunning = false
    private var mFaceAuthenticatorCreator: FaceAuthenticatorCreator? = null
    private val mExecutors = Executors.newSingleThreadExecutor()

    fun create() {
        thread {
            buildFaceBiometricsOnThread()
        }
    }

    fun destroy() {
        deconstructFaceAuthenticator()
        mExecutors.shutdown()
    }

    fun setFrameListener(listener: FrameListener?){
        mPreviewCallback?.setListener(listener)
    }

    private fun buildFaceBiometricsOnThread(block: () -> Unit = {}) {
        if (!mUsbCdcConnection.FindSupportedDevice(context)) {
            Log.e(TAG, "Supported USB device not found")
            deconstructFaceAuthenticator()
            return
        }
        if(mUsbCdcConnection.OpenConnection() && mFaceAuthenticator!=null){
            Log.d(TAG, "FaceAuthenticator class was created")
            doWhileConnected {
                updateUIAuthSettingsFromDevice()
            }
            buildPreview()
            block()
            return
        }
        Log.d(TAG, "AsyncTask: buildFaceBiometricsOnThread")
        Log.d(TAG, "The class UsbCdcConnection was created")
        mUsbCdcConnection.RequestDevicePermission(context, PermissionCallback { permissionGranted ->
            if (permissionGranted && mUsbCdcConnection.OpenConnection()) {
                mFaceAuthenticatorCreator = FaceAuthenticatorCreator()
                val config = AndroidSerialConfig().apply{
                    fileDescriptor = mUsbCdcConnection.GetFileDescriptor()
                    readEndpoint = mUsbCdcConnection.GetReadEndpointAddress()
                    writeEndpoint = mUsbCdcConnection.GetWriteEndpointAddress()
                }
                if(mFaceAuthenticator==null){
                    mFaceAuthenticator = mFaceAuthenticatorCreator!!.Create(config)
                }
                Log.d(TAG, "FaceAuthenticator class was created")
                doWhileConnected {
                    updateUIAuthSettingsFromDevice()
                }
                buildPreview()
                block()
                return@PermissionCallback
            } else {
                Log.i(TAG, "permission not granted")
            }
            Log.e(TAG, "Error creating the class FaceAuthenticator")
            deconstructFaceAuthenticator()
        })
    }

    private fun updateUIAuthSettingsFromDevice() {
        val dc = DeviceConfig().apply {
            this.algo_flow  = DeviceConfig.AlgoFlow.All
            this.camera_rotation = DeviceConfig.CameraRotation.Rotation_0_Deg
            this.face_selection_policy = DeviceConfig.FaceSelectionPolicy.Single
            this.preview_mode = DeviceConfig.PreviewMode.MJPEG_1080P
        }
        mFaceAuthenticator!!.QueryDeviceConfig(dc)
        mFlipOrientation = dc.camera_rotation == DeviceConfig.CameraRotation.Rotation_180_Deg
        dc.security_level = DeviceConfig.SecurityLevel.High
        mAllowMasks = dc.security_level == DeviceConfig.SecurityLevel.Medium
        mFaceAuthenticator!!.SetDeviceConfig(dc)
    }

    private fun buildPreview() {
        val previewConfig = PreviewConfig()
        Log.i(TAG, "Preview starting...")
        previewConfig.cameraNumber = mUsbCdcConnection.GetFileDescriptor()
        mPreview = Preview(previewConfig)
        mPreviewCallback = AndroidPreviewImageReadyCallback(m_previewTxv, mFlipOrientation)
        mPreview?.StartPreview(mPreviewCallback)
        Log.i(TAG, "Preview started")
    }

    private fun doWhileConnected(f: ConnectionWrappedFunction? = null) {
        val config = AndroidSerialConfig().apply{
            fileDescriptor = mUsbCdcConnection.GetFileDescriptor()
            readEndpoint = mUsbCdcConnection.GetReadEndpointAddress()
            writeEndpoint = mUsbCdcConnection.GetWriteEndpointAddress()
        }
        mFaceAuthenticator?.Connect(config)
        f?.Do()
        mFaceAuthenticator?.Disconnect()
    }

    private fun deconstructFaceAuthenticator() {
        mPreview?.StopPreview()
        mFaceAuthenticator?.Disconnect()
        mUsbCdcConnection.CloseConnection()
        mExecutors.shutdown()
        mFaceAuthenticator = null
    }

    fun stopAuthenticationLoop() {
        val status = mFaceAuthenticator!!.Cancel()
        mLoopRunning = false
        Log.d(TAG, "Authentication cancel done with status: $status")
    }

    fun executeAuthenticationExtractFacePrint(listener: AuthListener) {
        Log.d(TAG, "ExecuteAuthentication")
        if (mFaceAuthenticator == null) {
            Log.d(TAG, "faceAuthenticator is null")
            return
        }
        mExecutors.submit { // a potentially time consuming task
            val timeIn = System.currentTimeMillis()
            val authenticationCallback: AuthFaceprintsExtractionCallback = object : AuthFaceprintsExtractionCallback() {
                override fun OnFaceDetected(faces: SWIGTYPE_p_std__vectorT_FaceRect_t?, ts: Long) {
                    Log.i("AuthFaceprints", "" + faces.toString())
                }

                override fun OnResult(status: AuthenticateStatus?, faceprints: Faceprints?) {
                    Log.i("AuthFaceprints", "Status: $status - FacePrintVer: ${faceprints?.version}:${faceprints?.adaptiveDescriptorWithoutMask?.size}[${System.currentTimeMillis() - timeIn}]")
                    listener.onResult(status, "", faceprints)
                }

                override fun OnHint(hint: AuthenticateStatus) {
                    Log.i("AuthFaceprints", "Hint: $hint")
                    listener.onHint(hint)
                }
            }
            doWhileConnected {
                val authenticateStatus = mFaceAuthenticator?.ExtractFaceprintsForAuth(authenticationCallback)
                Log.d(TAG, "Authentication done with status: $authenticateStatus")
                listener.onDone(authenticateStatus)
            }
        }
    }

    fun executeAuthenticationExtractFacePrintLoop(listener: AuthListener) {
        Log.d(TAG, "ExecuteAuthenticationLoop")
        if (mFaceAuthenticator == null) {
            Log.d(TAG, "faceAuthenticator is null")
            return
        }
        if (mLoopRunning) {
            stopAuthenticationLoop()
            mLoopRunning = false
            return
        }
        mLoopRunning = true
        mExecutors.submit { // a potentially time consuming task
            val authenticationCallback: AuthFaceprintsExtractionCallback = object : AuthFaceprintsExtractionCallback() {
                override fun OnResult(status: AuthenticateStatus?, faceprints: Faceprints?) {
                    Log.d(TAG, "Status: $status - FacePrints: $faceprints")
                    listener.onResult(status, "", faceprints)
                }

                override fun OnHint(hint: AuthenticateStatus) {
                    Log.d(TAG, String.format("Authentication hint: %s", hint.toString()))
                    listener.onHint(hint)
                }
            }
            doWhileConnected {
                val authenticateStatus = mFaceAuthenticator!!.ExtractFaceprintsForAuthLoop(authenticationCallback)
                Log.d(TAG, "AuthenticationLoop done with status: $authenticateStatus")
                listener.onDone(authenticateStatus)
            }
        }
    }

    fun executeAuthentication(listener: AuthListener) {
        Log.d(TAG, "ExecuteAuthentication")
        if (mFaceAuthenticator == null) {
            Log.d(TAG, "faceAuthenticator is null")
            return
        }
        mExecutors.submit { // a potentially time consuming task
            val timeIn = System.currentTimeMillis()
            val authenticationCallback: AuthenticationCallback = object : AuthenticationCallback() {
                override fun OnFaceDetected(faces: SWIGTYPE_p_std__vectorT_FaceRect_t?, ts: Long) {
                    Log.i("Auth", "Face: $faces")
                }

                override fun OnHint(hint: AuthenticateStatus?) {
                    Log.i("Auth", "Hint: $hint")
                    listener.onHint(hint)
                }

                override fun OnResult(status: AuthenticateStatus?, userId: String?) {
                    Log.i("Auth", "Status: $status - userID: $userId [${System.currentTimeMillis() - timeIn}]")
                    listener.onResult(status,userId?:"",null)
                }
            }
            doWhileConnected {
                val authenticateStatus = mFaceAuthenticator!!.Authenticate(authenticationCallback)
                Log.d(TAG, "Authentication done with status: $authenticateStatus")
                listener.onDone(authenticateStatus)
            }
        }
    }

    fun executeAuthenticationLoop(listener: AuthListener) {
        Log.d(TAG, "ExecuteAuthenticationLoop")
        if (mFaceAuthenticator == null) {
            Log.d(TAG, "faceAuthenticator is null")
            return
        }
        if (mLoopRunning) {
            stopAuthenticationLoop()
            mLoopRunning = false
            return
        }
        mLoopRunning = true
        mExecutors.submit { // a potentially time consuming task
            val authenticationCallback: AuthenticationCallback = object : AuthenticationCallback() {
                override fun OnResult(status: AuthenticateStatus?, userId: String?) {
                    Log.d(TAG, "Status: $status - UserId: $userId")
                    listener.onResult(status,userId?:"",null)
                }

                override fun OnHint(hint: AuthenticateStatus?) {
                    Log.d(TAG, String.format("Authentication hint: %s", hint.toString()))
                    listener.onHint(hint)
                }
            }
            doWhileConnected {
                val authenticateStatus = mFaceAuthenticator!!.AuthenticateLoop(authenticationCallback)
                Log.d(TAG, "AuthenticationLoop done with status: $authenticateStatus")
                listener.onDone(authenticateStatus)
            }
        }
    }

    fun executeEnroll(userId: String, listener: EnrollListener) {
        Log.d(TAG, "ExecuteEnrollment")
        if (mFaceAuthenticator == null) {
            Log.d(TAG, "faceAuthenticator is null")
            return
        }
        mExecutors.submit {
            val poseTracker =
                EnrollmentPoseTracker(
                    mEnrollmentRequiredPoses
                )
            val enrollmentCallback: EnrollmentCallback = object : EnrollmentCallback() {
                override fun OnResult(status: EnrollStatus) {
                    if (status == EnrollStatus.Success) {
                        Log.d(TAG, "Enrollment completed successfully")
                    } else {
                        val msg = "Enrollment failed with status $status"
                        Log.d(TAG, msg)
                    }
                    listener.onResult(status, null)
                }

                override fun OnProgress(pose: FacePose) {
                    poseTracker.markPoseCheck(pose)
                    val nextPose = poseTracker.next
                    if (nextPose != null) {
                        Log.d(TAG, "Enrollment progress. pose: $pose")
                    }
                    listener.onMessage(pose.toString())
                }

                override fun OnHint(hint: EnrollStatus) {
                    Log.d(TAG, "Enrollment hint: $hint")
                    if (hint != EnrollStatus.CameraStarted
                            && hint != EnrollStatus.CameraStopped
                            && hint != EnrollStatus.FaceDetected
                            && hint != EnrollStatus.LedFlowSuccess) {
                        Log.i(TAG, hint.toString())
                    }
                    listener.onHint(hint.toString())
                }
            }
            doWhileConnected {
                val enrollStatus = mFaceAuthenticator!!.Enroll(enrollmentCallback, userId)
                Log.d(TAG, "Enrollment done with status: $enrollStatus")
                listener.onDone(enrollStatus)
            }
        }
    }

    fun executeEnrollExtractFacePrint(listener: EnrollListener) {
        Log.d(TAG, "ExecuteEnrollment")
        if (mFaceAuthenticator == null) {
            Log.d(TAG, "faceAuthenticator is null")
            return
        }
        mExecutors.submit {
            val poseTracker =
                EnrollmentPoseTracker(
                    mEnrollmentRequiredPoses
                )
            val enrollmentCallback: EnrollFaceprintsExtractionCallback = object : EnrollFaceprintsExtractionCallback() {

                override fun OnResult(status: EnrollStatus?, faceprints: Faceprints?) {
                    if (status == EnrollStatus.Success) {
                        Log.d(TAG, "Enrollment completed successfully")
                    } else {
                        val msg = "Enrollment failed with status $status"
                        Log.d(TAG, msg)
                    }
                    listener.onResult(status, faceprints)
                }

                override fun OnProgress(pose: FacePose) {
                    poseTracker.markPoseCheck(pose)
                    val nextPose = poseTracker.next
                    if (nextPose != null) {
                        Log.d(TAG, "Enrollment progress. pose: $pose")
                    }
                    listener.onMessage(pose.toString())
                }

                override fun OnHint(hint: EnrollStatus) {
                    Log.d(TAG, "Enrollment hint: $hint")
                    if (hint != EnrollStatus.CameraStarted
                            && hint != EnrollStatus.CameraStopped
                            && hint != EnrollStatus.FaceDetected
                            && hint != EnrollStatus.LedFlowSuccess) {
                        Log.i(TAG, hint.toString())
                    }
                    listener.onHint(hint.toString())
                }
            }
            doWhileConnected {
                val enrollStatus = mFaceAuthenticator!!.ExtractFaceprintsForEnroll(enrollmentCallback)
                Log.d(TAG, "Enrollment done with status: $enrollStatus")
                listener.onDone(enrollStatus)
            }
        }
    }

    fun executeQueryNumberUser(block: (listUser: ArrayList<String>) -> Unit) {
        mExecutors.submit {
            val numberOfUsers = longArrayOf(0)
            var s = mFaceAuthenticator!!.QueryNumberOfUsers(numberOfUsers)
            if (s != Status.Ok) {
                Log.i(TAG, "Failed to query number of users")
                block(arrayListOf())
                return@submit
            }
            mUserIds!!.clear()
            if (numberOfUsers[0] <= 0) {
                block(arrayListOf())
                return@submit
            }
            val ids = arrayOfNulls<String>(numberOfUsers[0].toInt())
            for (i in ids.indices) {
                ids[i] = String(CharArray(ID_MAX_LENGTH)) // string of ID_MAX_LENGTH characters as a placeholder. not including \0 at the end that is required in char[]
            }
            val numOfIds = longArrayOf(ids.size.toLong())
            s = mFaceAuthenticator!!.QueryUserIds(ids, numOfIds)
            if (s == Status.Ok) {
                for (i in 0 until numOfIds[0]) {
                    mUserIds.add(ids[i.toInt()]!!)
                }
            } else {
                Log.i(TAG, "Failed to query users")
            }
            block(mUserIds)
        }

    }

    fun executeRemoveUserId(userId: String, block: (status: Status) -> Unit) {
        mExecutors.submit {
            doWhileConnected {
                val res = mFaceAuthenticator!!.RemoveUser(userId)
                if (res == Status.Ok) {
                    Log.i(TAG, "Removed user: $userId")
                    mUserIds!!.clear()
                } else {
                    Log.i(TAG, "Failed to remove user")
                }
                Log.d(TAG, "Remove selected done with res: $res")
                block(res)
            }
        }
    }

    fun executeRemoveAll(block: (status: Status) -> Unit) {
        mExecutors.submit {
            doWhileConnected {
                val res = mFaceAuthenticator!!.RemoveAll()
                if (res == Status.Ok) {
                    Log.i(TAG, "Removed All")
                    mUserIds!!.clear()
                } else {
                    Log.i(TAG, "Failed to remove user")
                }
                Log.d(TAG, "Remove selected done with res: $res")
                block(res)
            }
        }
    }

    fun executeStandbyCamera(block: (status: Status) -> Unit) {
        mExecutors.submit {
            doWhileConnected {
                val res = mFaceAuthenticator!!.Standby()
                Log.i(TAG, "Stanby: $res")
                block(res)
            }
        }
    }

    fun executeMatchFacePrint(newFacePrint: Faceprints, oldFacePrint: Faceprints,updateFacePrint: Faceprints){
        mExecutors.submit {
            val timeIn = System.currentTimeMillis()
            doWhileConnected {
                val status = mFaceAuthenticator!!.MatchFaceprints(newFacePrint,oldFacePrint,updateFacePrint)
                Log.d(TAG, "Update FacePrint status: $status [${System.currentTimeMillis() - timeIn}]")
            }
        }
    }
}