package wee.vinmart.locker.camera.interfaces

import android.graphics.Bitmap
import android.view.TextureView
import androidx.lifecycle.MutableLiveData
import com.intel.realsenseid.api.Image
import com.intel.realsenseid.api.PreviewImageReadyCallback
import wee.vinmart.locker.camera.flipHorizontal
import java.nio.ByteBuffer
import java.util.concurrent.atomic.AtomicBoolean

class AndroidPreviewImageReadyCallback(private val mPreviewTxv: TextureView?, _flipPreview: Boolean) : PreviewImageReadyCallback() {
    private val mFlipPreview: AtomicBoolean = AtomicBoolean(_flipPreview)
    private var mPreviewBuffer: ByteArray? = null
    private var mFrameListener: FrameListener? = null

    fun setListener(listener: FrameListener?){
        mFrameListener = listener
    }

    override fun OnPreviewImageReady(image: Image) {
        mFrameListener?:return
        if (mPreviewBuffer == null) {
            mPreviewBuffer = ByteArray((image.width * image.height * BYTES_PER_PIXEL).toInt())
        }
        image.GetImageBuffer(mPreviewBuffer)
        renderImage(mPreviewBuffer!!, image.width.toInt(), image.height.toInt())
    }

    private fun renderImage(rgbBuffer: ByteArray, width: Int, height: Int) {
        try {
            val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            val buffer = ByteBuffer.wrap(rgbBuffer)
            bmp.copyPixelsFromBuffer(buffer)
            val flipBM = bmp.flipHorizontal()?:return
            mFrameListener?.onFrame(flipBM)
            mPreviewTxv ?: return
            ImagePoster(bmp, mPreviewTxv, mFlipPreview.get()).run()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private const val BYTES_PER_PIXEL = 4
    }

}