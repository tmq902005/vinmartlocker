package wee.vinmart.locker.camera

import android.graphics.Rect
import com.google.mlkit.vision.face.Face
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetectorOptions

/**
 * -------------------------------------------------------------------------------------------------
 * @Project: WeeLib
 * @Created: Huy 2021/05/14
 * @Organize: Wee Digital
 * @Description: ...
 * All Right Reserved
 * -------------------------------------------------------------------------------------------------
 */
object FaceDetector {
    const val RESIZE_BLUR = 0.25
    const val RESIZE = 120.0
    const val MIN_SIZE = 20
    const val MIN = -20f
    const val MAX = 20f
    private const val minY = -20f
    private const val maxY = 20f
    private const val minZ = -15f
    const val maxZ = 15f
    private val rangeX = MIN..MAX
    private val rangeY = minY..maxY
    private val rangeZ = minZ..maxZ
    var ratio = 1.0
    var RECT_TRACKING: Rect? = null


    private val faceDetectionOption: FaceDetectorOptions
        get() = FaceDetectorOptions.Builder()
            .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
//            .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_NONE)
//            .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_NONE)
            .setMinFaceSize(0.5f)
            .enableTracking()
            .build()

    val detector by lazy {
        FaceDetection.getClient(faceDetectionOption)
    }


    fun checkFaceOke(face: Face): Boolean {
        val x = face.headEulerAngleX
        val y = face.headEulerAngleY
        val z = face.headEulerAngleZ
        val boxWidth = face.boundingBox.width()
        val isInside = RECT_TRACKING?.contains(face.boundingBox) ?: return false
        return boxWidth >= MIN_SIZE && x in rangeX && y in rangeY && z in rangeZ && isInside

    }

    fun setRectTracking(width: Int, height: Int) {
        if (RECT_TRACKING != null) return
        val minus = (height - width) / 2
        RECT_TRACKING = Rect(0, minus, width, height - minus)
    }


}