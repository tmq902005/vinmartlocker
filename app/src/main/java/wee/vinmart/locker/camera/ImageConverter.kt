package wee.vinmart.locker.camera

import android.graphics.*
import android.view.View
import com.google.mlkit.vision.face.Face
import com.google.mlkit.vision.face.FaceLandmark
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc
import java.io.ByteArrayOutputStream
import kotlin.math.roundToInt


object ImageConverter {

    private fun JPEGtoARGB8888(input: Bitmap): Bitmap? {
        var output: Bitmap? = null
        val size = input.width * input.height
        val pixels = IntArray(size)
        input.getPixels(pixels, 0, input.width, 0, 0, input.width, input.height)
        output = Bitmap.createBitmap(input.width, input.height, Bitmap.Config.ARGB_8888)
        output.setPixels(pixels, 0, output.width, 0, 0, output.width, output.height)
        return output // ARGB_8888 formated bitmap
    }

    fun Rect.padding(padding: Double): Rect {
        if (padding <= 0) return this
        val l = this.left + padding
        val t = this.top + padding
        val r = this.right - padding
        val b = this.bottom - padding
        return Rect(l.roundToInt(), t.roundToInt(), r.roundToInt(), b.roundToInt())
    }

    fun Rect.paddingMTCNN(): Rect{
        val minus = this.width() * 0.12
        val l = this.left + minus
        val t = this.top
        val r = this.right - minus
        val b = this.bottom
        return Rect(l.roundToInt(),t,r.roundToInt(),b)
    }

    fun Face.getRectRatio(ratio: Double = FaceDetector.ratio): Rect {
        val rect = this.boundingBox
        val left = rect.left / ratio
        val top = rect.top / ratio
        val right = rect.right / ratio
        val bottom = rect.bottom / ratio
        return Rect(left.roundToInt(), top.roundToInt(), right.roundToInt(), bottom.roundToInt())
    }

    fun Face.getListPoints(): ArrayList<PointF?> {
        val le = this.getLandmark(FaceLandmark.LEFT_EYE)?.position?.getRatio()
        val re = this.getLandmark(FaceLandmark.RIGHT_EYE)?.position?.getRatio()
        val lm = this.getLandmark(FaceLandmark.MOUTH_LEFT)?.position?.getRatio()
        val rm = this.getLandmark(FaceLandmark.MOUTH_RIGHT)?.position?.getRatio()
        val n = this.getLandmark(FaceLandmark.NOSE_BASE)?.position?.getRatio()
        return arrayListOf(le,re,lm,rm,n)
    }



    fun PointF.getRatio(ratio: Double = FaceDetector.ratio): PointF {
        val x = this.x / ratio.toFloat()
        val y = this.y / ratio.toFloat()
        return PointF(x, y)
    }


    fun Bitmap.resize(maxImageSize: Double, format: Bitmap.CompressFormat = Bitmap.CompressFormat.WEBP): Bitmap {
        val ratio: Double = (maxImageSize / this.width).coerceAtMost(maxImageSize / this.height)
        val width = (ratio * this.width).roundToInt()
        val height = (ratio * this.height).roundToInt()
        FaceDetector.ratio = ratio
        val bitmapCP = this.copy(Bitmap.Config.ARGB_8888, true)
        val bm = Bitmap.createScaledBitmap(bitmapCP, width, height, false)
        val stream = ByteArrayOutputStream()
        bm.compress(format, 80, stream)
        val byteArray = stream.toByteArray()
        stream.close()
        bitmapCP.recycle()
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
    }

    fun Mat.resize(maxSize: Double): Mat {
        if(this.empty()) return this
        val ratio: Double = maxSize / this.width().coerceAtLeast(this.height())
        FaceDetector.ratio = ratio
        val downscaledSize = Size(this.width() * ratio, this.height() * ratio)
        val srcResize = Mat(downscaledSize, this.type())
        Imgproc.resize(this, srcResize, downscaledSize)
        this.release()
        return srcResize
    }

    fun Bitmap.cropRect(rect: Rect): Bitmap {
        val cropRect = rect.getRectCrop(this.width, this.height)
        val cropped = Bitmap.createBitmap(this, cropRect.left, cropRect.top, cropRect.width(), cropRect.height())
        this.recycle()
        return cropped
    }
    fun Bitmap.cropPadding(padding: Double): Bitmap {
        val rect = Rect(0,0,width,height).padding(padding)
        return this.cropRect(rect)
    }


    fun Rect.getRectCrop(w: Int, h: Int): Rect {
        val top = if (top < 0) 0 else top
        val left = if (left < 0) 0 else left
        val right = if (right > w) w else right
        val bottom = if (bottom > h) h else bottom
        return Rect(left, top, right, bottom)
    }

    fun Bitmap.drawRect(rect: Rect?):Bitmap{
        try{
            rect?:return this
            val bm = this.copy(Bitmap.Config.ARGB_8888,true)
            val canvas = Canvas(bm)
            val paint = Paint().also {
                it.strokeWidth = 5f
                it.style = Paint.Style.STROKE
                it.color = Color.GREEN
            }
            canvas.drawRect(rect,paint)
            this.recycle()
            return bm
        }catch (e: java.lang.Exception){
            e.printStackTrace()
            return this
        }
    }

    fun Bitmap.drawPoints(listPoint: ArrayList<android.graphics.PointF?>):Bitmap{
        try{
            if(listPoint.isEmpty()) return this
            val bm = this.copy(Bitmap.Config.ARGB_8888,true)
            val canvas = Canvas(bm)
            val paint = Paint().also {
                it.strokeWidth = 5f
                it.style = Paint.Style.STROKE
                it.color = Color.GREEN
            }
            listPoint.forEach {
                if(it!=null) canvas.drawPoint(it.x, it.y,paint)
            }
            this.recycle()
            return bm
        }catch (e: java.lang.Exception){
            e.printStackTrace()
            return this
        }
    }

    fun Bitmap.flipHorizontal(): Bitmap? {
        return try {
            val matrix = Matrix()
            matrix.preScale(-1.0f, 1.0f)
            return Bitmap.createBitmap(this, 0, 0, this.width, this.height, matrix, true)
        } catch (e: Throwable) {
            null
        } finally {
            recycle()
        }
    }

    fun View.boundingBox(): Rect {
        val param = this.layoutParams
        val location = IntArray(2)
        this.getLocationOnScreen(location)
        val width: Int = this.width
        val height: Int = this.height
        return Rect(location[0], location[1], location[0] + width, location[1] + height)
    }

    fun NV21toJPEG(nv21: ByteArray?, width: Int, height: Int, quality: Int): ByteArray {
        val out = ByteArrayOutputStream()
        val yuv = YuvImage(nv21, ImageFormat.NV21, width, height, null)
        yuv.compressToJpeg(Rect(0, 0, width, height), quality, out)
        return out.toByteArray()
    }

    fun NV21toBitmap(byteArrayNV21: ByteArray?, width: Int, height: Int): Bitmap {
        val byteToNV21 = NV21toJPEG(byteArrayNV21, width, height, 80)
        val bitmap = BitmapFactory.decodeByteArray(byteToNV21, 0, byteToNV21.size)
        val matrix = Matrix()
        matrix.postRotate(270f)
        matrix.postScale(-1f, 1f, bitmap.width / 2f, bitmap.height / 2f)
        val bm = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        bitmap.recycle()
        return bm
    }

    fun Bitmap?.rotateBitmapFlip(degree: Float?, isFlip: Boolean): Bitmap? {
        this ?: return null
        val matrix = Matrix()
        matrix.postRotate(degree!!)
        if (isFlip) matrix.postScale(-1f, 1f, this.width / 2f, this.height / 2f)
        val bm = Bitmap.createBitmap(
                this,
                0,
                0,
                this.width,
                this.height,
                matrix,
                true
        )
        this.recycle()
        return bm
    }

}