package wee.vinmart.locker.camera

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.google.mlkit.vision.face.Face
import com.intel.realsenseid.api.AuthenticateStatus
import com.intel.realsenseid.api.EnrollStatus
import com.intel.realsenseid.api.Faceprints
import com.intel.realsenseid.api.Status
import kotlinx.android.synthetic.main.control_verifyface_layout.view.*
import wee.vinmart.locker.R
import wee.vinmart.locker.app.app
import wee.vinmart.locker.camera.interfaces.AuthListener
import wee.vinmart.locker.camera.interfaces.EnrollListener
import wee.vinmart.locker.camera.interfaces.FrameListener
import wee.vinmart.locker.utils.FaceUtil
import wee.vinmart.locker.utils.post


class FaceControl : ConstraintLayout, FaceDetectorProcess.FaceListener, FrameListener {
    private var isStarted = false
    private var isAction = false
    var isOn: Boolean = false
        set(value) {
            field = value
            if (value) {
                startCamera()
            } else {
                stopCamera()
            }
        }

    //---
    private var mOnVerifyFaceListener: OnVerifyFaceListener? = null

    //---
    private val detector = FaceDetectorProcess.instance

    private var curFaceResult: FaceResult? = null


    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs){
        initView(context, attrs)
    }

    private fun initView(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.control_verifyface_layout, this)
        detector.listener = this
    }

    fun successFace() {
        verifyFaceControl_camera.borderColor = ContextCompat.getColor(context, R.color.green)
        //verifyFaceControl_Anim.startAnim()
        /*verifyFaceControl_lottie?.playAnimation()
        post(500) { verifyFaceControl_lottie?.pauseAnimation() }*/
    }

    fun createPreviewCamera() {
        app.realSenseIDImp.setFrameListener(this)
    }

    fun startCamera() {
        isStarted = true
    }

    fun stopCamera() {
        isStarted = false
    }

    fun destroyCamera() {
        app.realSenseIDImp.setFrameListener(null)
    }

    fun resetData() {
        detector.resetData()
    }

    fun setVerifyFaceListener(listener: OnVerifyFaceListener) {
        mOnVerifyFaceListener = listener
    }

    fun actionVerify() {
        if(isAction) return
        isAction = true
        if(curFaceResult!=null) {
            app.realSenseIDImp.executeAuthenticationExtractFacePrint(object : AuthListener {
                override fun onHint(hint: AuthenticateStatus?) {
                    mOnVerifyFaceListener?.onMessage(hint?.toString() ?: "")
                }

                override fun onResult(
                    status: AuthenticateStatus?,
                    userId: String,
                    facePrints: Faceprints?
                ) {
                    if (facePrints != null) {
                        mOnVerifyFaceListener?.onMessage("Success")
                        mOnVerifyFaceListener?.onActionDone(curFaceResult!!,facePrints)
                        resetData()
                    }else{
                        mOnVerifyFaceListener?.onMessage("Failed")
                        resetData()
                        isAction = false
                    }
                }

                override fun onDone(status: Status?) {}

            })
        }else{
            resetData()
            isAction = false
        }
    }

    fun actionEnroll() {
        if(isAction) return
        isAction = true
        if(curFaceResult!=null) {
            app.realSenseIDImp.executeEnrollExtractFacePrint(object : EnrollListener {

                override fun onMessage(message: String) {
                    mOnVerifyFaceListener?.onMessage(message)
                }

                override fun onHint(hint: String) {
                    mOnVerifyFaceListener?.onMessage(hint)
                }

                override fun onResult(status: EnrollStatus?, facePrints: Faceprints?) {
                    if (facePrints != null) {
                        mOnVerifyFaceListener?.onActionDone(curFaceResult!!,facePrints)
                        resetData()
                    } else {
                        mOnVerifyFaceListener?.onMessage("FacePrint is null")
                        resetData()
                        isAction = false
                    }
                }

                override fun onDone(status: Status?) {}

            })
        }else{
            isAction = false
            resetData()
        }
    }

    private fun faceProcess(rawColorMat: Bitmap, face: Face) {
        try {
            FaceUtil.ins.getFaceResult(rawColorMat, face).let {

                if (it != null) {
                    Log.e(TAG,"getFaceResult ${it.face} ${it.blurScore}")
                    post {
                        curFaceResult = it
                        mOnVerifyFaceListener?.onResult(curFaceResult!!)
                    }
                } else {
                    resetData()
                }
            }
        } catch (ex: Exception) {
            Log.e(TAG, ex.message.toString())
            resetData()
        }
    }

    override fun onFaceRecognized(face: Face, colorMat: Bitmap) {
        Log.e(TAG,"onFaceRecognized")
        faceProcess(colorMat, face)
    }

    override fun hadFace(face: Face) {

    }

    override fun onNoFace() {
        Log.e(TAG,"onNoFace")
        resetData()
        mOnVerifyFaceListener?.onMessage("No")
    }

    interface OnVerifyFaceListener {
        fun onMessage(message: String)
        fun onResult(faceResult: FaceResult)
        fun onActionDone(faceResult: FaceResult, faceprints: Faceprints)
    }

    companion object {
        private val TAG = FaceControl::class.java.simpleName
    }

    override fun onFrame(bitmap: Bitmap) {
        if(isAction) return
        this.post {
            verifyFaceControl_camera.setImageBitmap(bitmap.copy(bitmap.config,true))
        }
        if(isStarted && !isAction) {
            detector.processImageBitmap(bitmap)
        }
    }

}