package wee.vinmart.locker.camera.interfaces

import com.intel.realsenseid.api.EnrollStatus
import com.intel.realsenseid.api.Faceprints
import com.intel.realsenseid.api.Status

interface EnrollListener {
    fun onMessage(message: String)
    fun onHint(hint: String)
    fun onResult(status: EnrollStatus?, facePrints: Faceprints?)
    fun onDone(status: Status?)
}