package wee.vinmart.locker.data

import androidx.room.*
import com.google.gson.annotations.SerializedName
import rsidlib.MatchFaceprint

@Entity(tableName = "Admin")
class Admin {

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "admin_id")
    var adminId: String = ""

    @SerializedName("name")
    @ColumnInfo(name = "admin_name")
    var adminName: String = ""

    @SerializedName("phone")
    @ColumnInfo(name = "admin_phone")
    var adminPhone: String = ""

    @SerializedName("photo")
    @ColumnInfo(name = "admin_photo")
    var adminPhoto: ByteArray? = null


    @SerializedName("enroll")
    @ColumnInfo(name = "admin_enroll")
    var adminEnroll: String? = null

    @SerializedName("withMask")
    @ColumnInfo(name = "admin_withMask")
    var adminWithMask: String? = null

    @SerializedName("withoutMask")
    @ColumnInfo(name = "admin_withoutMask")
    var adminWithoutMask: String? = null

    @SerializedName("template_version")
    @ColumnInfo(name = "template_version")
    var templateVersion: Int = 7

    @SerializedName("time")
    @ColumnInfo(name = "admin_time")
    var adminTime: Long = 0L

    constructor(
        adminId: String,
        adminName: String,
        adminPhone: String,
        adminPhoto: ByteArray?,
        adminEnroll: String?,
        adminWithMask: String?,
        adminWithoutMask: String?,
        templateVersion: Int,
        adminTime: Long
    ) {
        this.adminId = adminId
        this.adminName = adminName
        this.adminPhone = adminPhone
        this.adminPhoto = adminPhoto
        this.adminEnroll = adminEnroll
        this.adminWithMask = adminWithMask
        this.adminWithoutMask = adminWithoutMask
        this.templateVersion = templateVersion
        this.adminTime = adminTime
    }

    @Dao
    interface DAO : BaseDAO<Admin> {

        @Query("SELECT * FROM Admin")
        fun all(): List<Admin>

        @Query("DELETE FROM Admin")
        fun clear()

        @Query("SELECT * FROM Admin WHERE admin_id=:id ")
        fun loadSingle(id: String): Admin?
    }

    fun getMatchFacePrint():MatchFaceprint{
        return if(adminEnroll!=null && adminWithMask!=null && adminWithoutMask!=null){
            val matchFaceprint = MatchFaceprint().apply {
                this.version = templateVersion.toLong()
                this.enrollmentDescriptor = adminEnroll
                this.adaptiveDescriptorWithMask = adminWithMask
                this.adaptiveDescriptorWithoutMask = adminWithoutMask
            }
            matchFaceprint
        }else{
            MatchFaceprint()
        }

    }
}