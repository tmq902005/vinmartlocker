package wee.vinmart.locker.data

import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Setting")
class Setting {

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "setting_id")
    var settingId: String = ""

    @SerializedName("verName")
    @ColumnInfo(name = "setting_verName")
    var settingVerName: String? = ""

    @SerializedName("verCode")
    @ColumnInfo(name = "setting_verCode")
    var settingVerCode: Int? = null

    @SerializedName("upTime")
    @ColumnInfo(name = "setting_upTime")
    var settingUpTime: Long? = null

    @SerializedName("regCount")
    @ColumnInfo(name = "setting_regCount")
    var settingRegCount: Int? = null


    constructor(
        settingId: String,
        settingVerName: String?,
        settingVerCode: Int?,
        settingUpTime: Long?,
        settingRegCount: Int?
    ) {
        this.settingId = settingId
        this.settingVerName = settingVerName
        this.settingVerCode = settingVerCode
        this.settingUpTime = settingUpTime
        this.settingRegCount = settingRegCount
    }

    @Dao
    interface DAO : BaseDAO<Setting> {

        @Query("SELECT * FROM Setting")
        fun all(): List<Setting>

        @Query("DELETE FROM Setting")
        fun clear()

    }
}