package wee.vinmart.locker.data

import androidx.room.*
import com.google.gson.annotations.SerializedName
import rsidlib.MatchFaceprint

@Entity(tableName = "User")
class User {

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "user_id")
    var userId: String = ""

    @SerializedName("photo")
    @ColumnInfo(name = "user_photo")
    var userPhoto: ByteArray? = null

    @SerializedName("enroll")
    @ColumnInfo(name = "user_enroll")
    var userEnroll: String? = null

    @SerializedName("withMask")
    @ColumnInfo(name = "user_mask")
    var userWidthMask: String? = null

    @SerializedName("withoutMask")
    @ColumnInfo(name = "user_outMask")
    var userOutMask: String? = null

    @SerializedName("template_version")
    @ColumnInfo(name = "template_version")
    var templateVersion: Int = 7

    @SerializedName("time")
    @ColumnInfo(name = "user_time")
    var userTime: Long = -1L


    constructor(
        userId: String,
        userPhoto: ByteArray?,
        userEnroll: String?,
        userWidthMask: String?,
        userOutMask: String?,
        templateVersion: Int,
        userTime: Long
    ) {
        this.userId = userId
        this.userPhoto = userPhoto
        this.userEnroll = userEnroll
        this.userWidthMask = userWidthMask
        this.userOutMask = userOutMask
        this.templateVersion = templateVersion
        this.userTime = userTime
    }

    @Dao
    interface DAO : BaseDAO<User> {

        @Query("SELECT * FROM User")
        fun all(): List<User>

        @Query("DELETE FROM User")
        fun clear()

        @Query("SELECT * FROM User WHERE user_id=:id ")
        fun loadSingle(id: String): User?


    }

    fun getMatchFacePrint():MatchFaceprint{
        return if(userEnroll!=null && userWidthMask!=null && userOutMask!=null){
            val matchFaceprint = MatchFaceprint().apply {
                this.version = templateVersion.toLong()
                this.enrollmentDescriptor = userEnroll
                this.adaptiveDescriptorWithMask = userWidthMask
                this.adaptiveDescriptorWithoutMask = userOutMask
            }
            matchFaceprint
        }else{
            MatchFaceprint()
        }

    }
}