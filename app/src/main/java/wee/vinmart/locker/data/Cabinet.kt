package wee.vinmart.locker.data

import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Cabinet")
class Cabinet {

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "cabinet_id")
    var cabinetId: String = ""

    @SerializedName("userId")
    @ColumnInfo(name = "cabinet_userId")
    var cabinetUserId: String = ""

    @SerializedName("time")
    @ColumnInfo(name = "cabinet_time")
    var cabinetTime: Long = 0L

    @SerializedName("isUsed")
    @ColumnInfo(name = "cabinet_isUsed")
    var cabinetIsUsed: Boolean = false


    constructor(
        cabinetId: String,
        cabinetUserId: String,
        cabinetTime: Long,
        cabinetIsUsed: Boolean
    ) {
        this.cabinetId = cabinetId
        this.cabinetUserId = cabinetUserId
        this.cabinetTime = cabinetTime
        this.cabinetIsUsed = cabinetIsUsed
    }

    @Dao
    interface DAO : BaseDAO<Cabinet> {

        @Query("SELECT * FROM Cabinet")
        fun all(): List<Cabinet>

        @Query("DELETE FROM Cabinet")
        fun clear()

        @Query("SELECT * FROM Cabinet WHERE cabinet_id=:id ")
        fun loadSingle(id: String): Cabinet

        @Query("SELECT * FROM Cabinet WHERE cabinet_userId=:userID")
        fun loadWithUser(userID: String): Cabinet
    }
}