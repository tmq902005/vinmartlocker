package wee.vinmart.locker.data

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import wee.vinmart.locker.app.app

@Database(
    entities = [User::class, Cabinet::class, Setting::class, Admin::class],
    version = 1,
    exportSchema = false
)
abstract class MyRoom : RoomDatabase() {

    abstract val userDao: User.DAO

    abstract val cabinetDao: Cabinet.DAO

    abstract val settingDao: Setting.DAO

    abstract val adminDao: Admin.DAO

    companion object {

        val instance: MyRoom by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
            initDatabase()
        }

        private fun initDatabase(): MyRoom {
            return Room.databaseBuilder(app, MyRoom::class.java, "citygym_locker")
                .allowMainThreadQueries().build()
        }
    }

}