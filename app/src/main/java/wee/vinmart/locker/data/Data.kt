package wee.vinmart.locker.data

data class AppDataDownload(val dataDownload: Int, val dataSuccess: Int, val status: Int)

data class ApplicationInfo(
    val appVersionID: String,
    val name: String,
    val description: String,
    val packageName: String,
    val timeUpdated: String,
    val url: String,
    val verName: String,
    val verCode: Int
)

data class RespLatestVersion(
    val code: Int,
    val message: String,
    val actionErrcode: String,
    val result: ApplicationInfo
)

data class RespAllVersion(
    val code: Int,
    val message: String,
    val result: Array<ApplicationInfo>
)