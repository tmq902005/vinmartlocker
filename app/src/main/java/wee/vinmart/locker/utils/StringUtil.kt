package wee.vinmart.locker.utils

import android.graphics.Typeface
import android.widget.TextView
import android.text.Spannable
import android.text.SpannableString
import android.util.Log
import wee.vinface.acmv1.custom.CustomTypefaceSpan
import wee.vinface.acmv1.custom.CustomTypefaceSpanColor
import java.util.*


/**
 * Created by duyen on 10/31/17.
 */
const val DATE_TYPE_01="DATE_28/03/2017, 15:47"
const val DATE_TYPE_02="DATE_16 March, 2017"
const val DATE_TYPE_03="DATE_28/03/2017"
val vnDisplayNameDayOfWeekData=listOf("Chủ nhật", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7")
val vnDisplayNameOfMonth=listOf("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6",
                                "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12")
data class MoneyStrObj(val formatMoney:String, var selectionIndex:Int)

fun formatMoneyByStr(format: String, numberString: String): String {
    try {
        val value = numberString.replace(format, "")
        val reverseValue = StringBuilder(value).reverse().toString()
        val finalValue = StringBuilder()
        for (i in 1..reverseValue.length) {
            val `val` = reverseValue[i - 1]
            finalValue.append(`val`)
            if (i % 3 == 0 && i != reverseValue.length && i > 0) {
                finalValue.append(format)
            }
        }
        return finalValue.reverse().toString()
    } catch (e: Exception) {
        // Do nothing since not a number
        return numberString
    }
}

fun getTimesOfChar(dataString:String, charStr:String):Int{
    var times=0
    for(char in dataString){
        if(char.toString()==charStr){
            times++
        }
    }
    return times
}

fun getTimesFormatChar(format: String, numberString: String): Int {
    try {
        var time=0
        val value = numberString.replace(format, "")
        val reverseValue = StringBuilder(value).reverse().toString()
        for (i in 1..reverseValue.length) {
            if (i % 3 == 0 && i != reverseValue.length && i > 0) {
                time++
            }
        }
        return time
    } catch (e: Exception) {
        // Do nothing since not a number
        return 0
    }
}

fun formatDateByteType(date: Date, type:String): String {
    Log.e("RequestTime", date.toString())
    val cal = Calendar.getInstance()
    cal.time = date
    //---
    val hourOfDay = cal.get(Calendar.HOUR_OF_DAY)
    val minute = cal.get(Calendar.MINUTE)
    //---
    var hourStr = "" + hourOfDay
    if (hourOfDay < 10) {
        hourStr = "0" + hourOfDay
    }
    var minuteStr = "" + minute
    if (minute < 10) {
        minuteStr = "0" + minute
    }
    //---
    val dayOfMonth=cal.get(Calendar.DAY_OF_MONTH)
    val year=cal.get(Calendar.YEAR)
    //---
    val month=cal.get(Calendar.MONTH)+1
    //---
    var monthStr=month.toString()
    if(month<10){
        monthStr="0$month"
    }
    var dayOfMonthStr=dayOfMonth.toString()
    if(dayOfMonth<10){
        dayOfMonthStr="0$dayOfMonth"
    }
    //---
    val displayNameDayOfWeek=cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.UK)
    val displayNameMonthShort=cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.UK)
    var displayNameMonthLong=cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.UK)
    /*if(languageCode == vnLanguageCode){
        displayNameMonthLong= vnDisplayNameOfMonth[month-1]
    }*/
    //---
    var result=""
    if(type== DATE_TYPE_01){
        result="$dayOfMonthStr/$monthStr/$year, $hourStr:$minuteStr"
    }
    else if(type== DATE_TYPE_02){
        result="$dayOfMonthStr $displayNameMonthLong, $year"
    }
    else if(type== DATE_TYPE_03){
        result="$dayOfMonthStr/$monthStr/$year"
    }
    //---
    return result
}

fun setTypefaceSpan(textView:TextView, dataStr:String, fontFamily:Typeface, indexStart:Int, indexEnd:Int){
    val typefaceSpan= CustomTypefaceSpan("",fontFamily)
    val span=SpannableString(dataStr)
    span.setSpan(typefaceSpan, indexStart, indexEnd, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    textView.text = span
}

fun setTypefaceSpan(textView:TextView, dataStr:String, fontFamily:Typeface, indexStart:Int, indexEnd:Int, colorID: Int){
    val typefaceSpan= CustomTypefaceSpanColor("",fontFamily,colorID)
    val span=SpannableString(dataStr)
    span.setSpan(typefaceSpan, indexStart, indexEnd, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    textView.text = span
}

fun setTypefaceSpanArrayString(textView:TextView, dataStr:String, textBold: List<String>, fontFamily:Typeface){
    val mSpan: SpannableString?=SpannableString(dataStr)
    for(item in textBold){
        if(!item.isEmpty()) {
            val typefaceSpan = CustomTypefaceSpan("", fontFamily)
            val startIndex = dataStr.indexOf(item, 0, true)
            if(startIndex!=-1) {
                val endIndex = startIndex + item.length
                mSpan!!.setSpan(typefaceSpan, startIndex, endIndex, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            }
        }
    }
    textView!!.text = mSpan
}

fun formatDateStringToRFC1123Z(dateStr:String): String{
    var listStr=dateStr.split("/")
    val calender=Calendar.getInstance()
    calender.set(listStr[2].trim().toInt(), listStr[1].trim().toInt()-1, listStr[0].trim().toInt())
    return formatRFC1123Z(calender.time)
}