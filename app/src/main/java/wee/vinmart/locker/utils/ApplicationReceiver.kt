package wee.vinmart.locker.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class ApplicationReceiver: BroadcastReceiver() {
    companion object{
        const val TAG = "ApplicationReceiver"
        var mListener: ApplicationReceiverListener? = null
    }



    fun setListener(listener: ApplicationReceiverListener){
        mListener = listener
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        when(intent!!.action){
            "action.zed.apk.SlientDelete" -> {
                Log.e(TAG,"delete success")
            }
            "action.zed.apk.SlientInstall" -> {
                Log.e(TAG,"install success")
            }
            Intent.ACTION_PACKAGE_ADDED -> {
                Log.e(TAG,"${intent?.action} && $mListener")
                mListener?.appInstalled()
            }
            Intent.ACTION_PACKAGE_REMOVED -> {
                Log.e(TAG,"${intent?.action}&& $mListener")
                mListener?.appUninstalled()
            }
        }
    }

    interface ApplicationReceiverListener{
        fun appInstalled()
        fun appUninstalled()
    }
}