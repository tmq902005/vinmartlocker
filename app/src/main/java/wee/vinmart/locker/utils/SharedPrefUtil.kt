package wee.vinmart.locker.utils

import android.content.Context
import android.util.Log

class SharedPrefUtil {
    companion object {
        val DEVICE_NAME_DATA = "DEVICE_NAME_DATA"
        val DEVICE_NAME_KEY = "DEVICE_NAME_KEY"

        fun clearAllData(context: Context) {
            deleteNameDevice(context)
        }

        //=======================================
        fun getNameDevice(context: Context): String {
            val preferences = context.getSharedPreferences(DEVICE_NAME_DATA, Context.MODE_PRIVATE)
            val name = preferences.getString(DEVICE_NAME_KEY, "none")!!

            return if (name == "none") "" else name

        }

        fun saveNameDevice(context: Context, name: String) {
            try {
                val preferences = context.getSharedPreferences(DEVICE_NAME_DATA, Context.MODE_PRIVATE)
                val editor = preferences.edit()
                editor.putString(DEVICE_NAME_KEY, name)
                editor.apply()
            } catch (e: Exception) {
                Log.e("", "${e.message}")
            }

        }

        fun deleteNameDevice(context: Context) {
            val preferences = context.getSharedPreferences(DEVICE_NAME_DATA, Context.MODE_PRIVATE)
            val editor = preferences.edit()
            editor.remove(DEVICE_NAME_KEY)
            editor.apply()
        }
    }
}
