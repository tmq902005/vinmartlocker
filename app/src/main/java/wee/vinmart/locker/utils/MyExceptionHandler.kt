package wee.vinmart.locker.utils

import android.app.Activity
import android.app.PendingIntent
import android.util.Log
import androidx.core.content.ContextCompat.startActivity
import kotlin.system.exitProcess

class MyExceptionHandler(private val activity: Activity) : Thread.UncaughtExceptionHandler {
    override fun uncaughtException(thread: Thread, ex: Throwable) {
        Log.e("ErrorHandler","${thread.name} - ${ex.cause}")
//        restartApp()
    }
    private fun restartApp(){
        val restartIntent = activity.packageManager
            .getLaunchIntentForPackage(activity.packageName)
        val intent = PendingIntent.getActivity(
            activity, 0,
            restartIntent, PendingIntent.FLAG_CANCEL_CURRENT
        )
        startActivity(activity,restartIntent!!,null)
        exitProcess(2)
    }
}
