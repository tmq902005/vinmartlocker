package wee.vinmart.locker.utils

import android.content.Context
import android.net.ConnectivityManager
import java.net.InetAddress

internal object InternetUtil {
    fun getConnectivityStatusString(context: Context): String? {
        var status: String? = null
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        if (activeNetwork != null) {
            /*if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                status = "Wifi enabled"
                //return status
            } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                status = "Mobile data enabled"
                //return status
            }*/
            return if(checkInternetAccess()){
                "Online"
            }else{
                "No Internet Access"
            }
        } else {
            status = "No internet is available"
            return status
        }
        return status
    }

    fun checkInternetAccess():Boolean{
        return InetAddress.getByName("weezi.biz").isReachable(3000)
    }

}