package wee.vinmart.locker.utils;

import android.animation.Animator;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

public class CommonAnimation {


    public Animation setFadeInForView(final View view, int duration, final Runnable runEnd){
        Animation returnAnimation = new AlphaAnimation(0,1);
        returnAnimation.setDuration(duration);
        returnAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        view.setAnimation(returnAnimation);
        returnAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.VISIBLE);
                if(runEnd!=null)
                    runEnd.run();
                //animation.cancel();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return returnAnimation;
    }

    public Animation setTranslateForView(final View view,Float fromX, Float toX,
                                         Float fromY, Float toY, final Runnable runEnd){

        Animation returnAnimation = new TranslateAnimation(fromX,toX,fromY,toY);
        returnAnimation.setDuration(700);
        returnAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        returnAnimation.setFillAfter(false);
        view.setAnimation(returnAnimation);
        returnAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.VISIBLE);
                if(runEnd!=null)
                    runEnd.run();
                //animation.cancel();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return returnAnimation;
    }

    /*public Animation setRotateForView(final View view, final Runnable runEnd){
        Animation returnAnimation = new RotateAnimation(360,0,
                view.getPivotX(),view.getPivotY());
        returnAnimation.setDuration(1000);
        returnAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        returnAnimation.setFillAfter(false);
        view.setAnimation(returnAnimation);
        returnAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.VISIBLE);
                if(runEnd!=null)
                    runEnd.run();
                //animation.cancel();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return returnAnimation;
    }*/

    public Animation setTouchAnimationForView(final View view,int duration, final Runnable runEnd){
        Animation returnAnimation = new AlphaAnimation(0,1);
        returnAnimation.setDuration(duration);
        returnAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        view.setAnimation(returnAnimation);
        view.setVisibility(View.INVISIBLE);
        returnAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.VISIBLE);
                if(runEnd!=null)
                    runEnd.run();
                //animation.cancel();

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return returnAnimation;
    }



    public void setAnimShake(final View view, final Runnable runEnd){
        view.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.Shake)
                .duration(1000)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }
    public void setAnimSlideOutUp(final View view, final Runnable runEnd){
        view.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.SlideOutUp)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }

    public void setAnimSlideInDown(final View view, final Runnable runEnd){
        view.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.SlideInDown)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }

    public void setAnimFadeIn(final View view, int duration, final Runnable runEnd){
        view.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.FadeIn)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);
    }

    public void setAnimLanding(final View view, int duration, final Runnable runEnd){
        view.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.Landing)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }

    public void setAnimFadeOut(final View view, int duration, final Runnable runEnd){
        YoYo.with(Techniques.FadeOut)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }
    public void setAnimSlideInUp(final View view, int duration, final Runnable runEnd){
        view.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.SlideInUp)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }
    public void setAnimSlideOutDown(final View view, int duration, final Runnable runEnd){
        YoYo.with(Techniques.SlideOutDown)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }

    public void setAnimSlideOutUp(final View view, final int duration, final Runnable runEnd){
        YoYo.with(Techniques.SlideOutUp)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }

    public void setAnimSlideInDown(final View view, final int duration, final Runnable runEnd){
        view.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.SlideInDown)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.VISIBLE);
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }

    public void setAnimTada(final View view, final int duration, final Runnable runEnd){
        YoYo.with(Techniques.Tada)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }

    public void setAnimDropOut(final View view, final int duration, final Runnable runEnd){
        YoYo.with(Techniques.DropOut)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.VISIBLE);
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }

    public void setAnimSlideOutLeft(final View view, final int duration, final Runnable runEnd){
        YoYo.with(Techniques.SlideOutLeft)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }

    public void setAnimSlideOutRight(final View view, final int duration, final Runnable runEnd){
        YoYo.with(Techniques.SlideOutRight)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }

    public void setAnimSlideInRight(final View view, final int duration, final Runnable runEnd){
        view.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.SlideInRight)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }
    public void setAnimSlideInLeft(final View view, final int duration, final Runnable runEnd){
        view.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.SlideInLeft)
                .duration(duration)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                        if(runEnd!=null)
                            runEnd.run();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


    }

}
