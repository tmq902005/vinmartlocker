package wee.vinmart.locker.utils

/**
 * Created by Ezequiel Adrian on 24/02/2017.
 */

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.res.Resources
import android.graphics.Point
import android.graphics.PointF
import android.net.ConnectivityManager
import android.text.TextUtils
import android.view.WindowManager
import android.widget.Toast
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*

object Utils {
    var widthScreen = 0
    var heightScreen = 0
    private val widthDefault = dpToPx(1200)
    private val heightDefault = dpToPx(1920)

    fun getScreenWidthFloat(c: Context): Float {
        return getScreenWidth(c).toFloat()
    }

    fun dpToPx(dp: Int): Int {
        return (dp * Resources.getSystem().displayMetrics.density).toInt()
    }

    fun dpToPxFloat(dp: Int): Float {
        return dp * Resources.getSystem().displayMetrics.density
    }

    fun pxToDp(px: Float): Float {
        return (px / Resources.getSystem().displayMetrics.density).toInt().toFloat()
    }

    fun pxToDpInt(px: Int): Int {
        return (px / Resources.getSystem().displayMetrics.density).toInt()
    }


    fun getScreenHeight(c: Context): Int {
        if (heightScreen == 0) {
            val wm = c.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay
            val size = Point()
            display.getSize(size)
            heightScreen = size.y
        }
        return heightScreen
    }

    fun getScreenWidth(c: Context): Int {
        if (widthScreen == 0) {
            val wm = c.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay
            val size = Point()
            display.getSize(size)
            widthScreen = size.x
        }
        //---
        return widthScreen
    }

    fun reCalculatorWidth(context: Context, width: Int): Int {
        if (widthScreen == 0) {
            widthScreen = getScreenWidth(context)
        }
        return width * widthScreen / widthDefault
    }

    fun reCalculatorHeight(context: Context, height: Int): Int {
        if (heightScreen == 0) {
            heightScreen = getScreenHeight(context)
        }
        return height * heightScreen / heightDefault
    }

    fun getScreenRatio(c: Context): Float {
        val metrics = c.resources.displayMetrics
        return metrics.heightPixels.toFloat() / metrics.widthPixels.toFloat()
    }

    fun getScreenRotation(c: Context): Int {
        val wm = c.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        return wm.defaultDisplay.rotation
    }

    fun distancePointsF(p1: PointF, p2: PointF): Int {
        return Math.sqrt(((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y)).toDouble()).toInt()
    }

    fun middlePoint(p1: PointF?, p2: PointF?): PointF? {
        return if (p1 == null || p2 == null) null else PointF((p1.x + p2.x) / 2, (p1.y + p2.y) / 2)
    }

    fun inverse(degrees: Int): Int {
        return 360 - degrees % 360
    }

    @Throws(IOException::class)
    fun readFileResource(resourceClass: Class<*>, resourcePath: String): ByteArray {
        val `is` = resourceClass.classLoader!!.getResourceAsStream(resourcePath)
            ?: throw IOException("cannot find resource: $resourcePath")
        return getBytesFromInputStream(`is`)
    }

    @Throws(IOException::class)
    fun getBytesFromInputStream(inp: InputStream): ByteArray {
        return inp.readBytes()
    }

    fun allertMessage(context: Context, message: String) {
        AlertDialog.Builder(context).setMessage(message).setNeutralButton("OK") { dialog, which -> dialog.dismiss() }
            .show()
    }

    fun toastMessage(activity: Activity, message: String) {
        activity.runOnUiThread { Toast.makeText(activity, message, Toast.LENGTH_LONG).show() }
    }

    @Throws(Exception::class)
    fun stringToDate(string: String): Date {

        return SimpleDateFormat("dd/MM/yyyy").parse(string)
    }

    @Throws(Exception::class)
    fun stringToDate(date: Date): String {

        return SimpleDateFormat("dd/MM/yyyy").format(date)
    }


    fun isValidEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    fun activeNetwork(activity: Activity): Boolean {
        val cm = activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo

        return activeNetwork != null && activeNetwork.isConnected

    }

}
