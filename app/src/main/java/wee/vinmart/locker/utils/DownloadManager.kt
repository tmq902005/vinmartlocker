package wee.vinmart.locker.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.widget.Toast
import wee.vinmart.locker.control.ButtonControl
import wee.vinmart.locker.data.AppDataDownload
import java.io.File
import kotlin.concurrent.thread

@Suppress("DEPRECATION")
class DownloadManager(activity: Activity) {

    companion object{
        const val TIMEOUT = 20000L
    }

    private var mActivity: Activity = activity

    private var downloadManager: DownloadManager? = null

    private var downloadImageId: Long = -1

    private var handler: Handler = Handler(activity.mainLooper)

    private var runnable: Runnable = Runnable {  }

    private var nameApk = ""

    private var curStatus = 0

    private var isDownloading = false

    private var mListener: MyDownloadListener? = null

    private val runTimeOut = Runnable {
        handler.removeCallbacks(runnable)
        stopDownload()
        mListener?.onFailed()
        Toast.makeText(mActivity, "Download TimeOut", Toast.LENGTH_SHORT).show()
    }

    fun setListener(listener: MyDownloadListener){
        mListener = listener
    }

    @SuppressLint("SetTextI18n")
    fun start(url: Uri, name: String, tv: ButtonControl) {
        nameApk = name
        downloadManager = mActivity.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        downloadImageId = startDownload(url)
        mActivity.registerReceiver(
            onComplete,
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
        mListener?.onStart()
        tv.setText("Connecting...")
        runnable = object : Runnable {
            override fun run() {
                val state = getDownloadState(downloadImageId)
                if ((state.dataDownload) == -1) {
                    Toast.makeText(mActivity, "Download Failed", Toast.LENGTH_SHORT).show()
                    stopDownload()
                    mListener?.onFailed()
                    return
                }

                try {
                    val down = (state.dataDownload) / 1024
                    val full = (state.dataSuccess) / 1024
                    var status = 0
                    if(full != 0){
                        status = (down * 100) / full
                        tv.setText("$name đang tải : $status%")
                        if (status == 100) {
                            handler.removeCallbacks(this)
                            return
                        }
                        if(!isDownloading) isDownloading = true
                        handler.postDelayed(this, 100)
                    }else{
                        handler.removeCallbacks(this)
                        if(!isDownloading) {
                            cancelTimeOut()
                            tv.setText("Download Failed")
                            mListener?.onFailed()
                            Toast.makeText(mActivity, "Download Failed", Toast.LENGTH_SHORT).show()
                            stopDownload()
                        }
                    }
                    timeOut(status)
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(mActivity, "Download Error ${e.message}", Toast.LENGTH_SHORT).show()
                    stopDownload()
                    mListener?.onFailed()
                }
            }
        }
        handler.postDelayed(runnable, 1000)
    }

    private fun timeOut(status: Int){
        if(curStatus!=status){
            curStatus = status
            handler.removeCallbacks(runTimeOut)
            handler.postDelayed(runTimeOut, TIMEOUT)
        }
    }

    private fun cancelTimeOut(){
        handler.removeCallbacks(runTimeOut)
        curStatus = 0
    }

    @SuppressLint("NewApi")
    private fun startDownload(uri: Uri): Long {
        val downloadReference: Long
        val request = DownloadManager.Request(uri)
        request.setTitle("Data Download")
        request.setDescription("Android Data download using DownloadManager.")
        val mUri =
            Uri.fromFile(File(Environment.getExternalStorageDirectory(), "/download/$nameApk"))
        request.setDestinationUri(mUri)
        downloadReference = downloadManager?.enqueue(request) ?: -1
        return downloadReference
    }

    private fun openFile() {
        val intent = Intent(Intent.ACTION_VIEW)
        val file = File(Environment.getExternalStorageDirectory().toString() + File.separator + "Download", nameApk)
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive")
        mActivity?.startActivity(Intent.createChooser(intent, "Open Download Folder"))
    }

    private fun openFileSystem() {
        cancelTimeOut()
        val file = File(
            Environment.getExternalStorageDirectory().toString() + File.separator + "Download",
            nameApk
        )
        mListener?.onDone()
        ApplicationReceiver.mListener = object : ApplicationReceiver.ApplicationReceiverListener{
            override fun appInstalled() {
                ApplicationReceiver.mListener = null
                mListener?.installed()
            }

            override fun appUninstalled() {
                mListener?.onFailed()
            }

        }
        //SystemHelper.ZedSilentInstallApk(mActivity, file.toString(), 0)
        thread(start = true) {
            Shell.installApp(file.path)
        }

    }

    @SuppressLint("NewApi")
    private fun getDownloadState(downloadID: Long): AppDataDownload {
        var status = DownloadManager.STATUS_FAILED
        var download = 0
        var all = 0
        val query = DownloadManager.Query().setFilterById(downloadID)
        val c = downloadManager?.query(query)
        if (c != null) {
            if (c.moveToFirst()) {
                status = c.getInt(c.getColumnIndexOrThrow(DownloadManager.COLUMN_STATUS))
                when (status) {
                    DownloadManager.STATUS_PENDING -> download = 0
                    DownloadManager.STATUS_PAUSED, DownloadManager.STATUS_RUNNING -> {
                        download =
                            c.getInt(c.getColumnIndexOrThrow(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR))
                        all =
                            c.getInt(c.getColumnIndexOrThrow(DownloadManager.COLUMN_TOTAL_SIZE_BYTES))
                    }
                    DownloadManager.STATUS_SUCCESSFUL -> download = 100
                    DownloadManager.STATUS_FAILED -> download = -1
                }
            }
            c.close()
        }
        return AppDataDownload(
            download,
            all,
            status
        )
    }

    private var onComplete: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when(intent.action){
                DownloadManager.ACTION_DOWNLOAD_COMPLETE -> {
                    openFileSystem()
                }
            }
        }
    }

    fun stopDownload() {
        cancelTimeOut()
        handler.removeCallbacks(runnable)
        downloadManager?.remove(downloadImageId)
        mActivity?.unregisterReceiver(onComplete)
    }

    interface MyDownloadListener{
        fun onStart()
        fun onDone()
        fun onFailed()
        fun installed()
    }

}