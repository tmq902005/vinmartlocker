package wee.vinmart.locker.utils

import android.graphics.*
import android.media.FaceDetector
import android.util.Log
import com.google.mlkit.vision.face.Face
import org.opencv.android.Utils
import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.MatOfDouble
import org.opencv.imgproc.Imgproc
import wee.vinmart.locker.camera.FaceResult
import wee.vinmart.locker.camera.ImageConverter.cropPadding
import wee.vinmart.locker.camera.ImageConverter.getRectRatio
import java.io.ByteArrayOutputStream
import java.util.*
import kotlin.math.pow
import kotlin.math.roundToInt

class FaceUtil {
    companion object {
        const val MAX_FACE = 5
        val ins: FaceUtil by lazy {
            FaceUtil()
        }
    }

    fun getFace(bitmap: Bitmap): Array<FaceDetector.Face?> {
        return try {
            val stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
            val byteArray = stream.toByteArray()
            getFace(byteArray, bitmap.width, bitmap.height)
        } catch (e: Exception) {
            arrayOf()
        }
    }

    fun getFace(frame: ByteArray, width: Int, height: Int): Array<FaceDetector.Face?> {
        return try {
            val bitmapFactoryOptionsbfo = BitmapFactory.Options()
            bitmapFactoryOptionsbfo.inPreferredConfig = Bitmap.Config.RGB_565
            val mBitmap =
                BitmapFactory.decodeByteArray(frame, 0, frame.size, bitmapFactoryOptionsbfo)
            val myFace = arrayOfNulls<FaceDetector.Face>(MAX_FACE)
            val myFaceDetect = FaceDetector(width, height, MAX_FACE)
            myFaceDetect.findFaces(mBitmap, myFace)
            myFace
        } catch (e: Exception) {
            arrayOf()
        }
    }

    fun checkLandmark(largestFace: Face): Boolean {
        val x = largestFace.boundingBox.exactCenterX()
        val y = largestFace.boundingBox.exactCenterY()
        val eyeLeft = largestFace.leftEyeOpenProbability
        val eyeRight = largestFace.rightEyeOpenProbability
        val headEulerAngleY = largestFace.headEulerAngleY
        val headEulerAngleZ = largestFace.headEulerAngleZ
        //Log.e("Face Zone","X: $x - Y: $y")
        val isBottom = y > 420
        return (largestFace.boundingBox.width() > 100) || (isBottom && largestFace.boundingBox.width() > 90)
    }

    private fun checkZoneFace(face: Face): Boolean {
        val x = face.boundingBox.exactCenterX()
        //Log.e("Check Zone Face","X: $x")
        return x in 60f..400f
    }

    private fun checkBottomFace(face: Face): Boolean {
        val y = face.boundingBox.exactCenterY()
        return y > 420
    }

    private fun checkSizeFace(face: Face?): Boolean {
        return getFaceSize(face) >= 90
    }

    private fun checkSizeFaceInf(face: Face?): Boolean {
        return getFaceSize(face) >= 75
    }

    fun getLargestFace(faces: List<Face>): Face? {
        val listBottomFace = arrayListOf<Face>()
        val listNoneBottomFace = arrayListOf<Face>()
        if (faces.isNotEmpty()) {
            for (i in faces.indices) {
                val face = faces[i]
                if (checkZoneFace(face) && checkSizeFace(face)) {
                    if (checkBottomFace(face)) {
                        listBottomFace.add(face)
                    } else {
                        listNoneBottomFace.add(face)
                    }
                }
            }
        }
        val bottomFace = getLargestFaceFromList(listBottomFace)
        val noneBottomFace = getLargestFaceFromList(listNoneBottomFace)
        //Log.e("ListIDFaceActive",""+PubKt.getListActiveFaceId());
        //Log.e("LagestFace","Size: ${getFaceSize(largestFace)}")
        return bottomFace ?: noneBottomFace
    }

    fun getLargestFaceInf(faces: List<Face>): Face? {
        val listBottomFace = arrayListOf<Face>()
        val listNoneBottomFace = arrayListOf<Face>()
        if (faces.isNotEmpty()) {
            for (i in faces.indices) {
                val face = faces[i]
                if (checkSizeFaceInf(face)) {
                    if (checkBottomFace(face)) {
                        listBottomFace.add(face)
                    } else {
                        listNoneBottomFace.add(face)
                    }
                }
            }
        }
        val bottomFace = getLargestFaceFromList(listBottomFace)
        val noneBottomFace = getLargestFaceFromList(listNoneBottomFace)
        return bottomFace ?: noneBottomFace
    }

    fun getLargestFaceFromList(arrayList: ArrayList<Face>): Face? {
        return if (arrayList.isNotEmpty()) {
            var largestFace = arrayList.first()
            arrayList.forEach {
                if (getFaceSize(largestFace) < getFaceSize(it)) {
                    largestFace = it
                }
            }
            largestFace
        } else {
            null
        }
    }

    private fun getFaceSize(face: Face?): Int {
        face ?: return 0
        val bounds = face.boundingBox
        return /*bounds.height() * */bounds.width()
    }

    fun getFaceResult(bitmap: Bitmap?, face: Face): FaceResult? {
        val boundingBox = face.getRectRatio()
        bitmap ?: return null
        //--- Check blurry image
        //---
        //if(isBlur) {
        var top = boundingBox.top
        if (top < 0) {
            top = 0
        }
        //---
        var left = boundingBox.left
        if (left < 0) {
            left = 0
        }
        //---
        val plusH = boundingBox.width() * 0.4
        val plusW = boundingBox.width() * 0.2
        try {
            val bitmapFace = cropFaceWithPadding(bitmap, boundingBox, plusH, plusW)
            val blurScore = checkIfImageIsBlurred(
                bitmapFace.copy(bitmapFace.config, true).cropPadding(boundingBox.width() * 0.35)
            )
            if (blurScore < 60.0) {
                return null
            }
            val stream = ByteArrayOutputStream()
            bitmapFace.compress(Bitmap.CompressFormat.JPEG, 80, stream)
            val byteArray = stream.toByteArray()
            val faceResult = FaceResult(face, byteArray, blurScore)
            stream.close()
            bitmapFace.recycle()
            return faceResult
        } catch (ex: Exception) {
            Log.e("getFaceResult", ex.message.toString())
            return null
        }
    }

    private fun cropFaceWithPadding(
        bitmap: Bitmap,
        rect: Rect,
        plusH: Double,
        plusW: Double
    ): Bitmap {
        val height = rect.height() + plusH.roundToInt()
        val width = rect.width() + plusW.roundToInt()
        var top = rect.top - (plusH / 2).roundToInt()
        var left = rect.left - (plusW / 2).roundToInt()
        if (top < 0) top = 0
        if (left < 0) left = 0
        val rectCrop = getRectCrop(bitmap, Rect(left, top, left + width, top + height))
        val copiedBitmap = bitmap.copy(Bitmap.Config.RGB_565, true)
        return try {
            Bitmap.createBitmap(
                copiedBitmap,
                rectCrop.left,
                rectCrop.top,
                rectCrop.width(),
                rectCrop.height()
            )
        } catch (ex: Exception) {
            copiedBitmap
        }
    }

    private fun getRectCrop(bitmap: Bitmap, rect: Rect): Rect {
        val top = if (rect.top < 0) 0 else rect.top
        val left = if (rect.left < 0) 0 else rect.left
        val right = if (rect.right > bitmap.width) bitmap.width else rect.right
        val bottom = if (rect.bottom > bitmap.height) bitmap.height else rect.bottom
        return Rect(left, top, right, bottom)
    }

    private fun checkIfImageIsBlurred(bitmap: Bitmap?): Double {
        if (bitmap == null) {
            //Log.e("Expected bitmap was null",);
            return 0.0
        }

        val imageBitmapMat = Mat(bitmap.width, bitmap.height, CvType.CV_8UC1)
        Utils.bitmapToMat(bitmap, imageBitmapMat)

        val grayscaleBitmapMat = Mat()
        Imgproc.cvtColor(imageBitmapMat, grayscaleBitmapMat, Imgproc.COLOR_RGB2GRAY)

        val postLaplacianMat = Mat()
        Imgproc.Laplacian(grayscaleBitmapMat, postLaplacianMat, 3)

        val mean = MatOfDouble()
        val standardDeviation = MatOfDouble()
        Core.meanStdDev(postLaplacianMat, mean, standardDeviation)

        //Log.e("blurry result", "" + result)
        return standardDeviation.get(0, 0)[0].pow(2.0)
    }

    fun rotateBitmap(bitmap: Bitmap, degree: Float?): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degree!!)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    fun getRectFace(face: FaceDetector.Face): Rect {
        val midPoint = PointF()
        face.getMidPoint(midPoint)
        val faceWidth = face.eyesDistance() * 3
        val faceHeight = face.eyesDistance() * 4
        val x = midPoint.x
        val y = midPoint.y
        val left = x - (faceWidth / 2)
        val top = y - (faceHeight / 2)
        val right = x + (faceWidth / 2)
        val bottom = y + (faceHeight / 2)
        return Rect(left.roundToInt(), top.roundToInt(), right.roundToInt(), bottom.roundToInt())
    }
}