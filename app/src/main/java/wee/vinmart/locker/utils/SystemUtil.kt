@file:Suppress("DEPRECATION")

package wee.vinmart.locker.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Base64
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import java.io.*
import java.security.MessageDigest
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by duyen on 11/4/17.
 */
private val RFC1123_DATE_PATTERN = "EEE, dd MMM yyyy HH:mm:ss zzz"
var TYPE_WIFI = 1
var TYPE_MOBILE = 2
var TYPE_NOT_CONNECTED = 0

fun showKeyboard(editText: EditText, activity: AppCompatActivity) {
    editText.isFocusableInTouchMode = true
    editText.requestFocus()
    //---
    val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
}

fun hideKeyboard(editText: EditText, activity: AppCompatActivity) {
    val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(editText.windowToken, 0)
}

fun hideKeyboard(editText: EditText, activity: Activity) {
    val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(editText.windowToken, 0)
}

fun hideKeyboard(editText: EditText, activity: androidx.fragment.app.FragmentActivity) {
    val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(editText.windowToken, 0)
}

@SuppressLint("MissingPermission")
//----
fun getConnectNetworkStatus(context: Context): Int {
    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val activeNetwork = cm.activeNetworkInfo
    if (null != activeNetwork) {
        if (activeNetwork.type == ConnectivityManager.TYPE_WIFI)
            return TYPE_WIFI

        if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
            return TYPE_MOBILE
    }
    return TYPE_NOT_CONNECTED
}

//----
fun getDeviceInfo(): String {
    return Build.MANUFACTURER + " " + Build.MODEL
}

@SuppressLint("MissingPermission")
fun getLocationInfo(context: Context): Location? {
    val lm = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    val location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER)
    return location
}

fun getPathDevice(activity: AppCompatActivity): String {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        activity.noBackupFilesDir.absolutePath
    } else {
        activity.filesDir.absolutePath
    }
}

fun getWidthScreen(activity: AppCompatActivity): Int {
    val displayMetrics = DisplayMetrics()
    activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

fun getHeightScreen(activity: AppCompatActivity): Int {
    val displayMetrics = DisplayMetrics()
    activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.heightPixels
}

fun isStringNumber(numberStr: String): Boolean {
    val regexStr = Regex("^[0-9]*$")

    return numberStr.trim().matches(regexStr)
}

fun checkPhoneNumber(phoneStr: String): Boolean {
    val regexStr = Regex("\\d+(?:\\.\\d+)?")
    return phoneStr.trim().matches(regexStr) && phoneStr.length > 9
}

fun isDateValid(date: String): Boolean {
    return try {
        val df = SimpleDateFormat("dd/mm/yyyy")
        df.isLenient = false
        df.parse(date)
        true
    } catch (e: ParseException) {
        false
    }

}

fun parseRFC1123Z(timeStr: String): Date {
    return try {
        val dateFormat = SimpleDateFormat(RFC1123_DATE_PATTERN, Locale.US)
        dateFormat.parse(timeStr)
    } catch (ex: Exception) {
        Date()
    }

}

fun formatRFC1123Z(date: Date): String {
    val dateFormat = SimpleDateFormat(RFC1123_DATE_PATTERN, Locale.US)
    val formatDate = dateFormat.format(date)
    val para = formatDate.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    if (para.size != 6) {
        return formatDate
    }
    para[5] = para[5].replace("GMT", "")
    para[5] = para[5].replace(":", "")
    return para[0] + " " + para[1] + " " + para[2] + " " + para[3] + " " + para[4] + " " + para[5]

}

fun sha256Hashing(password: String): String {
    val md = MessageDigest.getInstance("SHA-256")

    md.update(password.toByteArray())
    val digest = md.digest()

    return Base64.encodeToString(digest, Base64.URL_SAFE or Base64.NO_WRAP)
}

@SuppressLint("SimpleDateFormat")
fun getDate(milliSeconds: Long, dateFormat: String): String {
    // Create a DateFormatter object for displaying date in specified format.
    val formatter = SimpleDateFormat(dateFormat)
    // Create a calendar object that will convert the date and time value in milliseconds to date.
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = milliSeconds
    return formatter.format(calendar.time)
}

fun getNumberOnString(str: String): String {
    val arrStr = str.toCharArray()
    var value = ""
    for (char in arrStr) {
        if (Character.toString(char).matches(Regex("[0-9]"))) {
            value += char
        }
    }
    return value
}

fun removeLogFile() {
    val fileLogSetting =
        "${Environment.getExternalStorageDirectory().absolutePath}/LogData/logSetting.txt"
    val logFileSetting = File(fileLogSetting)
    val fileLogIdentify =
        "${Environment.getExternalStorageDirectory().absolutePath}/LogData/logIdentify.txt"
    val logFileIdentify = File(fileLogIdentify)
    logFileIdentify.deleteOnExit()
    logFileSetting.deleteOnExit()
}

fun saveLogSetting(text: String) {
    val folder = "LogData"
    val f = File(Environment.getExternalStorageDirectory(), folder)
    if (!f.exists()) {
        f.mkdirs()
    }
    val date = getDate(System.currentTimeMillis(), "ddMMyyyy")
    val fileLog = "${f.absolutePath}/logSetting_$date.txt"
    val time = getDate(System.currentTimeMillis(), "dd/MM/yyyy hh:mm:ss.SSS")
    val logFile = File(fileLog)
    if (!logFile.exists()) {
        try {
            logFile.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
    try {
        val buf = BufferedWriter(FileWriter(logFile, true))
        buf.append("$time : $text")
        buf.newLine()
        buf.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }
}

fun saveLogIdentify(text: String) {
    val folder = "LogData"
    val f = File(Environment.getExternalStorageDirectory(), folder)
    if (!f.exists()) {
        f.mkdirs()
    }
    val fileLog = "${f.absolutePath}/logIdentify.txt"
    val time = getDate(System.currentTimeMillis(), "dd/MM/yyyy hh:mm:ss.SSS")
    val logFile = File(fileLog)
    if (!logFile.exists()) {
        try {
            logFile.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
    try {
        val buf = BufferedWriter(FileWriter(logFile, true))
        buf.append("$time : $text")
        buf.newLine()
        buf.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }
}

fun setSystemUIEnabled(enabled: Boolean) {
    try {
        val p = Runtime.getRuntime().exec("su")
        val os = DataOutputStream(p.outputStream)
        os.writeBytes(
            "pm " + (if (enabled) "enable" else "disable")
                    + " com.android.systemui\n"
        )
        os.writeBytes("exit\n")
        os.flush()
    } catch (e: IOException) {
        e.printStackTrace()
    }
}

val handlerMain = Handler(Looper.getMainLooper())

fun post(s : Long, block : () -> Unit){
    handlerMain.postDelayed({block()}, s)
}

fun post(block : () -> Unit){
    handlerMain.post { block() }
}

fun View.hide() {
    handlerMain.post { this.visibility = View.INVISIBLE }
}

fun View.show() {
    handlerMain.post { this.visibility = View.VISIBLE }
}

fun View.gone() {
    handlerMain.post { this.visibility = View.GONE }
}

fun ImageView.tint(@ColorInt color: Int) {
    this.post { this.setColorFilter(color) }
}

@SuppressLint("ResourceAsColor")
fun TextView.color(@ColorRes res: Int) {
    handlerMain.post { this.setTextColor(res) }
}

