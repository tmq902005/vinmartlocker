package wee.vinmart.locker.vm

import com.intel.realsenseid.api.Faceprints
import wee.vinmart.locker.app.app
import wee.vinmart.locker.camera.toMatchFacePrint
import wee.vinmart.locker.camera.toStringBase64
import wee.vinmart.locker.control.LockerControl
import wee.vinmart.locker.control.RegisterInfoAdminControl
import wee.vinmart.locker.data.Admin
import wee.vinmart.locker.data.Cabinet
import wee.vinmart.locker.data.MyRoom
import wee.vinmart.locker.data.User
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.collections.ArrayList

class CabinetVM {
    companion object {
        val executors: ExecutorService by lazy { Executors.newSingleThreadExecutor() }
        val ins: CabinetVM by lazy {
            CabinetVM()
        }
    }

    private val room = MyRoom.instance

    private fun addDefaultCabinet(): List<Cabinet> {
        if (room.cabinetDao.all().isNullOrEmpty()) {
            val list = listOf(
                Cabinet("01", "", 0, false),
                Cabinet("00", "", 0, false),
                Cabinet("02", "", 0, false),
                Cabinet("03", "", 0, false),
                Cabinet("04", "", 0, false),
                Cabinet("05", "", 0, false)
            )
            room.cabinetDao.insert(list)
        }
        return room.cabinetDao.all()

    }

    fun getListUsedCabinet(): ArrayList<Cabinet>{
        val list = arrayListOf<Cabinet>()
        room.cabinetDao.all().forEach{
            if(it.cabinetIsUsed){
                list.add(it)
            }
        }
        return list
    }

    fun getAllCabinet() : ArrayList<Cabinet>{
        val list = arrayListOf<Cabinet>()
        room.cabinetDao.all().forEach{ list.add(it) }
        return list
    }

    fun updateInfoCabinet(updateCabinetInfoListener: UpdateCabinetInfoListener?) {
        executors.submit {
            try {
                val arrList = arrayListOf<LockerControl.LockerData>()
                addDefaultCabinet().forEach {
                    val lockerData = LockerControl.LockerData().apply {
                        this.Code = it.cabinetId
                        this.ID = it.cabinetId
                        this.Type =
                            if (it.cabinetIsUsed) LockerControl.USED else LockerControl.READY
                        this.Times = it.cabinetTime
                        if (this.ID == "00") this.Type = LockerControl.SCREEN
                    }
                    arrList.add(lockerData)
                }
                pListLockerData = arrList
                updateCabinetInfoListener?.onResult(true, pListLockerData, "Success")
            } catch (t: Throwable) {
                t.printStackTrace()
                updateCabinetInfoListener?.onResult(false, pListLockerData, t.message.toString())
            }
        }
    }

    private fun regNewUser(
        face: ByteArray,
        facePrint: Faceprints,
        lockerData: LockerControl.LockerData
    ): RespRegLocker {
        // Register User
        val cabinet = room.cabinetDao.loadSingle(lockerData.ID)
        if (cabinet.cabinetId.isNotEmpty()) {

            val user = User(
                "${System.currentTimeMillis()}",
                face,
                facePrint.enrollmentDescriptor.contentToString(),
                facePrint.adaptiveDescriptorWithMask.contentToString(),
                facePrint.adaptiveDescriptorWithoutMask.contentToString(),
                facePrint.version,
                System.currentTimeMillis()
            )
            room.userDao.inserts(user)
            cabinet.apply {
                this.cabinetUserId = user.userId
                this.cabinetIsUsed = true
                this.cabinetTime = user.userTime
            }
            room.cabinetDao.update(cabinet)
            return RespRegLocker(
                user.userId,
                cabinet.cabinetId,
                cabinet.cabinetId,
                face.toStringBase64(),
                0,
                "Success",
                cabinet.cabinetTime,
                1
            )
        } else {
            return RespRegLocker(
                "",
                "",
                "",
                "",
                -1,
                "Locker not availble",
                0L,
                1
            )
        }

    }

    private fun regOldUser(
        uid: String,
        face: ByteArray,
        facePrint: Faceprints,
        lockerData: LockerControl.LockerData
    ): RespRegLocker {
        val cabinet = room.cabinetDao.loadSingle(lockerData.ID)
        if (cabinet != null) {
            val user = room.userDao.loadSingle(uid)
            if (user != null) {
                //-- Update info User
                user.apply {
                    this.userPhoto = face
                    this.userEnroll = facePrint.enrollmentDescriptor.contentToString()
                    this.userOutMask = facePrint.adaptiveDescriptorWithoutMask.contentToString()
                    this.userWidthMask = facePrint.adaptiveDescriptorWithMask.contentToString()
                }
                room.userDao.update(user)
                cabinet.apply {
                    this.cabinetUserId = user.userId
                    this.cabinetIsUsed = true
                    this.cabinetTime = user.userTime
                }
                room.cabinetDao.update(cabinet)
                return RespRegLocker(
                    user.userId,
                    cabinet.cabinetId,
                    cabinet.cabinetId,
                    face.toStringBase64(),
                    0,
                    "Success",
                    cabinet.cabinetTime,
                    1
                )
            } else {
                return RespRegLocker(
                    "",
                    "",
                    "",
                    "",
                    -2,
                    "User not availble",
                    0L,
                    1
                )
            }

        } else {
            return RespRegLocker(
                "",
                "",
                "",
                "",
                -1,
                "Locker not availble",
                0L,
                1
            )
        }
    }


    fun registerLocker(
        face: ByteArray,
        facePrint: Faceprints,
        lockerData: LockerControl.LockerData,
        regLockerListener: RegLockerListener
    ) {
        executors.submit {
            //--- Check User Available
            checkUser(facePrint, object : RegLockerListener {
                override fun onResult(resp: RespRegLocker?) {
                    if (resp != null) {
                        when (resp.ErrorCode) {
                            10 -> {
                                //--- User have a locker
                                regLockerListener.onResult(
                                    RespRegLocker(
                                        resp.BiometricID,
                                        resp.CabinetID,
                                        resp.CabinetNo,
                                        resp.Avatar,
                                        10,
                                        "User Had Locker",
                                        resp.Times,
                                        1
                                    )
                                )
                            }
                            1 -> {
                                //--- User not have any locker
                                regLockerListener.onResult(
                                    regOldUser(
                                        resp.BiometricID,
                                        face,
                                        facePrint,
                                        lockerData
                                    )
                                )
                            }

                            0 -> {
                                //--- User ready
                                regLockerListener.onResult(regNewUser(face, facePrint, lockerData))
                            }
                            else -> {
                                //--- Exeption
                                regLockerListener.onResult(
                                    RespRegLocker(
                                        resp.BiometricID,
                                        resp.CabinetID,
                                        resp.CabinetNo,
                                        resp.Avatar,
                                        resp.ErrorCode,
                                        resp.ErrorMessage,
                                        resp.Times,
                                        1
                                    )
                                )
                            }
                        }
                    } else {
                        regLockerListener.onResult(
                            RespRegLocker(
                                "",
                                "",
                                "",
                                "",
                                -100,
                                "Resp is null",
                                0L,
                                1
                            )
                        )
                    }
                }
            })
        }
    }

    private fun identifyUser(facePrint: Faceprints): User? {
        try {
            val listUser = room.userDao.all()
            var bestUser: User? = null
            var conf = 0
            val userMatchFacePrint = facePrint.toMatchFacePrint()
            listUser.forEach { user ->
                try {
                    val result = app.lib.match(user.getMatchFacePrint(), userMatchFacePrint)
                    // Matching user func
                    if (result.isSame && conf < result.confidence) {
                        bestUser = user
                        conf = result.confidence
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
            return bestUser
        } catch (e: Exception) {
            return null
        }

    }

    private fun identifyAdmin(facePrint: Faceprints): Admin? {
        val listAdmin = room.adminDao.all()
        var bestAdmin: Admin? = null
        var conf = 0
        val adminMatchFacePrint = facePrint.toMatchFacePrint()
        listAdmin.forEach { admin ->
            val result = app.lib.match(admin.getMatchFacePrint(), adminMatchFacePrint)
            // Matching admin func
            if (result.isSame && conf < result.confidence) {
                bestAdmin = admin
                conf = result.confidence
            }
        }
        return bestAdmin
    }


    fun checkUser(facePrint: Faceprints, regLockerListener: RegLockerListener) {
        executors.submit {
            try {
                val user = identifyUser(facePrint)
                if (user != null) {
                    //--- Check user has any locker
                    val listCabinet = room.cabinetDao.all()
                    var cabinet: Cabinet? = null
                    listCabinet.forEach { cabi ->
                        if (cabi.cabinetIsUsed) {
                            if (cabi.cabinetUserId == user.userId) {
                                cabinet = cabi
                                return@forEach
                            }
                        }
                    }
                    if (cabinet == null) {
                        //--- User not have any locker
                        val resp =
                            RespRegLocker(
                                user.userId,
                                "",
                                "",
                                "",
                                1,
                                "User not have any locker",
                                0,
                                0
                            )
                        regLockerListener.onResult(resp)
                    } else {
                        //--- User have a locker
                        val resp = RespRegLocker(
                            user.userId,
                            cabinet!!.cabinetId,
                            cabinet!!.cabinetId,
                            user.userPhoto.toStringBase64(),
                            10,
                            "Had Locker",
                            cabinet!!.cabinetTime,
                            1
                        )
                        regLockerListener.onResult(resp)
                    }
                } else {
                    //--- User not available
                    val resp = RespRegLocker("", "", "", "", 0, "User Ready", 0, 0)
                    regLockerListener.onResult(resp)
                }
            } catch (t: Throwable) {
                t.printStackTrace()
                //--- Exeption
                val resp = RespRegLocker("", "", "", "", -1, "${t.message}", 0, 0)
                regLockerListener.onResult(resp)
            }
        }
    }

    fun verifyLocker(facePrint: Faceprints, regLockerListener: RegLockerListener) {
        //--- IdentifyUser
        val user = identifyUser(facePrint)
        if (user != null) {
            //--- check cabinet of user
            val cabinet = room.cabinetDao.loadWithUser(user.userId)
            if (cabinet != null) {
                //--- Access
                regLockerListener.onResult(
                    RespRegLocker(
                        user.userId,
                        cabinet.cabinetId,
                        cabinet.cabinetId,
                        user.userPhoto.toStringBase64(),
                        0,
                        "Success",
                        cabinet.cabinetTime,
                        1
                    )
                )
                if (pIsRemove) {
                    //-- Remove used
                    cabinet.apply {
                        this.cabinetUserId = ""
                        this.cabinetIsUsed = false
                        this.cabinetTime = 0L
                    }
                    room.cabinetDao.update(cabinet)
                }
            } else {
                //--- User do not have any cabinet
                regLockerListener.onResult(
                    RespRegLocker(
                        user.userId,
                        "",
                        "",
                        user.userPhoto.toStringBase64(),
                        10,
                        "User not have any cabinet",
                        0L,
                        1
                    )
                )
            }
        } else {
            //--- User is not available
            regLockerListener.onResult(
                RespRegLocker(
                    "",
                    "",
                    "",
                    "",
                    1,
                    "User not available",
                    0L,
                    1
                )
            )
        }
    }

    fun resetLocker(cabinet: Cabinet){
        executors.submit {
            cabinet.apply {
                this.cabinetTime = 0L
                this.cabinetIsUsed = false
                this.cabinetUserId = ""
            }
            room.cabinetDao.update(cabinet)
        }
    }

    fun registerUserAdmin(
        userAdminInfo: RegisterInfoAdminControl.RegisterAdminInfo,
        regAdminListener: RegAdminListener
    ) {
        executors.submit {
            val admin = identifyAdmin(userAdminInfo.faceprints)
            if (admin != null) {
                val resp = RespRegAdmin(
                    admin.adminId,
                    admin.adminPhoto.toStringBase64(),
                    admin.adminTime,
                    10,
                    "Admin Available"
                )
                regAdminListener.onResult(resp)
            } else {
                val newAdmin = Admin(
                    "${System.currentTimeMillis()}",
                    userAdminInfo.name,
                    userAdminInfo.phone,
                    userAdminInfo.avatar,
                    userAdminInfo.faceprints.enrollmentDescriptor.contentToString(),
                    userAdminInfo.faceprints.adaptiveDescriptorWithMask.contentToString(),
                    userAdminInfo.faceprints.adaptiveDescriptorWithoutMask.contentToString(),
                    userAdminInfo.faceprints.version,
                    System.currentTimeMillis()
                )
                room.adminDao.inserts(newAdmin)
                val resp = RespRegAdmin(
                    newAdmin.adminId,
                    newAdmin.adminPhoto.toStringBase64(),
                    newAdmin.adminTime,
                    0,
                    "Success"
                )
                regAdminListener.onResult(resp)
            }
        }
    }

    fun verifyAdminUser(facePrint: Faceprints, verifyAdminListener: VerifyAdminListener) {
        executors.submit {
            val listAdmin = room.adminDao.all()
            val resp = if (listAdmin.isNotEmpty()) {
                val admin = identifyAdmin(facePrint)
                if (admin != null) {
                    RespVerifyAdmin(
                        admin.adminId,
                        admin.adminPhoto.toStringBase64(),
                        admin.adminTime,
                        0,
                        "Success",
                        admin.adminName,
                        admin.adminPhone,
                        ""
                    )
                } else {
                    RespVerifyAdmin("", "", 0L, 1, "Admin not available", "", "", "")
                }
            } else {
                RespVerifyAdmin("", "", 0L, 2, "No Admin available", "", "", "")
            }

            verifyAdminListener.onResult(resp)
        }
    }

    fun getUserAvatar(bioID: String, getAvatarUserListener: GetAvatarUserListener) {
        executors.submit {
            val user = room.userDao.loadSingle(bioID)
            val avatar = user?.userPhoto
            getAvatarUserListener.onResult(avatar)
        }
    }

    fun resetLocker(cabinetNo: String, bioID: String, resetLockerListener: ResetLockerListener) {
        executors.submit {
            val cabinet = room.cabinetDao.loadSingle(cabinetNo)
            val user = room.userDao.loadSingle(bioID)
            val admin = room.adminDao.loadSingle(pAdminData)
            if (cabinet != null && user != null && admin != null) {
                if (cabinet.cabinetUserId == user.userId) {
                    //-- Reset cabinet
                    cabinet.apply {
                        this.cabinetTime = 0L
                        this.cabinetIsUsed = false
                        this.cabinetUserId = ""
                    }
                    room.cabinetDao.update(cabinet)
                    updateInfoCabinet(object : UpdateCabinetInfoListener {
                        override fun onResult(
                            isSuccess: Boolean,
                            listLockerData: ArrayList<LockerControl.LockerData>,
                            mess: String
                        ) {
                            resetLockerListener.onResult(isSuccess)
                        }

                    })
                } else {
                    resetLockerListener.onResult(false)
                }
            } else {
                resetLockerListener.onResult(false)
            }
        }
    }

    fun adminUnLocker(cabinetNo: String, resetLockerListener: ResetLockerListener) {
        val cabinet = room.cabinetDao.loadSingle(cabinetNo)
        val admin = room.adminDao.loadSingle(pAdminData)
        if (cabinet != null && admin != null) {
            resetLockerListener.onResult(true)
        } else {
            resetLockerListener.onResult(false)
        }
    }

    fun getInfoCabinet(cabinetId: String, infoCabinetListener: InfoCabinetListener) {
        executors.submit {
            val cabinet = room.cabinetDao.loadSingle(cabinetId)
            val resp = if (cabinet != null) {
                val user = room.userDao.loadSingle(cabinet.cabinetUserId)
                RespCabinetInfo(user, cabinet)
            } else {
                RespCabinetInfo(null, null)
            }
            infoCabinetListener.onResult(resp)
        }
    }


    data class RespCabinetInfo(val user: User?, val cabinet: Cabinet?)

    data class RespRegLocker(
        val BiometricID: String, val CabinetID: String, val CabinetNo: String, val Avatar: String,
        val ErrorCode: Int, val ErrorMessage: String, val Times: Long, val Size: Int
    )

    data class RespRegAdmin(
        val AdminID: String,
        val Avatar: String,
        val Times: Long,
        val ErrorCode: Int,
        val ErrorMessage: String
    )

    data class RespVerifyAdmin(
        val AdminID: String,
        val Avatar: String,
        val Times: Long,
        val ErrorCode: Int,
        val ErrorMessage: String,
        val FullName: String,
        val PhoneNumber: String,
        val CabinetID: String
    )

    data class RespReset(val ErrorCode: Int, val ErrorMessage: String)

    interface UpdateCabinetInfoListener {
        fun onResult(
            isSuccess: Boolean,
            listLockerData: ArrayList<LockerControl.LockerData>,
            mess: String
        )
    }

    interface RegLockerListener {
        fun onResult(resp: RespRegLocker?)
    }

    interface RegAdminListener {
        fun onResult(resp: RespRegAdmin?)
    }

    interface VerifyAdminListener {
        fun onResult(resp: RespVerifyAdmin?)
    }

    interface GetAvatarUserListener {
        fun onResult(avatar: ByteArray?)
    }

    interface ResetLockerListener {
        fun onResult(isSuccess: Boolean)
    }

    interface InfoCabinetListener {
        fun onResult(resp: RespCabinetInfo?)
    }
}