package wee.vinmart.locker.vm

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import wee.vinmart.locker.WeeActivity
import wee.vinmart.locker.control.LockerControl
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

const val TIME_OUT = 15000L
var pCurActivity = WeeActivity()
const val pBlurMin = 50
var pIsRemove = true
var pIsShowTime = false
var pIsInitOpenCV = false
var pIsEmpty = false
var pIsOnline = false
var pAdminData: String = ""

var pOnLockerClickSelectListener: LockerControl.OnLockerClickListener? = null
var pOnLockerClickListener: LockerControl.OnLockerClickListener? = null
var pListLockerData = arrayListOf<LockerControl.LockerData>()

var pLockerData: LockerControl.LockerData? = null
var pLockerDataAdmin: LockerControl.LockerData? = null
const val pCabinetID = "5d32886b142faf581b192995" // HaNoi

//const val pCabinetID = "5d57679864e4f6e336c2ea82" // HCM
const val pUpdateLink = "http://app.sycomore.vn/vinmart_locker_v1.apk"
var internetStatusListener: InternetStatusListener? = null
private var isCheckInternet = false
private var mPingTimer = Timer()
private var mPingTimerTask = object : TimerTask() {
    override fun run() {
        Log.e("PingTimerTask", "isOnline: $pIsOnline")
        if (isCheckInternet) {
            checkInternetAccess()
        }
    }

}
private fun checkInternetAccess(){
    pIsOnline = pingAccess()//InternetUtil.checkInternetAccess()
    internetStatusListener?.onStatus(pIsOnline)
}

fun pStartCheckInternetAccess(time: Long){
    if(isCheckInternet) return
    isCheckInternet = true
    mPingTimer.schedule(mPingTimerTask,1000,time)
}

/*fun pStopCheckInternetAccess(){
    isCheckInternet = false
}*/

fun pingAccess(): Boolean {
    return try {
        val p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 weezi.biz")
        val returnVal = p1.waitFor()
        returnVal == 0
    } catch (e: Exception) {
        e.printStackTrace()
        false
    }
}

fun ByteArray?.toBitmap(): Bitmap? {
    this ?: return null
    return try {
        BitmapFactory.decodeByteArray(this, 0, this.size)
    } catch (e: java.lang.Exception) {
        null
    }
}

@SuppressLint("DefaultLocale")
fun Long?.formatTime(): String {
    this ?: return ""
    val hours = TimeUnit.MILLISECONDS.toHours(this)
    val minutes = TimeUnit.MILLISECONDS.toMinutes(this) - TimeUnit.HOURS.toMinutes(
        TimeUnit.MILLISECONDS.toHours(this)
    )
    val milli = TimeUnit.MILLISECONDS.toSeconds(this) - TimeUnit.MINUTES.toSeconds(
        TimeUnit.MILLISECONDS.toMinutes(this)
    )
    return String.format("%02d:%02d:%02d", hours, minutes, milli)
}

fun Long?.formatTimer(): String? {
    this ?: return ""
    val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm")
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    return formatter.format(calendar.time)
}

interface InternetStatusListener {
    fun onStatus(hasInternet: Boolean)
}