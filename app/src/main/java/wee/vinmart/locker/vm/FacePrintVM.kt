package wee.vinmart.locker.vm

import android.util.Log
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import java.util.concurrent.Executors

class FacePrintVM{
    companion object{
        val ins: FacePrintVM by lazy { FacePrintVM() }
        const val TIMEOUT_API = 2000
        private val executors = Executors.newSingleThreadExecutor()
        const val HOST = "http://weezi.biz:2086"
        const val ROW_REG = "$HOST/faceprint/registerUser"
        const val ROW_IDE = "$HOST/faceprint/identifyUser"
    }

    data class ReqIdentify(var realsenseVersion: Int, var realsenseEnrollment: ShortArray, var realsenseAdaptiveWithoutMask: ShortArray, var realsenseAdaptiveWithMask: ShortArray, var photo: String)
    data class Matched(val isSame: Boolean, val isIdentical: Boolean, val confidence: Float, val score: Float)
    data class MatchedInfo(val uuid: String, val name: String, val matched: Matched)
    data class RespIdentify(val code: Int = -100, val message: String = "Api Failed", val matchedInfo: MatchedInfo? = null)

    data class ReqRegister(var name: String, var realsenseVersion: Int, var realsenseEnrollment: ShortArray, var realsenseAdaptiveWithoutMask: ShortArray, var realsenseAdaptiveWithMask: ShortArray, var photo: String)
    data class RespRegister(val code: Int = -100, val message: String = "Api Failed", val uuid: String ="")

    fun identify(reqIdentify: ReqIdentify, block: (data: RespIdentify) -> Unit){
        val regUrl= ROW_IDE
        val gSon = Gson()
        val data = gSon.toJson(reqIdentify)
        executors.submit {
            try {
                regUrl.httpPost().timeout(TIMEOUT_API).header(Pair("Content-Type", "application/json")).body(data).responseString { _, _, result ->
                    when (result) {
                        is Result.Failure -> {
                            Log.e("identify","Send Fail: ${result.error}")
                            block(RespIdentify())
                        }
                        is Result.Success -> {
                            Log.e("identify","Send Success: ${result.value}")
                            val resp = gSon.fromJson(result.value,RespIdentify::class.java)
                            block(resp)
                        }
                    }
                }
            } catch (t: Throwable){
                t.printStackTrace()
                block(RespIdentify())
            }
        }

    }

    fun register(reqRegister: ReqRegister, block: (data: RespRegister) -> Unit){
        val regUrl= ROW_REG
        val gSon = Gson()
        val data = gSon.toJson(reqRegister)
        executors.submit {
            try {
                regUrl.httpPost().timeout(TIMEOUT_API).header(Pair("Content-Type", "application/json")).body(data).responseString { _, _, result ->
                    when (result) {
                        is Result.Failure -> {
                            Log.e("register","Send Fail: ${result.error}")
                            block(RespRegister())
                        }
                        is Result.Success -> {
                            Log.e("register","Send Success: ${result.value}")
                            val resp = gSon.fromJson(result.value,RespRegister::class.java)
                            block(resp)
                        }
                    }
                }
            } catch (t: Throwable){
                t.printStackTrace()
                block(RespRegister())
            }
        }

    }
}