package wee.vinmart.locker

import android.os.Bundle
import androidx.lifecycle.LifecycleOwner
import kotlinx.android.synthetic.main.activity_test.*
import wee.vinmart.locker.control.LockerControl
import wee.vinmart.locker.vm.CabinetVM

class TestActivity : WeeActivity(),LifecycleOwner {
    val mListLockerDataControl = arrayListOf<LockerControl.LockerData>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        CabinetVM.ins.updateInfoCabinet(object : CabinetVM.UpdateCabinetInfoListener {
            override fun onResult(isSuccess: Boolean, listLockerData: ArrayList<LockerControl.LockerData>, mess:String) {
                runOnUiThread {
                    /*if(listLockerData.isNotEmpty()){
                        var isUsed = false
                        for(locker in listLockerData){
                            if(locker.Type== LockerControl.USED){
                                isUsed = true
                                break
                            }
                        }
                        if(isUsed) {
                            //gotoVerifyFaceActivity()
                        }else{
                            Toast.makeText(this@TestActivity,"Tủ trống", Toast.LENGTH_SHORT).show()
                        }

                        mListLockerDataControl.clear()
                        mListLockerDataControl.addAll(listLockerData)
                        actTest_listLockerControl.setData(mListLockerDataControl)
                    }else{
                        Toast.makeText(this@TestActivity,"Không có dữ liệu tủ", Toast.LENGTH_SHORT).show()

                    }*/
                    for(i in 1..28){
                        listLockerData.add(LockerControl.LockerData("$i",1,"$i","",0L,1))
                    }
                    mListLockerDataControl.clear()
                    mListLockerDataControl.addAll(listLockerData)
                    actTest_listLockerControl.setData(mListLockerDataControl)
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        actTest_listLockerControl.destroy()
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()

    }
}
