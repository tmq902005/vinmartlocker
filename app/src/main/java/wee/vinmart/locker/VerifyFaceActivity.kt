package wee.vinmart.locker

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.lifecycle.LifecycleOwner
import com.intel.realsenseid.api.Faceprints
import kotlinx.android.synthetic.main.activity_verify_face.*
import wee.vinmart.locker.app.app
import wee.vinmart.locker.camera.FaceResult
import wee.vinmart.locker.camera.FaceControl
import wee.vinmart.locker.utils.handlerMain
import wee.vinmart.locker.utils.post
import wee.vinmart.locker.utils.show
import wee.vinmart.locker.vm.CabinetVM
import wee.vinmart.locker.vm.TIME_OUT

class VerifyFaceActivity : WeeActivity(), LifecycleOwner {

    companion object {
        const val TAG = "VerifyFaceActivity"
    }

    private var isProcessing = false

    private val mHandler: Handler by lazy { Handler(mainLooper) }

    private var mRunTimeOut = Runnable {
        actVerifyFace_verifyFaceControl.isOn = false
        gotoWelcomeActivity()
    }

    private var mRunGoHome = Runnable {
        gotoWelcomeActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_face)
        initVerifyFaceControl()
    }

    private fun initVerifyFaceControl() {
        actVerifyFace_verifyFaceControl.createPreviewCamera()
        showCamera()
        actVerifyFace_verifyFaceControl.setVerifyFaceListener(object :
            FaceControl.OnVerifyFaceListener {

            override fun onMessage(message: String) {

            }

            override fun onResult(faceResult: FaceResult) {
                actVerifyFace_verifyFaceControl.actionVerify()
                post { actVerifyFace_status.text = "Hệ thống đang xử lý..." }
                isProcessing = true
                resetTimeOut()
            }

            override fun onActionDone(faceResult: FaceResult, faceprints: Faceprints) {
                CabinetVM.ins.verifyLocker(faceprints, object : CabinetVM.RegLockerListener {
                    override fun onResult(resp: CabinetVM.RespRegLocker?) {
                        when (resp?.ErrorCode) {
                            0 -> {
                                post {
                                    app.usbConnectionControlV2?.write(resp.CabinetID)
                                    actVerifyFace_verifyFaceControl.successFace()
                                    actVerifyFace_status.text = "Đang thực hiện mở tủ số"
                                    actVerifyFaceNumber.show()
                                    actVerifyFaceNumber.text = "- ${resp.CabinetID} -"
                                    handlerMain.postDelayed({ gotoWelcomeActivity() }, 2000)
                                }
                            }

                            1, 10 -> {
                                post {
                                    actVerifyFace_verifyFailedControl.show()
                                    actVerifyFace_verifyFailedControl.setIconAnimFail()
                                    actVerifyFace_verifyFailedControl.setTitle("Bạn chưa đăng ký tủ")
                                    actVerifyFace_verifyFailedControl.setMessage("Vui lòng đăng ký tủ trước khi lấy đồ.")
                                    actVerifyFace_verifyFailedControl.hideTryAgain = true
                                    actVerifyFace_verifyFailedControl.setActionCancelBtn { gotoWelcomeActivity() }
                                    mHandler.postDelayed({ gotoWelcomeActivity() }, 10000)
                                }
                            }

                            else -> {
                                post {
                                    actVerifyFace_verifyFailedControl.show()
                                    actVerifyFace_verifyFailedControl.setIconAnimFail()
                                    actVerifyFace_verifyFailedControl.setTitle("Chưa thể xử lý tủ cho bạn!")
                                    actVerifyFace_verifyFailedControl.setMessage("Vui lòng thử lại sau, xin cảm ơn")
                                    actVerifyFace_verifyFailedControl.hideTryAgain = true
                                    actVerifyFace_verifyFailedControl.setActionCancelBtn { gotoWelcomeActivity() }
                                    mHandler.postDelayed({ gotoWelcomeActivity() }, 10000)
                                }
                            }
                        }
                    }

                })
            }


        })
    }

    private fun showVerifyFailed(title: String, message: String, isButton: Boolean) {
        actVerifyFace_verifyFailedControl.setTitle(title)
        actVerifyFace_verifyFailedControl.setMessage(message)
        actVerifyFace_verifyFailedControl.visibility = View.VISIBLE
        mHandler.removeCallbacks(mRunTimeOut)
        mHandler.removeCallbacks(mRunGoHome)

        if (isButton) {
            mHandler.postDelayed(mRunGoHome, 5000)
            actVerifyFace_verifyFailedControl.changeBackgroundTextColor(
                R.color.pinkish_red,
                R.drawable.bg_pinkish_red_stroke2_corner10
            )
            actVerifyFace_verifyFailedControl.setActionCancelBtn {
                gotoWelcomeActivity()
            }
            actVerifyFace_verifyFailedControl.setActionTryAgainBtn {
                isProcessing = false
                actVerifyFace_verifyFailedControl.visibility = View.GONE
                resetTimeOut()
                mHandler.removeCallbacks(mRunGoHome)
            }
        } else {
            mHandler.postDelayed(mRunGoHome, 2000)
            actVerifyFace_verifyFailedControl.hideButton()
        }
    }

    private fun resetTimeOut() {
        mHandler.removeCallbacks(mRunTimeOut)
        mHandler.postDelayed(mRunTimeOut, TIME_OUT)
    }

    private fun showCamera() {
        actVerifyFace_verifyFaceControl.isOn = true
        actVerifyFace_verifyFaceControl.resetData()
    }

    override fun onResume() {
        super.onResume()
        resetTimeOut()
        actVerifyFace_verifyFaceControl.startCamera()
    }

    override fun onPause() {
        super.onPause()
        mHandler.removeCallbacks(mRunTimeOut)
        actVerifyFace_verifyFaceControl.stopCamera()
    }

    override fun onDestroy() {
        mHandler.removeCallbacksAndMessages(null)
        actVerifyFace_verifyFaceControl.destroyCamera()
        super.onDestroy()
    }
}
