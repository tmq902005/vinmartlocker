package wee.vinmart.locker

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.activity_admin.*
import wee.vinmart.locker.app.app
import wee.vinmart.locker.app.toast
import wee.vinmart.locker.utils.DownloadManager
import wee.vinmart.locker.utils.handlerMain
import wee.vinmart.locker.utils.post
import wee.vinmart.locker.vm.TIME_OUT

class AdminActivity : WeeActivity() {

    private var isDownloading = false

    private var downloadManager: DownloadManager? = null

    private val mHandler: Handler by lazy {
        Handler(mainLooper)
    }

    private val runnableTimeOut = Runnable {
        gotoWelcomeActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
        initUI()
        downloadManager = DownloadManager(this)
    }

    private fun initUI() {
        actCabinetBack.setOnClickListener {
            if (isDownloading) {
                toast("đang cập nhật hệ thống")
                return@setOnClickListener
            }
            gotoWelcomeActivity()
        }

        actAdminNewRegister.setBackground(R.drawable.bg_white_corner5)
        actAdminNewRegister.setTextColor(R.color.red_gym)
        actAdminNewRegister.setActionClick {
            if (isDownloading) {
                toast("đang cập nhật hệ thống")
                return@setActionClick
            }
            gotoRegisterFaceAdminActivity()
        }

        actAdminCheckStatus.setBackground(R.drawable.bg_white_corner5)
        actAdminCheckStatus.setTextColor(R.color.red_gym)
        actAdminCheckStatus.setActionClick {
            if (isDownloading) {
                toast("đang cập nhật hệ thống")
                return@setActionClick
            }
            gotoCabinetAdminActivity()
        }

        actAdminSetting.setBackground(R.drawable.bg_white_corner5)
        actAdminSetting.setTextColor(R.color.red_gym)
        actAdminSetting.setActionClick {
            if (isDownloading) {
                toast("đang cập nhật hệ thống")
                return@setActionClick
            }
            gotoSetting()
        }

        actAdminOpenAll.setBackground(R.drawable.bg_white_corner5)
        actAdminOpenAll.setTextColor(R.color.red_gym)
        actAdminOpenAll.setActionClick {
            app.usbConnectionControlV2.startOpenAll(false)
            resetTimeout()
        }

        actAdminResetAll.setBackground(R.drawable.bg_white_corner5)
        actAdminResetAll.setTextColor(R.color.red_gym)
        actAdminResetAll.setActionClick {
            app.usbConnectionControlV2.startOpenAll(true)
            resetTimeout()
        }

        /*actAdminCheckUpdate.setBackground(R.drawable.bg_white_corner5)
        actAdminCheckUpdate.setTextColor(R.color.red_gym)
        actAdminCheckUpdate.setActionClick {
            val intent = packageManager?.getLaunchIntentForPackage("wee.dev.installer")
            if (intent != null) {
                startActivity(intent)
            } else {
                installKiosk()
            }
        }*/
    }

    private fun installKiosk() {
        if (isDownloading) return
        isDownloading = true
        handlerMain.removeCallbacks(runnableTimeOut)
        downloadManager?.start(
            Uri.parse("http://app.sycomore.vn:81/citigym.apk"),
            "citigym_install.apk",
            actAdminCheckUpdate
        )
        downloadManager?.setListener(object : DownloadManager.MyDownloadListener {

            override fun onStart() {}

            override fun onDone() {}

            override fun onFailed() {
                post {
                    isDownloading = false
                    actAdminCheckUpdate.setText("Cài đặt thất bại")
                }
            }

            override fun installed() {
                post {
                    isDownloading = false
                    actAdminCheckUpdate.setText("Cài đặt thành công")
                    val intent = packageManager.getLaunchIntentForPackage("wee.dev.installer")
                    startActivity(intent)
                }
            }

        })
    }

    private fun resetTimeout() {
        mHandler.removeCallbacks(runnableTimeOut)
        mHandler.postDelayed(runnableTimeOut, TIME_OUT)
    }

    override fun onResume() {
        super.onResume()
        resetTimeout()
    }

    override fun onPause() {
        super.onPause()
        mHandler.removeCallbacks(runnableTimeOut)
    }

}
