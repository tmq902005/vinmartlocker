package wee.vinmart.locker

import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import wee.vinmart.locker.control.DialogLoading
import wee.vinmart.locker.utils.MyExceptionHandler
import wee.vinmart.locker.vm.pCurActivity
import wee.vinmart.locker.vm.pIsInitOpenCV
import wee.vinmart.locker.vm.pStartCheckInternetAccess
import java.net.Inet4Address
import java.net.NetworkInterface
import java.net.SocketException
import kotlin.system.exitProcess


open class WeeActivity : AppCompatActivity() {
    var isGoto = false
    private var mLoadingDialog: DialogLoading? = null
    private val mLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    Log.e("OpenCV", "OpenCV loaded successfully")
                    pIsInitOpenCV = true
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Thread.setDefaultUncaughtExceptionHandler(MyExceptionHandler(this))
        pCurActivity = this
        mLoadingDialog = DialogLoading(this)
    }



    override fun onBackPressed() {}

    fun showMainLoading() {
        mLoadingDialog?.show()
    }

    fun hideMainLoading() {
        mLoadingDialog?.hide()
    }

    fun gotoWelcomeActivity() {
        if (isGoto) return
        isGoto = true
        val intent = Intent(this, WelcomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun gotoCabinetActivity() {
        if (isGoto) return
        isGoto = true
        val intent = Intent(this, CabinetActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun gotoRegisterFaceActivity() {
        if (isGoto) return
        isGoto = true
        val intent = Intent(this, RegisterFaceActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun gotoLockerAccessActivity(){
        if(isGoto) return
        isGoto = true
        val intent = Intent(this,LockerAccessActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun gotoVerifyFaceActivity(){
        if(isGoto) return
        isGoto = true
        val intent = Intent(this,VerifyFaceActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun gotoAdminActivity(){
        if(isGoto) return
        isGoto = true
        val intent = Intent(this,AdminActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun gotoRegisterFaceAdminActivity(){
        if(isGoto) return
        isGoto = true
        val intent = Intent(this,RegisterFaceAdminActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun gotoCheckCabinetActivity(){
        if(isGoto) return
        isGoto = true
        val intent = Intent(this,CheckCabinetActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun gotoLockerAccessAdminActivity(isUnlock: Boolean, isReset: Boolean){
        if(isGoto) return
        isGoto = true
        val intent = Intent(this,LockerAccessAdminActivity::class.java)
        intent.putExtra("UNLOCK",isUnlock)
        intent.putExtra("RESET",isReset)
        startActivity(intent)
        finish()
    }

    fun gotoVerifyFaceAdminActivity(isUnlock: Boolean, isReset: Boolean){
        if(isGoto) return
        isGoto = true
        val intent = Intent(this,VerifyFaceAdminActivity::class.java)
        intent.putExtra("VERIFY_LOCKER",true)
        intent.putExtra("UNLOCK",isUnlock)
        intent.putExtra("RESET",isReset)
        startActivity(intent)
        finish()
    }

    fun gotoVerifyFaceAdminActivity() {
        if (isGoto) return
        isGoto = true
        val intent = Intent(this, VerifyFaceAdminActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun gotoCabinetAdminActivity() {
        if (isGoto) return
        isGoto = true
        val intent = Intent(this, CabinetAdminActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun gotoSetting() {
        if (isGoto) return
        isGoto = true
        startActivity(Intent(Settings.ACTION_SETTINGS))
    }

    fun gotoADB() {
        if (isGoto) return
        isGoto = true
        val i = packageManager.getLaunchIntentForPackage("com.ttxapps.wifiadb")
        startActivity(i)
    }

    fun startCheckInternetAccess(){
        pStartCheckInternetAccess(3000L)
    }

    /*fun stopCheckInternetAccess(){
        pStopCheckInternetAccess()
    }*/

    /*fun restartApp(){
        Log.e("", "restarting app")
        val mStartActivity = Intent(this, LoadingActivity::class.java)
        val mPendingIntentId = 123456
        val mPendingIntent = PendingIntent.getActivity(
            this,
            mPendingIntentId,
            mStartActivity,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
        val mgr = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        mgr.set(RTC, System.currentTimeMillis() + 100, mPendingIntent)
        exitProcess(-1)
    }*/
    fun restartApp(){
        Log.e("", "restarting app")
        val restartIntent = packageManager
            .getLaunchIntentForPackage(packageName)
        val intent = PendingIntent.getActivity(
            this, 0,
            restartIntent, PendingIntent.FLAG_CANCEL_CURRENT
        )
        startActivity(restartIntent)
        exitProcess(2)
    }

    override fun onResume() {
        hideSystemUI()
        super.onResume()
        if(!pIsInitOpenCV) {
            if (!OpenCVLoader.initDebug()) {
                Log.e("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization")
                OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mLoaderCallback)
            } else {
                Log.e("OpenCV", "OpenCV library found inside package. Using it!")
                mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)

            }
        }
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }
    fun hideKeyboard(activity: Activity) {
        val view = activity.findViewById<View>(android.R.id.content)
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showKeyboard(activity: Activity,editText: EditText) {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }

    override fun dispatchTouchEvent(event: MotionEvent?): Boolean {
        if (event!!.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    Log.d("focus", "touchevent")
                    v.clearFocus()
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.windowToken, 0)
                    hideSystemUI()
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    fun getIpAddress() {
        try {
            val en = NetworkInterface.getNetworkInterfaces()
            while (en.hasMoreElements()) {
                val intFace = en.nextElement()
                val enumIpAdd = intFace.inetAddresses
                while (enumIpAdd.hasMoreElements()) {
                    val iNetAddress = enumIpAdd.nextElement()
                    if (!iNetAddress.isLoopbackAddress && iNetAddress is Inet4Address) {
                        val ipAddress = iNetAddress.getHostAddress().toString()
                        Log.e("IP address", "" + ipAddress)
                        Toast.makeText(pCurActivity.applicationContext,"IP: $ipAddress",Toast.LENGTH_SHORT).show()
                    }
                }
            }
        } catch (ex: SocketException) {
            Log.e("Socket exception", ex.toString())
            //pDeviceStatus.IP = "0.0.0.0"
        }
    }
}