package wee.vinmart.locker

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import kotlinx.android.synthetic.main.activity_locker_access_admin.*
import wee.vinmart.locker.app.app
import wee.vinmart.locker.control.MainHeaderControl
import wee.vinmart.locker.vm.CabinetVM
import wee.vinmart.locker.vm.pLockerDataAdmin

class LockerAccessAdminActivity : WeeActivity() {

    private var isUnlock = false

    private var isReset = false

    private val mHandler: Handler by lazy { Handler(Looper.getMainLooper()) }

    private var mRunGoCheckCabinet = Runnable {
        Log.e("LockerAccessAdminActivity", "Go to Welcome")
        gotoCheckCabinetActivity()
        pLockerDataAdmin = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_locker_access_admin)
        if (intent.hasExtra("UNLOCK") && intent.hasExtra("RESET")) {
            isUnlock = intent.getBooleanExtra("UNLOCK", false)
            isReset = intent.getBooleanExtra("RESET", false)
        }
        initUI()
    }

    private fun actionUnlock() {
        val sendUsb = pLockerDataAdmin?.ID ?: ""
        app.usbConnectionControlV2?.write(sendUsb)
    }

    private fun initUI() {
        actLockerAccessAdmin_headerControl.addListener(object :
            MainHeaderControl.IMainHeaderControl {
            override fun onCancelBtClick() {

            }

            override fun onBackBtClick() {
            }

            override fun onLogoClick() {
                gotoAdminActivity()
            }
        })
        when {
            isUnlock -> {
                CabinetVM.ins.adminUnLocker(
                    pLockerDataAdmin!!.ID,
                    object : CabinetVM.ResetLockerListener {
                        override fun onResult(isSuccess: Boolean) {
                            runOnUiThread {
                                if (isSuccess) {
                                    actLockerAccessAdmin_lockerAccessControl.startAnimationUnlockSuccess()
                                    actionUnlock()
                                } else {
                                    actLockerAccessAdmin_lockerAccessControl.startAnimationUnlockUnSuccess()
                                }

                            }
                        }
                    })

            }
            isReset -> {
                CabinetVM.ins.resetLocker(
                    pLockerDataAdmin!!.ID,
                    pLockerDataAdmin!!.UsedID,
                    object : CabinetVM.ResetLockerListener {
                        override fun onResult(isSuccess: Boolean) {
                            runOnUiThread {
                                if (isSuccess) {
                                    actLockerAccessAdmin_lockerAccessControl.startAnimationResetSuccess()
                                    actionUnlock()
                                } else {
                                    actLockerAccessAdmin_lockerAccessControl.startAnimationResetUnSuccess()
                                }

                            }
                        }
                    })
            }
            else -> {
                gotoCheckCabinetActivity()
            }
        }

    }

    override fun onResume() {
        super.onResume()
        mHandler.postDelayed(mRunGoCheckCabinet, 8000)
    }
}
